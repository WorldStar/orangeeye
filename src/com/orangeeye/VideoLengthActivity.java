package com.orangeeye;

import com.orangeeye.data.ShareData;
import com.orangeeye.db.DBAdapter;
import com.orangeeye.db.OtherData;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class VideoLengthActivity extends Activity implements OnClickListener{
	Intent intent3;
	OtherData other;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_length_screen);
        ((MyButton)findViewById(R.id.min10bt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.min10bt)).setVisibility(View.GONE);
        ((MyButton)findViewById(R.id.min15bt)).setVisibility(View.GONE);
        ((MyButton)findViewById(R.id.min15bt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.min3bt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.min6bt)).setOnClickListener(this);
        DBAdapter.init(this);
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.min3bt:
			ShareData.video_record_length = 1000 * 120;
			OtherData tmp = DBAdapter.getOtherData("record_time");
			if(tmp != null){
				other = new OtherData(tmp._id,"record_time", "2", "");
				DBAdapter.updateOtherData(other);
			}else{
				other = new OtherData(1,"record_time", "2", "");
				DBAdapter.addOtherData(other);
			}
			OtherData tmp5 = DBAdapter.getOtherData("record_time");
			intent3 = new Intent(VideoLengthActivity.this, RecordActivity.class);
        	startActivity(intent3);
			finish();
			break;
		case R.id.min6bt:
			ShareData.video_record_length = 1000 * 240;
			OtherData tmp2 = DBAdapter.getOtherData("record_time");
			if(tmp2 != null){
				other = new OtherData(tmp2._id,"record_time", "4", "");
				DBAdapter.updateOtherData(other);
			}else{
				other = new OtherData(1,"record_time", "4", "");
				DBAdapter.addOtherData(other);
			}
			OtherData tmp41 = DBAdapter.getOtherData("record_time");
			intent3 = new Intent(VideoLengthActivity.this, RecordActivity.class);
        	startActivity(intent3);
			finish();
			break;
		case R.id.min10bt:
			ShareData.video_record_length = 1000 * 600;
			OtherData tmp3 = DBAdapter.getOtherData("record_time");
			if(tmp3 != null){
				other = new OtherData(tmp3._id,"record_time", "10", "");
				DBAdapter.updateOtherData(other);
			}else{
				other = new OtherData(1,"record_time", "10", "");
				DBAdapter.addOtherData(other);
			}
			intent3 = new Intent(VideoLengthActivity.this, RecordActivity.class);
        	startActivity(intent3);
			finish();
			break;
		case R.id.min15bt:
			ShareData.video_record_length = 1000 * 900;
			OtherData tmp4 = DBAdapter.getOtherData("record_time");
			if(tmp4 != null){
				other = new OtherData(tmp4._id,"record_time", "15", "");
				DBAdapter.updateOtherData(other);
			}else{
				other = new OtherData(1,"record_time", "15", "");
				DBAdapter.addOtherData(other);
			}
			intent3 = new Intent(VideoLengthActivity.this, RecordActivity.class);
        	startActivity(intent3);
			finish();
			break;
		}
	}

	 @Override
   public void onBackPressed() {
			intent3 = new Intent(VideoLengthActivity.this, HomeActivity.class);
        	startActivity(intent3);
			finish();
   }

    
}
