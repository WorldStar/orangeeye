package com.orangeeye;

import com.orangeeye.db.DBAdapter;
import com.orangeeye.db.OtherData;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.Window;


public class OrangeSplashActivity extends Activity{
	private boolean checkflg = false;
	OtherData setdata;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash);
		Thread splashTimer = new Thread(){
			public void run(){
				try{
					int splashTimer = 0;
					while (splashTimer < 2000){
						sleep (100);
						splashTimer += 100;
					}
					Display display = getWindowManager().getDefaultDisplay();
					Value_Info.width = display.getWidth();
					Value_Info.height = display.getHeight();
					CheckEmergencyAndInsurance();                      
					
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					finish();
				}
			}
			
		};
		
		splashTimer.start();
	}
	private void CheckEmergencyAndInsurance(){

        DBAdapter.init(this);
        setdata = DBAdapter.getOtherData("ntuc_data");
        if(setdata != null){
        	if(setdata.value.equals("1")){
        		Value_Info.incomeflg = true;
        		Intent intent = new Intent(OrangeSplashActivity.this, HomeActivity.class);
            	startActivity(intent);
            	finish();
        	}else if(setdata.value.equals("0")){
        		Intent intent = new Intent(OrangeSplashActivity.this, HomeActivity.class);
            	startActivity(intent);
            	finish();
        	}else{
        		Value_Info.incomeflg = false;
    			Intent intent = new Intent(OrangeSplashActivity.this, OrangeMainActivity.class);
            	startActivity(intent);
            	finish();
        	}
        }else{
			Intent intent = new Intent(OrangeSplashActivity.this, OrangeMainActivity.class);
        	startActivity(intent);
        	finish();
        }
		/*if(Value_Info.incomeflg){
			Intent intent = new Intent(OrangeSplashActivity.this, HomeActivity.class);
        	startActivity(intent);
		}else{
			Intent intent = new Intent(OrangeSplashActivity.this, OrangeMainActivity.class);
        	startActivity(intent);
		}*/
	}
}
