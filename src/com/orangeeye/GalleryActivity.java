package com.orangeeye;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.FacebookRequestError;
import com.facebook.LoggingBehavior;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.RequestBatch;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.facebook.UiLifecycleHelper;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;
import com.facebook.android.Util;
import com.facebook.internal.Utility;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.orangeeye.data.SearchAddr;
import com.orangeeye.data.ShareData;
import com.orangeeye.db.DBAdapter;
import com.orangeeye.db.RecordData;
import com.orangeeye.ffmpeg.FfmpegJob;
import com.orangeeye.ffmpeg.Utils;
import com.orangeeye.lib.CheckInternetAccess;
import com.orangeeye.lib.ReturnMessage;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;

import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.provider.MediaStore.Video;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Paint.Align;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class GalleryActivity extends Activity{
	LinearLayout share_screen_layout;
	Gallery gallery;
	List<RecordData> recordlist = new ArrayList<RecordData>();
	int pos = 0;
    private String mFfmpegInstallPath;
	private String mMarkInstallPath;
	private String recordfilename;
	private String cutfilename;
	private String tempfilename;
	private String realrecordfilepath;
	ProgressDialog progressDialog;
	public GraphUser mFBUserInfo = null;
	Session session;
    private UiLifecycleHelper uiHelper;
    Bundle bundle;
    boolean upload_flg = false;
    boolean login_flg = false;
    private static final List<String> PERMISSIONS = Arrays.asList("publish_stream, publish_actions, video_upload");
	Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
	private Session.StatusCallback statusCallback = new SessionStatusCallback();
	
	private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
        	if(upload_flg){
        		return;
        	}
            onSessionStateChange(session, state, exception);
        }
    };
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = savedInstanceState;
        //Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        recordfilename = Environment.getExternalStorageDirectory() + "/OrangeEyeRecord/OrangeEye.mp4";
 		uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(bundle);
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.orangeeye", 
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                }
        } catch (NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        
        session = Session.getActiveSession();
        if(!GalleryPreActivity.login_flg && session.getPermissions().size() == 0){
        	session = new Session(this);
    		List<String> permissions = new ArrayList<String>();
    		permissions.add("email");
    		//permissions.add("password");
    		Session.OpenRequest request = new Session.OpenRequest(this).setPermissions(permissions);
    		request.setCallback(callback);
    		//session.openForRead(new Session.OpenRequest(this).setPermissions(Arrays.asList("email")));
    		Session.setActiveSession(session);
    		session.openForRead(request);
    		GalleryPreActivity.login_flg = true;
        }
		//facebook_check();
        /*Session session = Session.getActiveSession();
        if (session == null) {
            if (savedInstanceState != null) {
                session = Session.restoreSession(this, null, statusCallback, savedInstanceState);
            }
            if (session == null) {
                session = new Session(this);
            }
            Session.setActiveSession(session);
            if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
                session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
            }
        }*/
    }


    @Override
    public void onResume() {
        super.onResume();
        session = Session.getActiveSession();
        if (session != null && (session.isOpened() || session.isClosed())) {
        	if(!GalleryPreActivity.doneflg){
        		onSessionStateChange(session, session.getState(), null);
        	}
        }
        uiHelper.onResume();

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if(uiHelper != null)
        	uiHelper.onDestroy();
       // Session session = Session.getActiveSession();
       // session.close();
    }
    @Override
    public void onPause() {
        super.onPause();
        if(uiHelper != null)
        	uiHelper.onPause();
        //Session session = Session.getActiveSession();
        //session.close();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);

		GalleryPreActivity.doneflg = true;
        //Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
        //facebook_video_upload(recordfilename);
        //video_upload();
    	//super.onActivityResult(requestCode, resultCode, data);
        //uiHelper.onActivityResult(requestCode, resultCode, data);
    	 super.onActivityResult(requestCode, resultCode, data);
    	// onSessionStateChange(session, session.getState(), null);
         //uiHelper.onActivityResult(requestCode, resultCode, data);
    }

	@Override
	public void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    uiHelper.onSaveInstanceState(outState);
	}

    private class SessionStatusCallback implements Session.StatusCallback {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
        	if (!session.isOpened() && !session.isClosed()) {
        		
        	}else{
        		//facebook_video_upload(recordfilename);
        	}
        	//facebook_video_upload(recordfilename);
        }
    }
	
	
	 @Override
	 public void onBackPressed() {
		 ShareData.selected_record_id = 0;
		Intent intent3 = new Intent(GalleryActivity.this, GalleryPreActivity.class);
      	startActivity(intent3);
		finish();
	 }
	 public void facebook_upload(String dataPath){
		 byte[] data = null;
		 String dataMsg = "Recording video file upload.";
		 Bundle param;
		 Facebook facebook = new Facebook("777673488923807");
		 AsyncFacebookRunner mAsyncRunner = new AsyncFacebookRunner(facebook);
		 InputStream is = null;
		 try {
		     is = new FileInputStream(dataPath);
		     data = readBytes(is);
		     param = new Bundle();
		     param.putString("message", dataMsg);
		     param.putByteArray("video", data);
		     mAsyncRunner.request("me/videos", param, "POST", null, null);
		 }
		 catch (FileNotFoundException e) {
		    e.printStackTrace();
		 }
		 catch (IOException e) {
		    e.printStackTrace();
		 }
	 }
	 public byte[] readBytes(InputStream inputStream) throws IOException {
		    // This dynamically extends to take the bytes you read.
		    ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

		    // This is storage overwritten on each iteration with bytes.
		    int bufferSize = 1024;
		    byte[] buffer = new byte[bufferSize];

		    // We need to know how may bytes were read to write them to the byteBuffer.
		    int len = 0;
		    while ((len = inputStream.read(buffer)) != -1) {
		        byteBuffer.write(buffer, 0, len);
		    }

		    // And then we can return your byte array.
		    return byteBuffer.toByteArray();
		}
	 public void facebook_video_upload(String path){
		 //String path;
	        //get the current active facebook session
	        session = Session.getActiveSession();

	        
	        //If the session is open
	       // if(session.isOpened()) {
	            //Get the list of permissions associated with the session
	            List<String> permissions = session.getPermissions();
	            //if the session does not have video_upload permission
	            if(!permissions.contains("publish_actions") && !upload_flg) {
	                //Get the permission from user to upload the video to facebook
	                Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(GalleryActivity.this, PERMISSIONS);
	                session.requestNewPublishPermissions(newPermissionsRequest);
	            }


	            progressDialog = ProgressDialog.show(this, "", "Uploading Video...", 
	    				true, false);
	    		progressDialog.setCancelable(true);
	            //Create a new file for the video 
	            File file = new File(path);
	            try {
	                //create a new request to upload video to the facebook
	                Request videoRequest = Request.newUploadVideoRequest(session, file, new Request.Callback() {

	                    @Override
	                    public void onCompleted(Response response) {

	                        if(response.getError()==null)
	                        {
	                            Toast.makeText(GalleryActivity.this, "video shared successfully", Toast.LENGTH_SHORT).show();
	                        }
	                        else
	                        {
	                            Toast.makeText(GalleryActivity.this, response.getError().getErrorMessage(), Toast.LENGTH_SHORT).show();
	                        }
	                        if(!upload_flg){

		                        upload_flg = true;
		                        //Session session = Session.getActiveSession();
		                        //session.removeCallback(callback);
		                        //session.close();
		                        //if(uiHelper != null)
		                        	//uiHelper.onDestroy();
		                        if(progressDialog != null)
		    						progressDialog.dismiss();
		        	            Intent intent5 = new Intent(GalleryActivity.this, GalleryPreActivity.class);
		                    	startActivity(intent5);
		                    	finish();
	                        }else{
	                        	finish();
	                        }
	                    }
	                });

	                //Execute the request in a separate thread
	                videoRequest.executeAsync();

	            } catch (FileNotFoundException e) {
	                e.printStackTrace();
	            }
	       // }

	        //Session is not open
	        //else {
	            //Toast.makeText(getApplicationContext(), "Please login to facebook first", Toast.LENGTH_SHORT).show();
	       // }
	 }
	 public void video_upload1(){
		 
	 }
	 public void video_upload(){
		 File tempFile;
		 try {
		     tempFile = createTempFileFromAsset(GalleryActivity.this, recordfilename);
		     Request request5 = Request.newUploadVideoRequest(session,
		                     tempFile, callback5);
		     RequestAsyncTask task5 = new RequestAsyncTask(request5);
		     task5.execute();
		 } catch (IOException e) {
		     e.printStackTrace();
		 }
	 }
	 Request.Callback callback5 = new Request.Callback() {
		    public void onCompleted(Response response) {    
		        // response will have an id if successful
		    	String s = "get";
		    }
	};
	public static File createTempFileFromAsset(Context context, String assetPath) {
		    InputStream inputStream = null;
		    FileOutputStream outStream = null;

		    try {
		        //AssetManager assets = context.getResources().getAssets();
		        inputStream = new FileInputStream(assetPath);
		    	File outputFile = new File(context.getCacheDir(), "OrangeEye.mp4");
		        //File sendfile = new File(assetPath);
		        File outputDir = context.getCacheDir(); // context being the
		                                                // Activity pointer
		        //File outputFile = File.createTempFile("prefix", assetPath,
		       //         outputDir);
		        outStream = new FileOutputStream(outputFile);

		        final int bufferSize = 1024 * 2;
		        byte[] buffer = new byte[bufferSize];
		        int n = 0;
		        while ((n = inputStream.read(buffer)) != -1) {
		            outStream.write(buffer, 0, n);
		        }

		        return outputFile;
		    }catch(Exception e){
		    	e.fillInStackTrace();
		    	return null;
		    } finally {
		        //Utility.closeQuietly(outStream);
		       // Utility.closeQuietly(inputStream);
		    }
		}
		private void onSessionStateChange(Session session, SessionState state, Exception exception) {
			if (session != null && !upload_flg){
				if(state.isOpened()) {
				
					makeMeRequest(session);
				}
	    	}
	    }
		private void makeMeRequest(final Session session) {
			if(upload_flg){
        		return;
        	}
	        Request request = Request.newMeRequest(session, new MyGrpahUserCallBack(this, session,true));
	        request.executeAsync();
	    }
		class MyGrpahUserCallBack implements Request.GraphUserCallback
		{
			GalleryActivity mParentActivity;
			//Session session;
			boolean isNeedFBRegister=false;
			public MyGrpahUserCallBack(GalleryActivity context, Session session,boolean isNeedFBRegister){
				mParentActivity = context;
				mParentActivity.session = session;
			}
			
			@Override
		    public void onCompleted(GraphUser user, Response response) {
				
				
		        if (session == Session.getActiveSession() && !upload_flg) {
		            if (user != null) {
		            	mParentActivity.mFBUserInfo = user;
		            	facebook_video_upload(recordfilename);
		            }
		        }
		        
		        if (response.getError() != null) {
		           
		        }
		    }
			
		}
	public void facebook_check(){
		/*ShareData.selected_record_id = pos;
		Session session = new Session(this);
		List<String> permissions = new ArrayList<String>();
		Session.OpenRequest request = new Session.OpenRequest(this);
		//request.setCallback(callback);
		//session.openForRead(new Session.OpenRequest(this).setPermissions(Arrays.asList("email")));
		Session.setActiveSession(session);
		session.openForRead(request);
		*/
		Session session = new Session(GalleryActivity.this);
		List<String> permissions = new ArrayList<String>();
		permissions.add("email");
		Session.OpenRequest request = new Session.OpenRequest(GalleryActivity.this).setPermissions(permissions);
		request.setCallback(callback);
		//session.openForRead(new Session.OpenRequest(this).setPermissions(Arrays.asList("email")));
		
		Session.setActiveSession(session);
		session.openForRead(request);
	}
	public void facebook_upload(){
		//ByteArrayOutputStream stream = new ByteArrayOutputStream();
		//bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream); //compress to which format you want.
		
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httpost = new HttpPost("http://graph.facebook.com/me/videos");
		HttpContext localContext = new BasicHttpContext();
		MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		File file=new File(recordfilename);
		byte[] data = null;
		try {
			data = FileUtils.readFileToByteArray(file);
			entity.addPart("source", new ByteArrayBody(data, "OrangeEye.mp4"));
			entity.addPart("title", new StringBody("Record Video"));
			entity.addPart("Description", new StringBody("Uploaded from Orange Eye App"));
			httpost.setEntity(entity);
			HttpResponse response;
			 response = httpclient.execute(httpost,localContext);
			String s = convertResponseToString(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
	public String convertResponseToString(HttpResponse response) throws IllegalStateException, IOException{
		 
        String res = "";
        InputStream inputStream;
        StringBuffer buffer = new StringBuffer();
        inputStream = response.getEntity().getContent();
        int contentLength = (int) response.getEntity().getContentLength(); //getting content length�..
        
     
        if (contentLength < 0){
        }
        else{
               byte[] data = new byte[512];
               int len = 0;
               try
               {
                   while (-1 != (len = inputStream.read(data)) )
                   {
                       buffer.append(new String(data, 0, len)); //converting to string and appending  to stringbuffer�..
                   }
               }
               catch (IOException e)
               {
                   e.printStackTrace();
               }
               try
               {
                   inputStream.close(); // closing the stream�..
               }
               catch (IOException e)
               {
                   e.printStackTrace();
               }
               res = buffer.toString();     // converting stringbuffer to string�..

               
               //System.out.println("Response => " +  EntityUtils.toString(response.getEntity()));
        }
        return res;
	}
}
