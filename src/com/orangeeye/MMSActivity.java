package com.orangeeye;

import com.orangeeye.data.ShareData;
import com.orangeeye.db.DBAdapter;
import com.orangeeye.db.OtherData;
import com.orangeeye.lib.ReturnMessage;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class MMSActivity extends Activity implements OnClickListener{
	EditText emer_num, messegecontent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mms_screen);
        ((MyButton)findViewById(R.id.nextbt)).setOnClickListener(this);
		DBAdapter.init(this);
        OtherData setdata = DBAdapter.getOtherData("mms_data");
        emer_num = (EditText)findViewById(R.id.emer_num);
        messegecontent = (EditText)findViewById(R.id.messegecontent);
        if(setdata != null){
        	emer_num.setText(setdata.value);
        	messegecontent.setText(setdata.otherflg);
        }
        emer_num.setTypeface(Typeface.createFromAsset(getAssets(), this.getResources().getString(R.string.font_string)));
        messegecontent.setTypeface(Typeface.createFromAsset(getAssets(), this.getResources().getString(R.string.font_string)));
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.nextbt:
			if(emer_num.getText().toString().equals("")){
				//ReturnMessage.showAlartDialog(MMSActivity.this,"Invalid phone number", "Please key in a valid phone number.");
				//emer_num.setFocusable(true);
				//emer_num.requestFocus();
				confirmDialog(MMSActivity.this, "Emergency message and phone numbers", "Are you sure to proceed without entering these information?");
			}/*else if(messegecontent.getText().toString().equals("")){
				ReturnMessage.showAlartDialog(MMSActivity.this,"Emergency message and phone numbers", "Are you sure to proceed without entering these information?");
				messegecontent.setFocusable(true);
				messegecontent.requestFocus();
			}*/else{			
				String num = emer_num.getText().toString();
				String messege = messegecontent.getText().toString();
		        OtherData setdata = DBAdapter.getOtherData("mms_data");
		        if(setdata == null){
		        	OtherData other = new OtherData(0,"mms_data", num, messege);
		        	DBAdapter.addOtherData(other);
		        }else{
		        	OtherData other = new OtherData(setdata._id,"mms_data", num, messege);
		        	DBAdapter.updateOtherData(other);
		        }
				Intent intent = new Intent(MMSActivity.this, EmergencyCallActivity.class);
	        	startActivity(intent);
	        	finish();
			}
			break;
		}
	}

	public void confirmDialog(Activity activity, String title, String content) {
		new AlertDialog.Builder(activity)
				.setTitle(title)
				.setMessage(
						content)
				.setPositiveButton("YES",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
								save();
							}
						})
				.setNegativeButton("NO", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog4, int which) {
						// TODO Auto-generated method stub
						dialog4.cancel();
					}
				}).show();
	}
	public void save(){
		String num = emer_num.getText().toString();
		String messege = messegecontent.getText().toString();
        OtherData setdata = DBAdapter.getOtherData("mms_data");
        if(setdata == null){
        	OtherData other = new OtherData(0,"mms_data", num, messege);
        	DBAdapter.addOtherData(other);
        }else{
        	OtherData other = new OtherData(setdata._id,"mms_data", num, messege);
        	DBAdapter.updateOtherData(other);
        }
		Intent intent = new Intent(MMSActivity.this, EmergencyCallActivity.class);
    	startActivity(intent);
    	finish();
	}
    
}
