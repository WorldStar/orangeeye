package com.orangeeye.ffmpeg;
import java.util.LinkedList;
import java.util.List;

import android.os.Environment;
import android.text.TextUtils;


public class FfmpegJob1 {

	public String inputPath;
	
	public String inputFormat;
	public String inputVideoCodec;
	public String inputAudioCodec;
	
	public String videoCodec;
	public int videoBitrate;
	public int videoWidth = -1;
	public int videoHeight = -1;
	public float videoFramerate = -1;	
	public String videoBitStreamFilter;
	
	public String audioCodec;
	public int audioBitrate;
	public int audioSampleRate;
	public int audioChannels;
	public int audioVolume;
	public String audioBitStreamFilter;
	
	public String audioFilter;
	public String videoFilter;
	
	public long startTime = -1;
	public long duration = -1;
	
	public String outputPath;
	public String format;
	
	public boolean disableVideo;
	public boolean disableAudio;
	
	private final String mFfmpegPath;
	private final String mMarkPath;
	private final String recordfilename;
	private final String tempfilename;
	private final String mergefilename;
	private final String starttime;
	private final String endtime;
	private String bitrate = "1500k";//"1700k"
	public FfmpegJob1(String ffmpegPath, String markPath, String recordfilename, String tempfilename, String starttime, String endtime, String mergefilename) {
		mFfmpegPath = ffmpegPath;
		mMarkPath = markPath;
		this.recordfilename = recordfilename;
		this.tempfilename = tempfilename;
		this.starttime = starttime;
		this.endtime = endtime;
		this.mergefilename = mergefilename;
	}
	
	public ProcessRunnable create() {
		/*if (inputPath == null || outputPath == null) {
			throw new IllegalStateException("Need an input and output filepath!");
		}	*/
		
		final List<String> cmd = new LinkedList<String>();
		
		/*cmd.add("-y");
		
		
		cmd.add(outputPath);
		*/
		if(!mergefilename.equals("")){
			//ffmpeg -i concat:"input1|input2" -codec copy output
			//ffmpeg -i 1.mp4 -c copy -bsf dump_extra 1.ts
			if(mergefilename.contains("1.ts")){
				cmd.add(mFfmpegPath);
				cmd.add("-i");
				cmd.add(tempfilename);
				cmd.add("-c");
				cmd.add("copy");
				cmd.add("-bsf");
				cmd.add("dump_extra");
				cmd.add(mergefilename);
			}else if(mergefilename.contains("2.ts")){
				cmd.add(mFfmpegPath);
				cmd.add("-i");
				cmd.add(recordfilename);
				cmd.add("-c");
				cmd.add("copy");
				cmd.add("-bsf");
				cmd.add("dump_extra");
				cmd.add(mergefilename);
			}else{
				//ffmpeg -i "concat:1.ts|2.ts" -c copy output.mp4
				//file '/home/jenia/input1.mp4'
				//file '/home/jenia/input2.mp4'
				//file '/home/jenia/input3.mp4'
				//ffmpeg -f concat -i input.txt -codec copy output.mp4
				cmd.add(mFfmpegPath);
				cmd.add("-f");
				cmd.add("concat");
				cmd.add("-i");
				cmd.add(tempfilename);
				cmd.add("-codec");
				cmd.add("copy");
				cmd.add(mergefilename);
			}
		}else{
			if(!starttime.equals("")){
				cmd.add(mFfmpegPath);
				cmd.add("-i");
				cmd.add(tempfilename);
	
				if(!mMarkPath.equals("")){
					//cmd.add("-i");
					//cmd.add(tempfilename);
					cmd.add("-i");
					cmd.add(mMarkPath);
				}
				cmd.add(FFMPEGArg.ARG_STARTTIME);
				cmd.add(starttime);
				cmd.add(FFMPEGArg.ARG_DURATION);
				cmd.add(endtime);
				cmd.add("-async");
				cmd.add("1");
				cmd.add("-strict");
				cmd.add("-2");
				if(!mMarkPath.equals("")){
					//cmd.add("-i");
					//cmd.add(tempfilename);
					//cmd.add("-i");
					//cmd.add(mMarkPath);
					cmd.add("-b");
					cmd.add(bitrate);
					cmd.add("-filter_complex");
					cmd.add("'overlay'=x=0:y=0");
					cmd.add("-vcodec");
					cmd.add("mpeg4");
					
					//cmd.add(recordfilename);			
				}else{
					cmd.add("-b");
					cmd.add(bitrate);
				}
				cmd.add(recordfilename);
			}else{
				/*if(!mMarkPath.equals("")){
					cmd.add("-i");
					cmd.add(tempfilename);
					cmd.add("-i");
					cmd.add(mMarkPath);
					cmd.add("-filter_complex");
					cmd.add("'overlay'");
					
					cmd.add(recordfilename);
				
				}*/
	
				if(!mMarkPath.equals("")){
					//cmd.add("-i");
					//cmd.add(tempfilename);
					cmd.add(mFfmpegPath);
					cmd.add("-i");
					cmd.add(tempfilename);
					cmd.add("-i");
					cmd.add(mMarkPath);
					cmd.add("-b");
					cmd.add(bitrate);
					cmd.add("-filter_complex");
					cmd.add("'overlay'=x=0:y=0");
					cmd.add("-vcodec");
					cmd.add("mpeg4");
					//cmd.add("libx264");
					cmd.add(recordfilename);
				}
			}
		}
		//-i video.mkv -i image.png -filter_complex 'overlay'
		//ffmpeg -y -i /storage/sdcard0/test/hsgjr_export.mp4 -i /storage/sdcard0/test/watermark.png 
		//-filter_complex [0:v][1:v]overlay=main_w-overlay_w-10:main_h-overlay_h-10[out] 
		//-map [out] -map 0:a -codec:a copy /storage/sdcard0/test/hsgjr_watermark.mp4
		//ffmpeg -i alldone.mp4 -i cancel_bttn.png -filter_complex "overlay=main_w/2-overlay_w/2:main_h/2-overlay_h/2" record.mp4 "
		//cmd.add("-ss 00:00:00 -t 00:00:08 -i sdcard/OrangeEyeRecord/record.mp4 -acodec copy -vcodec copy sdcard/OrangeEyeRecord/record_temp.mp4");

		final ProcessBuilder pb = new ProcessBuilder(cmd);
		return new ProcessRunnable(pb);
	}
	
	public class FFMPEGArg {
		String key;
		String value;
		
		public static final String ARG_VIDEOCODEC = "-vcodec";
		public static final String ARG_AUDIOCODEC = "-acodec";
		
		public static final String ARG_VIDEOBITSTREAMFILTER = "-vbsf";
		public static final String ARG_AUDIOBITSTREAMFILTER = "-absf";
		
		public static final String ARG_VIDEOFILTER = "-vf";
		public static final String ARG_AUDIOFILTER = "-af";
		
		public static final String ARG_VERBOSITY = "-v";
		public static final String ARG_FILE_INPUT = "-i";
		public static final String ARG_SIZE = "-s";
		public static final String ARG_FRAMERATE = "-r";
		public static final String ARG_FORMAT = "-f";
		public static final String ARG_BITRATE_VIDEO = "-b:v";
		
		public static final String ARG_BITRATE_AUDIO = "-b:a";
		public static final String ARG_CHANNELS_AUDIO = "-ac";
		public static final String ARG_FREQ_AUDIO = "-ar";
		public static final String ARG_VOLUME_AUDIO = "-vol";
		
		public static final String ARG_STARTTIME = "-ss";
		public static final String ARG_DURATION = "-t";
		
		public static final String ARG_DISABLE_AUDIO = "-an";
		public static final String ARG_DISABLE_VIDEO = "-vn";
	}
}
