package com.orangeeye;

import com.orangeeye.db.DBAdapter;
import com.orangeeye.db.OtherData;
import com.orangeeye.lib.ReturnMessage;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Intents;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;

public class EmergencyNumberActivity extends Activity implements OnClickListener{
	Intent intent;
	EditText emer_num1, emer_call1, emer_messege;
	CheckBox box1, box2;
	OtherData setdata, setdata1 , setdata2;
	LinearLayout force_layout;
	public boolean contactflg = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.emergency_number_screen);
        ((MyButton)findViewById(R.id.savebt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.backbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.yes5bt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.no5bt)).setOnClickListener(this);
        force_layout = (LinearLayout)findViewById(R.id.force_layout);
        force_layout.setVisibility(View.GONE);
        emer_num1 = (EditText)findViewById(R.id.emer_num1);
        emer_call1 = (EditText)findViewById(R.id.emer_call1);
        emer_messege = (EditText)findViewById(R.id.emer_message);
        emer_call1.setTypeface(Typeface.createFromAsset(getAssets(), this.getResources().getString(R.string.font_string)));
        emer_messege.setTypeface(Typeface.createFromAsset(getAssets(), this.getResources().getString(R.string.font_string)));
        emer_num1.setTypeface(Typeface.createFromAsset(getAssets(), this.getResources().getString(R.string.font_string)));
        box1 = (CheckBox)findViewById(R.id.checkBox1);
        box1.setTypeface(Typeface.createFromAsset(getAssets(), this.getResources().getString(R.string.font_string)));
        box2 = (CheckBox)findViewById(R.id.checkBox2);
        box2.setTypeface(Typeface.createFromAsset(getAssets(), this.getResources().getString(R.string.font_string)));
        DBAdapter.init(this);
        box2.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked && contactflg){
					contactflg = true;
					SharedPreferences sharedPreferences = PreferenceManager
			                .getDefaultSharedPreferences(EmergencyNumberActivity.this);
			        
					String s = sharedPreferences.getString("contact_added","0");
			        //if(s.equals("0")){
			            force_layout.setVisibility(View.VISIBLE);
			        //}
				}
				if(isChecked)contactflg = true;
			}
        	
        });
        setdata = DBAdapter.getOtherData("mms_data");
        setdata1 = DBAdapter.getOtherData("emer_data");
        if(setdata != null){
        	emer_num1.setText(setdata.value);
        	emer_messege.setText(setdata.otherflg);
        }
        if(setdata1 != null){
        	emer_call1.setText(setdata1.value);
        	if(setdata1.otherflg.equals("1")){
        		box1.setChecked(true);
        	}
        }
        setdata2 = DBAdapter.getOtherData("ntuc_data");
        if(setdata2 != null){
        	if(setdata2.value.equals("1")){
        		box2.setChecked(true);
        	}else{
        		contactflg = true;
        	}
        }else{
        	contactflg = true;
        }
        WebView mWebView = null;
        mWebView = (WebView) findViewById(R.id.webView1);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl("file:///android_asset/html/tnc.html");
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.savebt:
			if(emer_call1.getText().toString().equals("") || emer_num1.getText().toString().equals("")){
				confirmDialog(EmergencyNumberActivity.this, "Emergency message and phone numbers", "Are you sure to proceed without entering these information?");
			}
            else {
                save();
            }

			break;
		case R.id.backbt:
			intent = new Intent(EmergencyNumberActivity.this, SettingsActivity.class);
			EmergencyNumberActivity.this.startActivity(intent);
        	finish();
			break;
		case R.id.yes5bt:
	        force_layout.setVisibility(View.GONE);
			SharedPreferences sharedPreferences = PreferenceManager
            .getDefaultSharedPreferences(EmergencyNumberActivity.this);
    
		        Editor editor = sharedPreferences.edit();
		        editor.putString("contact_added","1");
		        editor.commit();
				Intent intent = new Intent(Intents.Insert.ACTION);
		    	// Sets the MIME type to match the Contacts Provider
		    	intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
	        	intent.putExtra(Intents.Insert.NAME, "Orange Force");
		    	//intent.putExtra(Intents.Insert.PHONE_TYPE, Phone.TYPE_MOBILE);
		    	intent.putExtra(Intents.Insert.PHONE, "+6567895000");
		    	//intent.putExtra(Intents.Insert.PHONE_TYPE, Phone.TYPE_WORK);
		    	intent.putExtra(Intents.Insert.SECONDARY_PHONE, "+6590882947");
		    	startActivity(intent);
			break;
		case R.id.no5bt:
	        force_layout.setVisibility(View.GONE);
			
			break;
		}
	}

	 @Override
	 public void onBackPressed() {
		Intent intent3 = new Intent(EmergencyNumberActivity.this, SettingsActivity.class);
		startActivity(intent3);
		finish();
	 }

	public void confirmDialog(Activity activity, String title, String content) {
		new AlertDialog.Builder(activity)
				.setTitle(title)
				.setMessage(
						content)
				.setPositiveButton("YES",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
								save();
							}
						})
				.setNegativeButton("NO", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog4, int which) {
						// TODO Auto-generated method stub
						dialog4.cancel();
					}
				}).show();
	}
	public void save(){
		String num = emer_num1.getText().toString();
		String messege = emer_messege.getText().toString();
        if(setdata == null){
        	OtherData other = new OtherData(0,"mms_data", num, messege);
        	DBAdapter.addOtherData(other);
        }else{
        	OtherData other = new OtherData(setdata._id,"mms_data", num, messege);
        	DBAdapter.updateOtherData(other);
        }
		String call = emer_call1.getText().toString();
		String flg = "";
		if(box1.isChecked()){
			flg = "1";
		}else{
			flg = "0";
		}
        if(setdata1 == null){
        	OtherData other = new OtherData(0,"emer_data", call, flg);
        	DBAdapter.addOtherData(other);
        }else{
        	OtherData other = new OtherData(setdata1._id,"emer_data", call, flg);
        	DBAdapter.updateOtherData(other);
        }
        flg = "";
		if(box2.isChecked()){
			flg = "1";
		}else{
			flg = "0";
		}
        if(setdata2 == null){
        	OtherData other = new OtherData(0,"ntuc_data", flg, "");
        	DBAdapter.addOtherData(other);
        }else{
        	OtherData other = new OtherData(setdata2._id,"ntuc_data", flg, "");
        	DBAdapter.updateOtherData(other);
        }
		intent = new Intent(EmergencyNumberActivity.this, SettingsActivity.class);
		EmergencyNumberActivity.this.startActivity(intent);
    	finish();
	}
}
