package com.orangeeye;

import com.orangeeye.db.DBAdapter;
import com.orangeeye.db.OtherData;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;

import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

public class RecordingSettingActivity extends Activity implements OnClickListener{
	LinearLayout record_length_layout, setting_layout_1, setting_layout_2;
	Intent intent;
	boolean flg = false;
	MyTextView sizetx;
	OtherData other;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_setting_screen);
        ((MyButton)findViewById(R.id.backbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.min3bt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.min6bt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.min10bt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.min10bt)).setVisibility(View.GONE);
        ((MyButton)findViewById(R.id.min15bt)).setVisibility(View.GONE);
        ((MyButton)findViewById(R.id.min15bt)).setOnClickListener(this);
        record_length_layout = (LinearLayout)findViewById(R.id.record_length_layout);
        setting_layout_1 = (LinearLayout)findViewById(R.id.setting_layout_1);
        setting_layout_2 = (LinearLayout)findViewById(R.id.setting_layout_2);
        record_length_layout.setVisibility(View.GONE);
        setting_layout_1.setOnClickListener(this);
        setting_layout_2.setOnClickListener(this);
        sizetx = (MyTextView)findViewById(R.id.sizetx);
        try{
	        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
	        double sdAvailsize = (double)stat.getAvailableBlocks() * (double)stat.getBlockSize();
	        double megasize = sdAvailsize *1014 / 1073741824;
	        String size = "" + megasize;
	        int index = 0;
	        if((index = size.indexOf(".")) != -1){
	        	size = size.substring(0,index);
	        }
	        sizetx.setText(size + " MB");
        }catch(Exception e){
            sizetx.setText("");
        }
        DBAdapter.init(this);
        OtherData tmp = DBAdapter.getOtherData("record_time");
		if(tmp != null){
			((MyTextView)findViewById(R.id.timetx)).setText(tmp.value+" minutes");
			if(tmp.value.equals("2")){
				((MyButton)findViewById(R.id.min3bt)).setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_3min_active));
			}else if(tmp.value.equals("4")){
				((MyButton)findViewById(R.id.min6bt)).setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_6min_active));
			}else if(tmp.value.equals("10")){
				((MyButton)findViewById(R.id.min10bt)).setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_10min_active));
			}else if(tmp.value.equals("15")){
				((MyButton)findViewById(R.id.min15bt)).setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_15min_active));
			}
		}else{
			((MyTextView)findViewById(R.id.timetx)).setText("");			
		}
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.backbt:
			intent = new Intent(RecordingSettingActivity.this, SettingsActivity.class);
        	startActivity(intent);
        	finish();
			break;
		case R.id.min3bt:
			OtherData tmp = DBAdapter.getOtherData("record_time");
			if(tmp != null){
				other = new OtherData(tmp._id,"record_time", "2", "");
				DBAdapter.updateOtherData(other);
			}else{
				other = new OtherData(1,"record_time", "2", "");
				DBAdapter.addOtherData(other);
			}
			((MyTextView)findViewById(R.id.timetx)).setText("2 minutes");
			//record_length_layout.setVisibility(View.GONE);
			setButton("2");
			flg = false;
			break;
		case R.id.min6bt:
			OtherData tmp1 = DBAdapter.getOtherData("record_time");
			if(tmp1 != null){
				other = new OtherData(tmp1._id,"record_time", "4", "");
				DBAdapter.updateOtherData(other);
			}else{
				other = new OtherData(1,"record_time", "4", "");
				DBAdapter.addOtherData(other);
			}
			((MyTextView)findViewById(R.id.timetx)).setText("4 minutes");
			//record_length_layout.setVisibility(View.GONE);
			setButton("4");
			flg = false;
			break;
		case R.id.min10bt:
			OtherData tmp2 = DBAdapter.getOtherData("record_time");
			if(tmp2 != null){
				other = new OtherData(tmp2._id, "record_time", "10", "");
				DBAdapter.updateOtherData(other);
			}else{
				other = new OtherData(1, "record_time", "10", "");
				DBAdapter.addOtherData(other);
			}
			((MyTextView)findViewById(R.id.timetx)).setText("10 minutes");
			//record_length_layout.setVisibility(View.GONE);
			setButton("10");
			flg = false;
			break;
		case R.id.min15bt:
			OtherData tmp3 = DBAdapter.getOtherData("record_time");
			if(tmp3 != null){
				other = new OtherData(tmp3._id,"record_time", "15", "");
				DBAdapter.updateOtherData(other);
			}else{
				other = new OtherData(1, "record_time", "15", "");
				DBAdapter.addOtherData(other);
			}
			((MyTextView)findViewById(R.id.timetx)).setText("15 minutes");
			//record_length_layout.setVisibility(View.GONE);
			setButton("15");
			flg = false;
			break;
		case R.id.setting_layout_1:
			if(flg){
				record_length_layout.setVisibility(View.GONE);
				flg = false;
			}else{
				record_length_layout.setVisibility(View.VISIBLE);
				flg = true;
			}
			break;
		case R.id.setting_layout_2:
			
			break;
		}
	}
	public void setButton(String value){
		((MyButton)findViewById(R.id.min3bt)).setBackgroundDrawable(getResources().getDrawable(R.drawable.min3_bt));
		((MyButton)findViewById(R.id.min6bt)).setBackgroundDrawable(getResources().getDrawable(R.drawable.min6_bt));
		((MyButton)findViewById(R.id.min10bt)).setBackgroundDrawable(getResources().getDrawable(R.drawable.min10_bt));
		((MyButton)findViewById(R.id.min15bt)).setBackgroundDrawable(getResources().getDrawable(R.drawable.min15_bt));
		if(value.equals("2")){
			((MyButton)findViewById(R.id.min3bt)).setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_3min_active));
		}else if(value.equals("4")){
			((MyButton)findViewById(R.id.min6bt)).setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_6min_active));
		}else if(value.equals("10")){
			((MyButton)findViewById(R.id.min10bt)).setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_10min_active));
		}else if(value.equals("15")){
			((MyButton)findViewById(R.id.min15bt)).setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_15min_active));
		}
	}
	 @Override
	 public void onBackPressed() {
		Intent intent3 = new Intent(RecordingSettingActivity.this, SettingsActivity.class);
		startActivity(intent3);
		finish();
	 }
    
}
