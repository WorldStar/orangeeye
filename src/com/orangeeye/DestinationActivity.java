package com.orangeeye;

import com.orangeeye.data.ShareData;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

public class DestinationActivity extends Activity implements OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.destination_screen);
        ((MyButton)findViewById(R.id.yesbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.nobt)).setOnClickListener(this);
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.yesbt:
			Intent intent3 = new Intent(DestinationActivity.this, NavigationActivity.class);
        	startActivity(intent3);	
        	finish();
			break;
		case R.id.nobt:
			Intent intent = new Intent(DestinationActivity.this, RecordActivity.class);
        	startActivity(intent);	
        	finish();
			break;
		}
	}
	 @Override
	   public void onBackPressed() {
				Intent intent3 = new Intent(DestinationActivity.this, HomeActivity.class);
	        	startActivity(intent3);
				finish();
	   }

    
}
