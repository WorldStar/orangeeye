package com.orangeeye;

import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.CheckBox;

public class ToolkitActivity extends Activity implements OnClickListener{
	Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toolkit_screen);
        ((MyButton)findViewById(R.id.backbt)).setOnClickListener(this);
        WebView mWebView = null;
        mWebView = (WebView) findViewById(R.id.webView1);
        mWebView.getSettings().setJavaScriptEnabled(true);
        //mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.loadUrl("file:///android_asset/html/accident/accident-toolkit.html");
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.backbt:
			intent = new Intent(ToolkitActivity.this, HomeActivity.class);
        	startActivity(intent);
        	finish();
			break;
		}
	}

	 @Override
	 public void onBackPressed() {
		Intent intent3 = new Intent(ToolkitActivity.this, HomeActivity.class);
		startActivity(intent3);
		finish();
	 }
    
}
