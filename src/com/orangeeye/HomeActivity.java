package com.orangeeye;

import com.google.android.gcm.GCMRegistrar;
import com.orangeeye.data.ShareData;
import com.orangeeye.db.DBAdapter;
import com.orangeeye.db.OtherData;
import com.orangeeye.lib.CheckInternetAccess;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;
import com.orangeeye.utils.ReturnMessage;

import android.location.LocationManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.app.Activity;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class HomeActivity extends Activity implements OnClickListener{

	private long lastBackPressTime = 0;
	private Toast toast;
	public boolean destinationsetflg = false;
	Controller aController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen);
        //Get Global Controller Class object (see application tag in AndroidManifest.xml)
      	aController = (Controller) getApplicationContext();
      	//turnGpsOff(this);
        //turnGpsOn(this);
      	// Check if Internet present
 		if (!aController.isConnectingToInternet()) {
 			
 			// Internet Connection is not present
 			ReturnMessage.showAlartDialog(this, "No internet connection.");
 			// stop executing code by return
 			//return;
 		}	
 		// Make sure the device has the proper dependencies.
		GCMRegistrar.checkDevice(this);

		// Make sure the manifest permissions was properly set 
		GCMRegistrar.checkManifest(this);
		// Register custom Broadcast receiver to show messages on activity
		registerReceiver(mHandleMessageReceiver, new IntentFilter(
				Config.DISPLAY_MESSAGE_ACTION));
		
		// Get GCM registration id
		final String regId = GCMRegistrar.getRegistrationId(this);

		// Check if regid already presents
		if (regId.equals("")) {
			
			// Register with GCM			
			GCMRegistrar.register(this, Config.GOOGLE_SENDER_ID);
			
		}
        ((MyButton)findViewById(R.id.recordbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.navigatebt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.gallerybt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.motorbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.settingbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.dealsbt)).setOnClickListener(this);
        DBAdapter.init(this);
    	//screen lock
        //KeyguardManager keyguardManager = (KeyguardManager)getSystemService(Activity.KEYGUARD_SERVICE); 
        //KeyguardLock lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE); 
        //lock.disableKeyguard();
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        //WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "no sleep");
        //wakeLock.acquire();
        //android.provider.Settings.System.putInt(getContentResolver(),
               // Settings.System.SCREEN_OFF_TIMEOUT, -1);
    }
    String beforeEnable = "";
    private void turnGpsOn (Context context) {
        beforeEnable = Settings.Secure.getString (context.getContentResolver(),
                                                  Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        String newSet = String.format ("%s,%s",
                                       beforeEnable,
                                       LocationManager.GPS_PROVIDER);
        try {
            Settings.Secure.putString (context.getContentResolver(),
                                       Settings.Secure.LOCATION_PROVIDERS_ALLOWED,
                                       newSet); 
        } catch(Exception e) {}
    }


    private void turnGpsOff (Context context) {
        if (null == beforeEnable) {
            String str = Settings.Secure.getString (context.getContentResolver(),
                                                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if (null == str) {
                str = "";
            } else {                
                String[] list = str.split (",");
                str = "";
                int j = 0;
                for (int i = 0; i < list.length; i++) {
                    if (!list[i].equals (LocationManager.GPS_PROVIDER)) {
                        if (j > 0) {
                            str += ",";
                        }
                        str += list[i];
                        j++;
                    }
                }
                beforeEnable = str;
            }
        }
        try {
            Settings.Secure.putString (context.getContentResolver(),
                                       Settings.Secure.LOCATION_PROVIDERS_ALLOWED,
                                       beforeEnable);
        } catch(Exception e) {}
    }
	// Create a broadcast receiver to get message and show on screen 
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			
			String newMessage = intent.getExtras().getString(Config.EXTRA_MESSAGE);
			
			// Waking up mobile if it is sleeping
			aController.acquireWakeLock(getApplicationContext());
					
			
			//Toast.makeText(getApplicationContext(), "Got Message: " + newMessage, Toast.LENGTH_LONG).show();
			
			// Releasing wake lock
			aController.releaseWakeLock();
		}
	};

	@Override
	protected void onDestroy() {
		try {
			// Unregister Broadcast Receiver
			unregisterReceiver(mHandleMessageReceiver);
			
			//Clear internal resources.
			GCMRegistrar.onDestroy(this);
			
		} catch (Exception e) {
			Log.e("UnRegister Receiver Error", "> " + e.getMessage());
		}
		//android.provider.Settings.System.putInt(getContentResolver(),
               // Settings.System.SCREEN_OFF_TIMEOUT, 15000);
		super.onDestroy();
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.recordbt:
			//CheckInternetAccess check = new CheckInternetAccess(this);
	        //if (!check.checkNetwork()) {
	        	//ReturnMessage.showAlartDialog(this, "No internet connection.");
	        	//return;
	        //}
			OtherData tmp = DBAdapter.getOtherData("record_time");
			if(tmp != null){
				try{
					String record_time = tmp.value;
					if(record_time.equals(""))record_time = "0";
					ShareData.video_record_length = 1000 * 60 * Integer.parseInt(record_time);
					ShareData.des_addr = null;
					ShareData.destination = null;
					//if(ShareData.destination != null && ShareData.destination.latitude != 0.0){
						Intent intent = new Intent(HomeActivity.this, RecordActivity.class);
			        	startActivity(intent);
					//}else{
						//Intent intent11 = new Intent(HomeActivity.this, DestinationActivity.class);
			        	//startActivity(intent11);
					//}
				}catch(Exception e){
					Intent intent4 = new Intent(HomeActivity.this, VideoLengthActivity.class);
		        	startActivity(intent4);	
				}
			}else{
				Intent intent4 = new Intent(HomeActivity.this, VideoLengthActivity.class);
	        	startActivity(intent4);	
			}
			finish();
			break;
		case R.id.navigatebt:
			/*CheckInternetAccess check1 = new CheckInternetAccess(this);
	        if (!check1.checkNetwork()) {
	        	ReturnMessage.showAlartDialog(this, "Internet failed,Please Try Again");
	        	return;
	        }
			Intent intent3 = new Intent(HomeActivity.this, NavigationActivity.class);
        	startActivity(intent3);
			finish();
			*/
			Intent intent9 = new Intent(HomeActivity.this, ToolkitActivity.class);
        	startActivity(intent9);
			finish();
			break;
		case R.id.gallerybt:
			ShareData.selected_record_id = 0;
			Intent intent2 = new Intent(HomeActivity.this, GalleryPreActivity.class);
        	startActivity(intent2);
			finish();
			break;
		case R.id.motorbt:
			Intent intent = new Intent(HomeActivity.this, MotorActivity.class);
        	startActivity(intent);
			finish();
			break;
		case R.id.settingbt:
			Intent intent1 = new Intent(HomeActivity.this, SettingsActivity.class);
        	startActivity(intent1);
			finish();
			break;
		case R.id.dealsbt:
			CheckInternetAccess check1 = new CheckInternetAccess(this);
	        if (!check1.checkNetwork()) {
	        	ReturnMessage.showAlartDialog(this, "No internet connection.");
	        	return;
	        }
			Intent intent5 = new Intent(HomeActivity.this, DealsActivity.class);
        	startActivity(intent5);
			finish();
			break;
		}
	}
	@Override
	public void onResume(){
		/*if(ShareData.homeActive != null && (ShareData.homeActive.getTaskId() != this.getTaskId())){
			ShareData.homeActive.finish();
			ShareData.homeActive.finish();
		}
        ShareData.homeActive = this;*/
		super.onResume();
	}
	 @Override
    public void onBackPressed() {
		 if (this.lastBackPressTime < System.currentTimeMillis() - 3000) {
			this.lastBackPressTime = System.currentTimeMillis();
			toast = Toast.makeText(this, "Press back again to close this app", 3000);
			toast.show();
		} else {
			if (toast != null)
				toast.cancel();
			finish();
		}
    }
	public void facebookVideoUpload(){
		byte[] data = null;
		String dataPath = "/mnt/sdcard/KaraokeVideos/myvideo.3gp";
		String dataMsg = "Your video description here.";
		Bundle param;
		/*Facebook facebook = new Facebook(FB_APP_ID);
		AsyncFacebookRunner mAsyncRunner = new AsyncFacebookRunner(facebook);
		InputStream is = null;
		try {
		    is = new FileInputStream(dataPath);
		    data = readBytes(is);
		    param = new Bundle();
		    param.putString("message", dataMsg);
		    param.putByteArray("video", data);
		    mAsyncRunner.request("me/videos", param, "POST", new fbRequestListener(), null);
		}
		catch (FileNotFoundException e) {
		   e.printStackTrace();
		}
		catch (IOException e) {
		   e.printStackTrace();
		}*/
	}
    
}
