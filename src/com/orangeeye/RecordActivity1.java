package com.orangeeye;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.orangeeye.NavigationActivity.MyLocationListener;
import com.orangeeye.NavigationActivity.ParkAdapter;
import com.orangeeye.NavigationActivity.SearchAddrAdapter;
import com.orangeeye.data.Book;
import com.orangeeye.data.Parking;
import com.orangeeye.data.SearchAddr;
import com.orangeeye.data.ShareData;
import com.orangeeye.data.busStopCodeSet;
import com.orangeeye.db.DBAdapter;
import com.orangeeye.db.MySQLiteHelper;
import com.orangeeye.db.OtherData;
import com.orangeeye.db.RecordData;
import com.orangeeye.lib.AppSettings;
import com.orangeeye.lib.CheckInternetAccess;
import com.orangeeye.lib.Constants;
import com.orangeeye.lib.DirectionsJSONParser;
import com.orangeeye.lib.GPSCallback;
import com.orangeeye.lib.GPSManager;
import com.orangeeye.lib.GetSearchLocation;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;
import com.orangeeye.utils.ReturnMessage;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.MediaRecorder.OnErrorListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v4.app.FragmentActivity;
import android.telephony.SmsManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.format.DateFormat;
import android.text.style.AbsoluteSizeSpan;
import android.util.FloatMath;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class RecordActivity1 extends FragmentActivity implements GPSCallback, SensorEventListener, OnClickListener, LocationListener, OnItemClickListener{
	public boolean pauseflg = false;
	public boolean startflg = false;
	public boolean realstartflg = false;
	public boolean recordflg = false;
	LinearLayout accident_layout, recording_stop_layout, right_panel_layout, park_list_layout, slide_bt_layout, line_layout;
	GoogleMap mGoogleMap;
	ArrayList<LatLng> mMarkerPoints;
	double mLatitude=0;
    double mLongitude=0;
    LatLng point2;//3.127554,101.643402);
    LatLng point1;
    LatLng point;
    LatLng point3;
	LayoutInflater vi;
	List<busStopCodeSet> parklist = new ArrayList<busStopCodeSet>();

	long recordoldtime = 0;
    // camera
    private Camera myCamera;
    private MyCameraSurfaceView myCameraSurfaceView;
    private MediaRecorder mediaRecorder;
    private int frontFacingCameraID;
    SurfaceHolder surfaceHolder;
    FrameLayout myCameraPreview;
    
    ListView parkinglist;
	busStopCodeSet setitem;
    Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    int btwidth = 0;
    
    BroadcastReceiver _broadcastReceiver;
    private final SimpleDateFormat _sdfWatchTime = new SimpleDateFormat("HH:mm aa");
    int status = 0;
    //accident
	private boolean mInitialized;
	private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private final float NOISE = (float) 2.0;
    private static final float SHAKE_THRESHOLD_GRAVITY = 1.1f;//2.7F;
    private static final int SHAKE_SLOP_TIME_MS = 500;
    private static final int SHAKE_COUNT_RESET_TIME_MS = 3000;
    private long mShakeTimestamp = 0;
    private int mShakeCount = 0;
    float mLastX = 0, mLastY = 0, mLastZ= 0;
    boolean accidentflg = false;
    int accidentcount = 0;
    long lasttime = 0;
    long ACCIDENT_DETECT_TIME = 50;  //miliseconds
    int DETECT_COUNT = 3;
    boolean intentbackflg = false;
    autoSendThread sendthread;
    boolean sendflg = false;
    String km = "0";
    public int aspeed = 0;
    public long time1 = 0;
    public int MY_REQUEST_ID = 100;
    List<Location> _recentLocations = new ArrayList<Location>();
    Location _bestLocation ;
    public boolean recordtimeflg = false;
    public String newfilename = "";
    public boolean saveflg = false;
    BroadcastReceiver smsSentReceiver, smsDeliveredReceiver;
    long lastlocationtime = 0;
    long lastlocationtime1 = 0,currentlocationtime1 = 0;
    LatLng po1, po2;
    LatLng po11, po21;
    private GPSManager gpsManager = null;
    private int measurement_index = Constants.INDEX_KM;
    private AbsoluteSizeSpan sizeSpanLarge = null;
    private AbsoluteSizeSpan sizeSpanSmall = null;
    public boolean gpsflg = false;
    LocationManager mlocManager = null;
    float kk = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_screen);
        status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        //screen unlock
        KeyguardManager keyguardManager = (KeyguardManager)getSystemService(Activity.KEYGUARD_SERVICE); 
        KeyguardLock lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE); 
        lock.disableKeyguard();
        
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
        }
        ((MyButton)findViewById(R.id.alertbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.fullmapbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.fullrecordbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.backtorecordbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.carparkbt)).setClickable(false);
        ((MyButton)findViewById(R.id.splitbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.back1bt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.callbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.makecallbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.mmsbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.cancelbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.cancelbt2)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.resumerecordbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.saverecordbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.deleterecordbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.yes2bt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.no2bt)).setOnClickListener(this);
        ((LinearLayout)findViewById(R.id.destination_park_layout)).setOnClickListener(this);
        ((LinearLayout)findViewById(R.id.destination_park_layout)).setSelected(true);
        ((LinearLayout)findViewById(R.id.current_park_layout)).setOnClickListener(this);
        accident_layout = (LinearLayout)findViewById(R.id.accident_layout);
        recording_stop_layout = (LinearLayout)findViewById(R.id.RecordStopScreen_layout);
		recording_stop_layout.setVisibility(View.GONE);
		right_panel_layout = (LinearLayout)findViewById(R.id.right_panel_layout);
		right_panel_layout.setVisibility(View.GONE);
		park_list_layout = (LinearLayout)findViewById(R.id.park_list_layout);
		park_list_layout.setVisibility(View.GONE);
		slide_bt_layout = (LinearLayout)findViewById(R.id.slide_bt_layout);
		line_layout = (LinearLayout)findViewById(R.id.line_layout);
		parkinglist = (ListView)findViewById(R.id.parkinglist);
		btwidth = ((MyButton)findViewById(R.id.fullmapbt)).getWidth();
		vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		CheckInternetAccess check = new CheckInternetAccess(this);
        if (!check.checkNetwork()) {
        	ReturnMessage.showAlartDialog(this, "No internet connection.");
        	Intent intent = new Intent(RecordActivity1.this, HomeActivity.class);
        	startActivity(intent);
        	finish();
        }
		// displaying date
		displayDate();
		
		mInitialized = false;
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccelerometer , SensorManager.SENSOR_DELAY_NORMAL);
        
        
    }
    
    @Override
    public void onResume(){
    	//camera part

    	recordtimeflg = false;
		recordflg = false; startflg = false; pauseflg = false; realstartflg = false;gpsflg = false;
		myCamera = getCameraInstance();
        if (myCamera == null) {
        	//Toast.makeText(RecordActivity.this, "Failed to open camera. Please restart the app after close all the running apps.", Toast.LENGTH_LONG).show();
        	//Intent intent31 = new Intent(RecordActivity.this, HomeActivity.class);
			//startActivity(intent31);
			//finish();
            //Toast.makeText(RecordActivity.this, "Fail to get Camera", Toast.LENGTH_LONG).show();
        }
        myCameraSurfaceView = new MyCameraSurfaceView(this, myCamera);
        myCameraPreview = (FrameLayout)findViewById(R.id.videoview);

        android.view.ViewGroup.LayoutParams lp = myCameraPreview.getLayoutParams();

	    //Set the width of the SurfaceView to the width of the screen
	    lp.width = Value_Info.width;

	    //Set the height of the SurfaceView to match the aspect ratio of the video 
	    //be sure to cast these as floats otherwise the calculation will likely be 0
	    lp.height = Value_Info.height;
	    

	    //Commit the layout parameters
	    myCameraPreview.setLayoutParams(lp); 
        myCameraPreview.addView(myCameraSurfaceView);
        
       // myCameraPreview.setAddStatesFromChildren(true);
       // myCameraPreview.setFocusable(false);
        //myCameraPreview.setKeepScreenOn(false);
        //MySQLiteHelper db = new MySQLiteHelper(this);
        //db.addBook(new Book("Android Application Development Cookbook", "Wei Meng Lee"));
        DBAdapter.init(this);
        
        Thread RecordTimer = new Thread(){
    		@Override
    		public void run(){
    			try{
    				Thread.sleep(1000);
    				RecordActivity1.this.runOnUiThread(new Runnable(){
						@Override
						public void run(){
		    				if(realstartflg){
		        		        recordMake(true); // stoping record  					
		    				}
		    		        recordMake(false); // starting record  
						}
					});
    			}catch(Exception e){
    				Toast.makeText(RecordActivity1.this, "Can not record.", 500);
    			}
    		}
    	};
    	RecordTimer.start();

    	Thread DisplayTimer = new Thread(){
    		@Override
    		public void run(){
    			try{
    				while(!isInterrupted()){
    					if(recordoldtime != 0){
    						RecordActivity1.this.runOnUiThread(new Runnable(){
    							@Override
    							public void run(){
    								updateRecordTime();
    							}
    						});
    					}
    					Thread.sleep(300);
    				}
    			}catch(Exception e){
    				
    			}
    		}
    	};
    	DisplayTimer.start();
    	
        
    	
		// maps part
		if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available

            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();

        }else { 
			// Initializing 
			mMarkerPoints = new ArrayList<LatLng>();
			
			// Getting reference to SupportMapFragment of the activity_main
			SupportMapFragment fm = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
			
			// Getting Map for the SupportMapFragment
			mGoogleMap = fm.getMap();
			
			// Enable MyLocation Button in the Map
			mGoogleMap.setMyLocationEnabled(true);
			mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(10));
			// Getting LocationManager object from System Service LOCATION_SERVICE
			 mlocManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);  
		        //LocationListener mlocListener = new MyLocationListener();  
		        mlocManager.requestLocationUpdates( LocationManager.NETWORK_PROVIDER, 0, 0, this);  
		  
		        if (mlocManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {  
		            
		          } else {  
		              
		              Toast.makeText(this, "GPS is not turned on...", 500);
		          } 
	        /*LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
	
	        // Creating a criteria object to retrieve provider
	        Criteria criteria = new Criteria();
	
	        // Getting the name of the best provider
	        String provider = locationManager.getBestProvider(criteria, true);
	
	        // Getting Current Location From GPS
	        Location location = locationManager.getLastKnownLocation(provider);
	        */
        }		
		
		//accident 
		mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

		super.onResume();
		
		smsSentReceiver=new BroadcastReceiver() {
            
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                // TODO Auto-generated method stub
                switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "SMS has been sent", Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(getBaseContext(), "Generic Failure", Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(getBaseContext(), "No Service", Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(getBaseContext(), "Radio Off", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;
                }
                
            }
        };
        smsDeliveredReceiver=new BroadcastReceiver() {
            
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                // TODO Auto-generated method stub
                switch(getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "SMS Delivered", Toast.LENGTH_SHORT).show();
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(getBaseContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
                    break;
                }
            }
        };
        registerReceiver(smsSentReceiver, new IntentFilter("SMS_SENT"));
        registerReceiver(smsDeliveredReceiver, new IntentFilter("SMS_DELIVERED"));
    }
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
               .setCancelable(false)
               .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                   public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                       startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                   }
               })
               .setNegativeButton("No", new DialogInterface.OnClickListener() {
                   public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                   }
               });
        final AlertDialog alert = builder.create();
        alert.show();
    }
    @Override
    public void onPause() {
        super.onPause();
        releaseMediaRecorder();       // if you are using MediaRecorder, release it first
        releaseCamera();              // release the camera immediately on pause event
        mSensorManager.unregisterListener(this);
        unregisterReceiver(smsSentReceiver);
        unregisterReceiver(smsDeliveredReceiver);
    }

	@Override
	public void onSensorChanged(SensorEvent event) {
		float x = event.values[0];
		float y = event.values[1];
		float z = event.values[2];
		//float gX = x / SensorManager.GRAVITY_EARTH;
        //float gY = y / SensorManager.GRAVITY_EARTH;
        //float gZ = z / SensorManager.GRAVITY_EARTH;
        // gForce will be close to 1 when there is no movement.
        /*float gForce = FloatMath.sqrt(gX * gX + gY * gY + gZ * gZ);
        if (gForce > SHAKE_THRESHOLD_GRAVITY) {
            final long now = System.currentTimeMillis();
            // ignore shake events too close to each other (500ms)
            if (mShakeTimestamp + SHAKE_SLOP_TIME_MS > now ) {
                return;
            }

            // reset the shake count after 3 seconds of no shakes
            if (mShakeTimestamp + SHAKE_COUNT_RESET_TIME_MS < now ) {
                mShakeCount = 0;
            }

            mShakeTimestamp = now;
            mShakeCount++;
            Log.e("value", ""+ gForce);
            mSensorManager.get.onShake(mShakeCount);
        }*/
        long cutime = System.currentTimeMillis();
        if((cutime - lasttime) > ACCIDENT_DETECT_TIME){
			if (!mInitialized) {
				mLastX = x;
				mLastY = y;
				mLastZ = z;
				mInitialized = true;
			} else {
				float deltaX = Math.abs(mLastX - x);
				float deltaY = Math.abs(mLastY - y);
				float deltaZ = Math.abs(mLastZ - z);
				if (deltaX < NOISE) deltaX = (float)0.0;
				if (deltaY < NOISE) deltaY = (float)0.0;
				if (deltaZ < NOISE) deltaZ = (float)0.0;
				mLastX = x;
				mLastY = y;
				mLastZ = z;
				float realvalue = deltaX + deltaY + deltaZ;
				if(realvalue > 24 && !accidentflg){
					accidentcount ++;
				}

				if(myCamera != null){
					if(!accidentflg){
						if(accidentcount > DETECT_COUNT){
							accidentcount = 0;
							accidentflg = true;
							//accident ocurr
							accident_layout.setVisibility(View.VISIBLE);
							
							OtherData accidata = DBAdapter.getOtherData("ntuc_data");
							if(accidata == null || !accidata.value.equals("1")){
								((MyButton)findViewById(R.id.callbt)).setVisibility(View.GONE);
							}else{
								((MyButton)findViewById(R.id.callbt)).setVisibility(View.VISIBLE);
							}
							OtherData emerdata = DBAdapter.getOtherData("emer_data");
							if(emerdata != null && emerdata.otherflg.equals("1")){
								//autoSendMMS();
							}
							RecordActivity1.this.runOnUiThread(new Runnable(){
								@Override
								public void run(){
									processAccidentRecord();	
								}
							});				
						}else{
							//accidentflg = false;
						}
					}
				}
			}
        }
	}
    public void displayDate(){
    	Date today = new Date();
    	SimpleDateFormat sourceFormat = new SimpleDateFormat("EEEEEEEEE, dd MMMM yyyy"); 
		String datestr = "";
		long fromtime = 0;
		try {
			datestr = sourceFormat.format(today);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		((MyTextView)findViewById(R.id.realdate)).setText(datestr);


        registerReceiver(_broadcastReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
    }
    public void getDirectionForMaps(){
    	try{
	    	if(ShareData.destination != null){
	    		
	    	}
			if(ShareData.destination != null){
				
				mGoogleMap.clear();
		    	mMarkerPoints.clear();	    	
				point2 = ShareData.destination;
				ShareData.desti_pos = point2;
				SearchAddr addr = ShareData.des_addr;
				String addrname = addr.addressname;
				if(addrname.length() > 20){
					addrname = addrname.substring(0,20) + "...";
				}
				((MyTextView)findViewById(R.id.MyTextView03)).setText("Destination");
				((MyTextView)findViewById(R.id.place)).setText(addrname);
				((MyTextView)findViewById(R.id.distance)).setText(addr.dis + " TO GO");
				((MyTextView)findViewById(R.id.speed)).setText(km);
				drawMarker(point2);
				// Getting URL to the Google Directions API
				String url = getDirectionsUrl(point1, point2);				
				
				DownloadTask downloadTask = new DownloadTask();
				
				// Start downloading json data from Google Directions API
				downloadTask.execute(url);
				// Setting onclick event listener for the map
				//mGoogleMap.setTrafficEnabled(true);
				mGoogleMap.setOnMapClickListener(new OnMapClickListener() {
					
					@Override
					public void onMapClick(LatLng point) {
						
						// Already map contain destination location	
						/*if(mMarkerPoints.size()>1){
							
							FragmentManager fm = getSupportFragmentManager();	
							mMarkerPoints.clear();
							mGoogleMap.clear();
							LatLng startPoint = new LatLng(mLatitude, mLongitude);
							drawMarker(startPoint);
						}
						
						drawMarker(point);
						
						// Checks, whether start and end locations are captured
						if(mMarkerPoints.size() >= 2){					
							LatLng origin = mMarkerPoints.get(0);
							LatLng dest = mMarkerPoints.get(1);
							
							// Getting URL to the Google Directions API
							String url = getDirectionsUrl(origin, dest);				
							
							DownloadTask downloadTask = new DownloadTask();
							
							// Start downloading json data from Google Directions API
							downloadTask.execute(url);
						}	*/				
					}
				});	
			}
    	}catch(Exception e){
    		
    	}
    }
    private boolean processAccidentRecord(){
    	if(realstartflg){
	    	releaseMediaRecorder1();
			    //releaseCamera();
	    	saveflg = true;
			copyTempFile();
			if (!prepareMediaRecorder()){
	            Toast.makeText(RecordActivity1.this, "Failed to open camera. Please restart the app after close all the running apps.", Toast.LENGTH_LONG).show();
	            //finish();
	        }
			if(myCamera != null && mediaRecorder != null){
				mediaRecorder.start();
        	}else{
        		Toast.makeText(RecordActivity1.this, "Failed to open camera. Please restart the app after close all the running apps.", Toast.LENGTH_LONG).show();
            		Intent intent31 = new Intent(RecordActivity1.this, HomeActivity.class);
    				startActivity(intent31);
    				finish();
        	}
	        //
	        
    	}else{
    		recordMake(false);
    	}
        return true;
    }
    private boolean recordMake(boolean flg){
    	if(flg){
			//stop record  and then button icon change and recording process layout show
			if(recordflg){
                // stop recording and release camera
				if(mediaRecorder != null)
					mediaRecorder.stop();  // stop the recording
                releaseMediaRecorder(); // release the MediaRecorder object
                releaseCamera();  
                //Exit after saved
                recordflg = false;
                startflg = false;
                realstartflg = false;
                pauseflg = false;
			}
		}else{
        	if(isSDPresent){
                //Release Camera before MediaRecorder start
        		File dir = new File(Environment.getExternalStorageDirectory() + "/OrangeEyeRecord/");
                
    			//Environment.getExternalStorageDirectory() + 
	    	 	if (!dir.exists())
	    	 	{
	    			dir.mkdirs();
	    		}
                releaseCamera();

                if (!prepareMediaRecorder()){
                	Toast.makeText(RecordActivity1.this, "Failed to open camera. Please restart the app after close all the running apps.", Toast.LENGTH_LONG).show();
            		Intent intent31 = new Intent(RecordActivity1.this, HomeActivity.class);
    				startActivity(intent31);
    				finish();
                }else{
                	if(myCamera != null && mediaRecorder != null){
                		mediaRecorder.start();
                	}else{
                		Toast.makeText(RecordActivity1.this, "Failed to open camera. Please restart the app after close all the running apps.", Toast.LENGTH_LONG).show();
                		Intent intent31 = new Intent(RecordActivity1.this, HomeActivity.class);
        				startActivity(intent31);
        				finish();
                	}
                }                

	            realstartflg = true;
                recordflg = true;
                pauseflg = true;
        	}else{
        		Toast.makeText(RecordActivity1.this, "You can not record video. Please insert SDCard.",Toast.LENGTH_LONG).show();
        	}

		}
    	return true;
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.stopbt:
			if(realstartflg){
				//stop record  and then button icon change and recording process layout show
				if(recordflg){
	                // stop recording and release camera
	                /*mediaRecorder.stop();  // stop the recording
	                releaseMediaRecorder(); // release the MediaRecorder object
	
	                //Exit after saved
	                recordflg = false;
	                startflg = false;
	                realstartflg = false;
	                pauseflg = false;*/
					recording_stop_layout.setVisibility(View.VISIBLE);
				}
			}else{
	        	/*if(isSDPresent){
	                //Release Camera before MediaRecorder start
	        		File dir = new File(Environment.getExternalStorageDirectory() + "/OrangeEyeRecord/");
	                
	    			//Environment.getExternalStorageDirectory() + 
		    	 	if (!dir.exists())
		    	 	{
		    			dir.mkdirs();
		    		}
	                releaseCamera();

	                if (!prepareMediaRecorder()){
	                    Toast.makeText(RecordActivity.this, "Camera can not use.", Toast.LENGTH_LONG).show();
	                    //finish();
	                }

	                mediaRecorder.start();

		            realstartflg = true;
	                recordflg = true;
	                pauseflg = true;
	        	}else{
	        		Toast.makeText(RecordActivity.this, "You can not record video. Please insert SDCard.",Toast.LENGTH_LONG).show();
	        	}*/
				recordMake(false);
			}
			break;
		case R.id.alertbt:
			if(myCamera != null){
				accident_layout.setVisibility(View.VISIBLE);
				OtherData accidata = DBAdapter.getOtherData("ntuc_data");
				if(accidata == null || !accidata.value.equals("1")){
					((MyButton)findViewById(R.id.callbt)).setVisibility(View.GONE);
				}else{
					((MyButton)findViewById(R.id.callbt)).setVisibility(View.VISIBLE);
				}	
				RecordActivity1.this.runOnUiThread(new Runnable(){
					@Override
					public void run(){
						if(realstartflg)
							//recordMake(true);
	
						//recordMake(false);
						accidentcount = 0;
						accidentflg = true;
					}
				});
				OtherData emerdata = DBAdapter.getOtherData("emer_data");
				if(emerdata != null && emerdata.otherflg.equals("1")){
					//autoSendMMS();
				}
			}
			break;
		case R.id.fullmapbt:
			((MyButton)findViewById(R.id.splitbt)).setVisibility(View.GONE);
			slide_bt_layout.setVisibility(View.GONE);
			line_layout.setVisibility(View.GONE);
			((MyButton)findViewById(R.id.splitbt)).setVisibility(View.GONE);
			right_panel_layout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
			right_panel_layout.setVisibility(View.VISIBLE);
			
			//camera
			android.view.ViewGroup.LayoutParams lp = myCameraPreview.getLayoutParams();

		    //Set the width of the SurfaceView to the width of the screen
		    lp.width = 3;

		    //Set the height of the SurfaceView to match the aspect ratio of the video 
		    //be sure to cast these as floats otherwise the calculation will likely be 0
		    lp.height = Value_Info.height;

		    //Commit the layout parameters
		    myCameraPreview.setLayoutParams(lp);
			((MyButton)findViewById(R.id.backtorecordbt)).setVisibility(View.VISIBLE);
			break;
		case R.id.fullrecordbt:
			((MyButton)findViewById(R.id.splitbt)).setVisibility(View.VISIBLE);
			right_panel_layout.setVisibility(View.GONE);
			//camera
			android.view.ViewGroup.LayoutParams lp1 = myCameraPreview.getLayoutParams();

		    //Set the width of the SurfaceView to the width of the screen
		    lp1.width = android.view.ViewGroup.LayoutParams.FILL_PARENT;

		    //Set the height of the SurfaceView to match the aspect ratio of the video 
		    //be sure to cast these as floats otherwise the calculation will likely be 0
		    lp1.height = android.view.ViewGroup.LayoutParams.FILL_PARENT;

		    //Commit the layout parameters
		    myCameraPreview.setLayoutParams(lp1);
			break;
		case R.id.backtorecordbt:
			((MyButton)findViewById(R.id.splitbt)).setVisibility(View.VISIBLE);
			right_panel_layout.setVisibility(View.GONE);
			slide_bt_layout.setVisibility(View.VISIBLE);
			line_layout.setVisibility(View.VISIBLE);
			//camera
			android.view.ViewGroup.LayoutParams lp3 = myCameraPreview.getLayoutParams();

		    //Set the width of the SurfaceView to the width of the screen
		    lp3.width = Value_Info.width;

		    //Set the height of the SurfaceView to match the aspect ratio of the video 
		    //be sure to cast these as floats otherwise the calculation will likely be 0
		    lp3.height = Value_Info.height;

		    //Commit the layout parameters
		    myCameraPreview.setLayoutParams(lp3);
			break;
		case R.id.carparkbt:
			//((MyButton)findViewById(R.id.splitbt)).setVisibility(View.GONE);

			if(point2 != null && point2.latitude != 0){
				park_list_layout.setVisibility(View.VISIBLE);
				point = point2;
				new GetParkTask().execute();
			}else{
				park_list_layout.setVisibility(View.VISIBLE);
				point = point1;
				new GetParkTask().execute();
				((LinearLayout)findViewById(R.id.destination_park_layout)).setSelected(false);
				((LinearLayout)findViewById(R.id.current_park_layout)).setSelected(true);
			}
			break;
		case R.id.current_park_layout:
			parklist.clear();
			ParkAdapter adapter1=new ParkAdapter(RecordActivity1.this, R.layout.navigation_park_list, parklist);
			
			parkinglist.setAdapter(adapter1);
			parkinglist.setSmoothScrollbarEnabled(true);
			parkinglist.setOnItemClickListener(RecordActivity1.this);
			//((LinearLayout)findViewById(R.id.destination_park_layout)).setBackgroundColor(getResources().getColor(R.color.gray));
			//((LinearLayout)findViewById(R.id.current_park_layout)).setBackgroundColor(getResources().getColor(R.color.blue_trans));
			((LinearLayout)findViewById(R.id.destination_park_layout)).setSelected(false);
			((LinearLayout)findViewById(R.id.current_park_layout)).setSelected(true);
			//((LinearLayout)findViewById(R.id.current_park_layout)).setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_parking_nearuser_on));
			if(point3 != null && point3.latitude != 0){
				point1 = point3;
				point = point1;
				new GetParkTask().execute();
			}else if(point1 != null && point1.latitude != 0){
				
				//point = new LatLng(1.298977, 103.809088);
				point = point1;
				new GetParkTask().execute();
			}
			break;
		case R.id.destination_park_layout:
			parklist.clear();
			ParkAdapter adapter=new ParkAdapter(RecordActivity1.this, R.layout.navigation_park_list, parklist);
			
			parkinglist.setAdapter(adapter);
			parkinglist.setSmoothScrollbarEnabled(true);
			parkinglist.setOnItemClickListener(RecordActivity1.this);
			//((LinearLayout)findViewById(R.id.current_park_layout)).setBackgroundColor(getResources().getColor(R.color.gray));
			//((LinearLayout)findViewById(R.id.destination_park_layout)).setBackgroundColor(getResources().getColor(R.color.blue_trans));
			((LinearLayout)findViewById(R.id.destination_park_layout)).setSelected(true);
			((LinearLayout)findViewById(R.id.current_park_layout)).setSelected(false);
			if(point2 != null && point2.latitude != 0){
				point = point2;
				new GetParkTask().execute();
			}else{
				parkinglist.setVisibility(View.GONE);
				((MyTextView)findViewById(R.id.noparklist)).setVisibility(View.VISIBLE);
			}
			break;
		case R.id.back1bt:
			//((MyButton)findViewById(R.id.splitbt)).setVisibility(View.VISIBLE);
			park_list_layout.setVisibility(View.GONE);
			break;
		case R.id.yes2bt:
			try{
				new GetYesTask().execute();
			}catch(Exception e){}
			break;
		case R.id.no2bt:
			((LinearLayout)findViewById(R.id.destination_setting_layout)).setVisibility(View.GONE);
			break;
		case R.id.splitbt:
		    
		    
			((MyButton)findViewById(R.id.splitbt)).setVisibility(View.GONE);
			right_panel_layout.setLayoutParams(new LinearLayout.LayoutParams(Value_Info.width / 2 + 60, LayoutParams.FILL_PARENT));
			right_panel_layout.setVisibility(View.VISIBLE);
			

			//camera
			android.view.ViewGroup.LayoutParams lp2 = myCameraPreview.getLayoutParams();

		    //Set the width of the SurfaceView to the width of the screen
		    lp2.width = Value_Info.width / 2;

		    //Set the height of the SurfaceView to match the aspect ratio of the video 
		    //be sure to cast these as floats otherwise the calculation will likely be 0
		    lp2.height = Value_Info.height;

		    //Commit the layout parameters
		    myCameraPreview.setLayoutParams(lp2);
			((MyButton)findViewById(R.id.backtorecordbt)).setVisibility(View.GONE);
			break;
		case R.id.callbt:// Call Orange Force
			accidentflg = false;
			saveflg = true;
			if(sendthread != null){
				sendflg = true;
			}
			recordMake(true);
			copyTempFile();
	        intentbackflg = true;
	        accident_layout.setVisibility(View.GONE);
	        Intent intent3 = new Intent(RecordActivity1.this, HomeActivity.class);
        	startActivity(intent3);
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        callIntent.setData(Uri.parse("tel:+6567895000"));
	        startActivity(callIntent);
	        finish();
	        //setResult(Activity.RESULT_OK, callIntent);
			break;
		case R.id.makecallbt: // emergencycall
			accidentflg = false;
			recordMake(true);
			if(sendthread != null){
				sendflg = true;
			}
	        intentbackflg = true;
			OtherData accidata1 = DBAdapter.getOtherData("emer_data");
			if(accidata1 == null || accidata1.value.equals("")){
				//ReturnMessage.showAlartDialog(RecordActivity.this, "There is not emergency number.");
				saveflg = true;
				copyTempFile();
				Intent intent31 = new Intent(RecordActivity1.this, HomeActivity.class);
				startActivity(intent31);
				//Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
		        //callIntent1.setData(Uri.parse("tel:+65"+accidata1.value));
		        //startActivity(intent);
				Intent dial = new Intent();
				dial.setAction("android.intent.action.DIAL");
				//dial.setData(Uri.parse("tel:"+dial_number));
				startActivity(dial); 
		        finish();
			}else{
				saveflg = true;
				copyTempFile();
				Intent intent31 = new Intent(RecordActivity1.this, HomeActivity.class);
				startActivity(intent31);
				Intent callIntent1 = new Intent(Intent.ACTION_CALL);
		        callIntent1.setData(Uri.parse("tel:+65"+accidata1.value));//
		        startActivity(callIntent1);
		        finish();
			}
	        accident_layout.setVisibility(View.GONE);
			break;
		case R.id.mmsbt:
			saveflg = true;
			((MyButton)findViewById(R.id.mmsbt)).setBackgroundDrawable(getResources().getDrawable(R.drawable.mms_orange_bt));
			if(sendthread != null){
				sendflg = true;
			}
			recordMake(true);
			copyTempFile();
			sendSMS();
	        accident_layout.setVisibility(View.GONE);
			break;
		case R.id.cancelbt:
			accidentflg = false;
			if(sendthread != null){
				sendflg = true;
			}
			if(intentbackflg){
				recordMake(true);
				recordMake(false);
				intentbackflg = false;
			}
			accident_layout.setVisibility(View.GONE);
			break;
		case R.id.cancelbt2:
			recording_stop_layout.setVisibility(View.GONE);
			break;
		case R.id.resumerecordbt:
            //recordMake(true);
			//recordMake(false);
			recording_stop_layout.setVisibility(View.GONE);
			//Intent intent1 = new Intent(RecordActivity.this, HomeActivity.class);
        	//startActivity(intent1);
        	//finish();
			break;
		case R.id.saverecordbt:
			saveflg = true;
            recordMake(true);
			copyTempFile();
			recording_stop_layout.setVisibility(View.GONE);
			Intent intent2 = new Intent(RecordActivity1.this, HomeActivity.class);
        	startActivity(intent2);
        	finish();
			break;
		case R.id.deleterecordbt:
            recordMake(true);			
			recording_stop_layout.setVisibility(View.GONE);
			Intent intent31 = new Intent(RecordActivity1.this, HomeActivity.class);
        	startActivity(intent31);
        	finish();
			break;
		}
	}
	protected void onActivityResult(int requestCode, int resultCode,
	          Intent data) {
	      if (requestCode == MY_REQUEST_ID) {
	          //if (resultCode == RESULT_OK) {
	            //String myValue = data.getStringExtra("valueName"); 
	            Intent intent3 = new Intent(RecordActivity1.this, HomeActivity.class);
	        	startActivity(intent3);
				finish();
	            // use 'myValue' return value here
	          //}
	      }
	}
	// part adapter
	/*public class ParkAdapter extends ArrayAdapter<Parking>
	{
		private ArrayList<Parking>items;
		public ParkAdapter(Context context, int textViewResourceId,
				ArrayList<Parking> objects) {
			
			super(context, textViewResourceId, objects);
			this.items=objects;
			// TODO Auto-generated constructor stub
		}
		
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View v=convertView;
			LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final Parking item=items.get(position);
			if(v==null)
			{
				v = vi.inflate(R.layout.navigation_park_list, null);
			}
			return v;
		}
		
	}*/


    @Override
    public void onGPSUpdate(Location location) 
    {
            location.getLatitude();
            location.getLongitude();
            float speed1 = location.getSpeed();
            
            String speedString = "" + roundDecimal(convertSpeed(speed1),2);
            int idx = 0;
            if((idx = speedString.indexOf(".")) != -1){
            	speedString = speedString.substring(0, idx);
            }
            km = speedString;
            ((MyTextView)findViewById(R.id.speed)).setText(km);
            avaSpeed(location);
            //String unitString = measurementUnitString(measurement_index);
            
            //setSpeedText(R.id.info_message,speedString + " " + unitString);
            //////////////////////////////////////////
            if(location == null){
            	mLatitude = 3.127554;
            	mLongitude = 101.643402;
            	ReturnMessage.showAlartDialog(RecordActivity1.this, "GPS no working. Can not find current location.");
            }else{
            	mLatitude = location.getLatitude();
            	mLongitude = location.getLongitude();
            }
            point1 = new LatLng(mLatitude, mLongitude);
            
        	String url = getDirectionsUrl(point1, point1);				
    		
    		DownloadTask1 downloadTask = new DownloadTask1();
    		
    		// Start downloading json data from Google Directions API
    		downloadTask.execute(url);
    		
    		
    		point3 = new LatLng(location.getLatitude(), location.getLongitude());
    		if(mMarkerPoints.size() < 2){
    			
    			mLatitude = location.getLatitude();
    	        mLongitude = location.getLongitude();
    	        LatLng point = new LatLng(mLatitude, mLongitude);
    	
    	        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
    	        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));        
            
            	drawMarker(point);			
            } 
    		gpsflg = true;

    		///////////////////////////////////
    }
    private void avaSpeed(Location location){
    	if (location.getAccuracy() < 25f) {
		    _recentLocations.add(location);

		    if (_bestLocation == null || location.getAccuracy() <= _bestLocation.getAccuracy())
		        _bestLocation = location;
		}

		if ((_bestLocation != null && _bestLocation.getAccuracy() < 10f && _recentLocations.size() >= 10)
		        || _recentLocations.size() >= 25)
		{
		    int Count = 0;
		        float TotalSpeed = 0f;
		        float AverageSpeed = 0f;
		        for (int i = 0; i<_recentLocations.size(); i++) {
		            if (_recentLocations.get(i).hasSpeed()) {
		                Count++;
		                TotalSpeed += _recentLocations.get(i).getSpeed();
		            }
		        }

		    if (Count > 0)
		    	aspeed = (int) (((TotalSpeed / Count)*3600)/1000);
		    }
    }
	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		// Draw the marker, if destination location is not set
		if(location == null){
        	mLatitude = 3.127554;
        	mLongitude = 101.643402;
        	ReturnMessage.showAlartDialog(RecordActivity1.this, "GPS no working. Can not find current location.");
        }else{
        	mLatitude = location.getLatitude();
        	mLongitude = location.getLongitude();
        }
		mlocManager.removeUpdates(this);
        point1 = new LatLng(mLatitude, mLongitude);
        
    	String url = getDirectionsUrl(point1, point1);				
		
		DownloadTask1 downloadTask = new DownloadTask1();
		
		// Start downloading json data from Google Directions API
		downloadTask.execute(url);
		
		
		point3 = new LatLng(location.getLatitude(), location.getLongitude());
		if(mMarkerPoints.size() < 2){
			
			mLatitude = location.getLatitude();
	        mLongitude = location.getLongitude();
	        LatLng point = new LatLng(mLatitude, mLongitude);
	
	        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
	        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));        
        
        	drawMarker(point);			
        } 
		//long currentlocationtime = System.currentTimeMillis();
		//po2 = new LatLng(mLatitude, mLongitude);
		//if((currentlocationtime - lastlocationtime) > 3000){
			//getSpeedForLocation(currentlocationtime, lastlocationtime, po1, po2);
		//}
		//lastlocationtime = currentlocationtime;
		//po1 = po2;
		/*int speed=(int) ((location.getSpeed()*3600)/1000);
		//aspeed = (aspeed + speed) / 2;
		km = ""+ speed;
		((MyTextView)findViewById(R.id.speed)).setText(km);
		
		if (location.getAccuracy() < 25f) {
		    _recentLocations.add(location);

		    if (_bestLocation == null || location.getAccuracy() <= _bestLocation.getAccuracy())
		        _bestLocation = location;
		}

		if ((_bestLocation != null && _bestLocation.getAccuracy() < 10f && _recentLocations.size() >= 10)
		        || _recentLocations.size() >= 25)
		{
		    int Count = 0;
		        float TotalSpeed = 0f;
		        float AverageSpeed = 0f;
		        for (int i = 0; i<_recentLocations.size(); i++) {
		            if (_recentLocations.get(i).hasSpeed()) {
		                Count++;
		                TotalSpeed += _recentLocations.get(i).getSpeed();
		            }
		        }

		    if (Count > 0)
		    	aspeed = (int) (((TotalSpeed / Count)*3600)/1000);
		    }
		    */
		if(gpsManager == null){
			gpsManager = new GPSManager();
	        
	        gpsManager.startListening(getApplicationContext());
	        gpsManager.setGPSCallback(this);
	        measurement_index = AppSettings.getMeasureUnit(this);
		}
        ((MyButton)findViewById(R.id.carparkbt)).setClickable(true);
        ((MyButton)findViewById(R.id.carparkbt)).setOnClickListener(this);
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
	public void getSpeedForLocation(long currentlocationtime1, long lastlocationtime1, LatLng po11, LatLng po21){
		this.lastlocationtime1 = lastlocationtime1;
		this.currentlocationtime1 = currentlocationtime1;
		this.po11 = po11;
		this.po21 = po21;
		new GetSpeedTask().execute();
	}
	private String getDirectionsUrl(LatLng origin,LatLng dest){
					
		// Origin of route
		String str_origin = "origin="+origin.latitude+","+origin.longitude;
		
		// Destination of route
		String str_dest = "destination="+dest.latitude+","+dest.longitude;			
					
		// Sensor enabled
		String sensor = "sensor=false";			
					
		// Building the parameters to the web service
		String parameters = str_origin+"&"+str_dest+"&"+sensor;
					
		// Output format
		String output = "json";
		
		// Building the url to the web service
		String url = "http://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;		
		
		return url;
	}
	
	/** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException{
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
                URL url = new URL(strUrl);

                // Creating an http connection to communicate with url 
                urlConnection = (HttpURLConnection) url.openConnection();

                // Connecting to url 
                urlConnection.connect();

                // Reading data from url 
                iStream = urlConnection.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

                StringBuffer sb  = new StringBuffer();

                String line = "";
                while( ( line = br.readLine())  != null){
                        sb.append(line);
                }
                
                data = sb.toString();

                br.close();

        }catch(Exception e){
                Log.d("Exception while downloading url", e.toString());
        }finally{
                iStream.close();
                urlConnection.disconnect();
        }
        return data;
     }

	
	
    /** A class to download data from Google Directions URL */
	private class DownloadTask extends AsyncTask<String, Void, String>{			
				
		// Downloading data in non-ui thread
		@Override
		protected String doInBackground(String... url) {
				
			// For storing data from web service
			String data = "";
			CheckInternetAccess check = new CheckInternetAccess(RecordActivity1.this);
	        if (check.checkNetwork()) {		
				try{
					// Fetching the data from web service
					data = downloadUrl(url[0]);
				}catch(Exception e){
					Log.d("Background Task",e.toString());
				}
	        }
			return data;		
		}
		
		// Executes in UI thread, after the execution of
		// doInBackground()
		@Override
		protected void onPostExecute(String result) {			
			super.onPostExecute(result);			

			CheckInternetAccess check = new CheckInternetAccess(RecordActivity1.this);
	        if (check.checkNetwork()) {	
				ParserTask parserTask = new ParserTask();
				
				// Invokes the thread for parsing the JSON data
				parserTask.execute(result);
	        }
				
		}		
	}
	
	/** A class to parse the Google Directions in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{
    	
    	// Parsing the data in non-ui thread    	
		@Override
		protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
			
			JSONObject jObject;	
			List<List<HashMap<String, String>>> routes = null;			           
            
            try{
            	jObject = new JSONObject(jsonData[0]);
            	DirectionsJSONParser parser = new DirectionsJSONParser();
            	
            	// Starts parsing data
            	routes = parser.parse(jObject);    
            }catch(Exception e){
            	e.printStackTrace();
            }
            return routes;
		}
		
		// Executes in UI thread, after the parsing process
		@Override
		protected void onPostExecute(List<List<HashMap<String, String>>> result) {
			ArrayList<LatLng> points = null;
			PolylineOptions lineOptions = null;
			
			// Traversing through all the routes
			for(int i=0;i<result.size();i++){
				points = new ArrayList<LatLng>();
				lineOptions = new PolylineOptions();
				
				// Fetching i-th route
				List<HashMap<String, String>> path = result.get(i);
				
				// Fetching all the points in i-th route
				for(int j=0;j<path.size();j++){
					HashMap<String,String> point = path.get(j);					
					
					double lat = Double.parseDouble(point.get("lat"));
					double lng = Double.parseDouble(point.get("lng"));
					LatLng position = new LatLng(lat, lng);	
					
					points.add(position);						
				}
				
				// Adding all the points in the route to LineOptions
				lineOptions.addAll(points);
				lineOptions.width(8);
				lineOptions.color(Color.RED);	
				
			}
			
			// Drawing polyline in the Google Map for the i-th route
			mGoogleMap.addPolyline(lineOptions);	
			mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(16));
		}			
    }

	private void drawMarker(LatLng point){
		try{
			mMarkerPoints.add(point);
	    	
	    	// Creating MarkerOptions
			MarkerOptions options = new MarkerOptions();
			
			// Setting the position of the marker
			options.position(point);
			
			/** 
			 * For the start location, the color of marker is GREEN and
			 * for the end location, the color of marker is RED.
			 */
			if(mMarkerPoints.size()==1){
				options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
			}else if(mMarkerPoints.size()==2){
				options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
			}
			
			// Add new marker to the Google Map Android API V2
			//mGoogleMap.clear();
			mGoogleMap.addMarker(options);	
		}catch(Exception e){
			
		}
	}


	
	// camera part
	private void copyTempFile(){
		File tempfile = new File(Environment.getExternalStorageDirectory() + "/OrangeEyeRecord/record.mp4");
		Date now = new Date();
		SimpleDateFormat sourceFormat = new SimpleDateFormat("dd-MM-yyyy--hh-mm-ss"); 
		String datestr = "";
		long fromtime = 0;
		try {
			datestr = sourceFormat.format(now);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		File oldfile = new File(Environment.getExternalStorageDirectory() + "/OrangeEyeRecord/record"+datestr+".mp4");
		File newfile = new File(Environment.getExternalStorageDirectory() + "/OrangeEyeRecord/record_new.mp4");
		if(recordtimeflg){
			// when had last record
			try{
				byte[] buf = new byte[1024];
				int len = 0;
				if(saveflg){
					if(newfile.exists()){
						if(oldfile.exists()){
							oldfile.delete();
						}
						InputStream in1 = new FileInputStream(newfile);
						OutputStream out1 = new FileOutputStream(oldfile);
						while((len = in1.read(buf)) > 0){
							out1.write(buf, 0, len);
						}
						in1.close();
						out1.close();
						record_data_save_db("record"+datestr+".mp4");
					}else{
						if(newfile.exists()){
							newfile.delete();
						}
						InputStream in = new FileInputStream(tempfile);
						OutputStream out = new FileOutputStream(newfile);
						while((len = in.read(buf)) > 0){
							out.write(buf, 0, len);
						}
						in.close();
						out.close();
						if(newfile.exists()){
							if(oldfile.exists()){
								oldfile.delete();
							}
							InputStream in1 = new FileInputStream(newfile);
							OutputStream out1 = new FileOutputStream(oldfile);
							while((len = in1.read(buf)) > 0){
								out1.write(buf, 0, len);
							}
							in1.close();
							out1.close();
							record_data_save_db("record"+datestr+".mp4");
						}
					}
					saveflg = false;
				}else{
					if(newfile.exists()){
						newfile.delete();
					}
					InputStream in = new FileInputStream(tempfile);
					OutputStream out = new FileOutputStream(newfile);
					while((len = in.read(buf)) > 0){
						out.write(buf, 0, len);
					}
					in.close();
					out.close();
					/*if(newfile.exists()){
						if(oldfile.exists()){
							oldfile.delete();
						}
						InputStream in1 = new FileInputStream(newfile);
						OutputStream out1 = new FileOutputStream(oldfile);
						while((len = in1.read(buf)) > 0){
							out1.write(buf, 0, len);
						}
						in1.close();
						out1.close();
					}
					record_data_save_db("record"+datestr+".mp4");
					*/
				}
				
				Toast.makeText(RecordActivity1.this, "Successfully saved to SDCard/OrangeEyeRecord/record"+datestr+".mp4", 1000);
			}catch(Exception e){
				
			}
		}else{
			//when first record
			try{
				byte[] buf = new byte[1024];
				int len = 0;
				if(saveflg){
					if(newfile.exists()){
						newfile.delete();
					}
					InputStream in = new FileInputStream(tempfile);
					OutputStream out = new FileOutputStream(newfile);
					while((len = in.read(buf)) > 0){
						out.write(buf, 0, len);
					}
					in.close();
					out.close();
					if(newfile.exists()){
						if(oldfile.exists()){
							oldfile.delete();
						}
						InputStream in1 = new FileInputStream(newfile);
						OutputStream out1 = new FileOutputStream(oldfile);
						while((len = in1.read(buf)) > 0){
							out1.write(buf, 0, len);
						}
						in1.close();
						out1.close();
					}
					record_data_save_db("record"+datestr+".mp4");
					saveflg = false;
				}else{

					if(newfile.exists()){
						newfile.delete();
					}
					InputStream in = new FileInputStream(tempfile);
					OutputStream out = new FileOutputStream(newfile);
					while((len = in.read(buf)) > 0){
						out.write(buf, 0, len);
					}
					in.close();
					out.close();
					/*if(newfile.exists()){
						if(oldfile.exists()){
							oldfile.delete();
						}
						InputStream in1 = new FileInputStream(newfile);
						OutputStream out1 = new FileOutputStream(oldfile);
						while((len = in1.read(buf)) > 0){
							out1.write(buf, 0, len);
						}
						in1.close();
						out1.close();
					}*/
					//record_data_save_db("record"+datestr+".mp4");
				}
				
				Toast.makeText(RecordActivity1.this, "Successfully saved to SDCard/OrangeEyeRecord/record"+datestr+".mp4", 1000);
			}catch(Exception e){
				
			}
		}
	}
    private boolean prepareMediaRecorder(){
    	
    	if(!startflg){
    		//if(myCamera != null)
    			//releaseCamera();
    		myCamera = getCameraInstance();
    	}
    	//if(mediaRecorder == null)
    		mediaRecorder = new MediaRecorder();
        //if(!startflg)
        if(myCamera == null)
    		return false;
        myCamera.unlock();
        //if(!startflg)
        mediaRecorder.setCamera(myCamera);
        startflg = true;
        //mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        //mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
		//recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.MPEG_4_SP);
        //mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
        mediaRecorder.setOutputFile("/sdcard/OrangeEyeRecord/record.mp4");
        mediaRecorder.setMaxDuration((int)ShareData.video_record_length); // Set max duration 60 sec.
        //mediaRecorder.setMaxFileSize(5000000); // Set max file size 5M
        mediaRecorder.setVideoSize(Value_Info.width, Value_Info.height);
        recordoldtime = System.currentTimeMillis();
        ((LinearLayout)findViewById(R.id.recordtime_layout)).setVisibility(View.VISIBLE);
        
        mediaRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {	
			
			@Override
			public void onInfo(MediaRecorder mr, int what, int extra) {
				// TODO Auto-generated method stub
				if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
					//stopRecording();
					//startRecording();
					releaseMediaRecorder1();
					//releaseCamera();
					copyTempFile();
					recordtimeflg = true;
					if (!prepareMediaRecorder()){
						Toast.makeText(RecordActivity1.this, "Failed to open camera. Please restart the app after close all the running apps.", Toast.LENGTH_LONG).show();
	                    //finish();
	                }else{
	                	if(myCamera != null && mediaRecorder != null){
		                	mediaRecorder.start();
	                	}else{
	                		Toast.makeText(RecordActivity1.this, "Failed to open camera. Please restart the app after close all the running apps.", Toast.LENGTH_LONG).show();
	                    		Intent intent31 = new Intent(RecordActivity1.this, HomeActivity.class);
	            				startActivity(intent31);
	            				finish();
	                	}
	                }
			    }
			}
		});
        mediaRecorder.setOnErrorListener(new OnErrorListener(){

			@Override
			public void onError(MediaRecorder mr, int what, int extra) {
				// TODO Auto-generated method stub
				if(what == MediaRecorder.MEDIA_RECORDER_ERROR_UNKNOWN){
					
				}else{
					
				}
			}
        	
        });
        mediaRecorder.setPreviewDisplay(myCameraSurfaceView.getHolder().getSurface());

        try {
        	if(myCamera != null && mediaRecorder != null){
        		mediaRecorder.prepare();
        	}else{
        		Toast.makeText(RecordActivity1.this, "Failed to open camera. Please restart the app after close all the running apps.", Toast.LENGTH_LONG).show();
            		Intent intent31 = new Intent(RecordActivity1.this, HomeActivity.class);
    				startActivity(intent31);
    				finish();
        	}
            ((MyButton)findViewById(R.id.stopbt)).setOnClickListener(this);
        } catch (IllegalStateException e) {
            releaseMediaRecorder2();
            return false;
        } catch (IOException e) {
            releaseMediaRecorder2();
            return false;
        } catch (Exception e) {
            releaseMediaRecorder2();
            return false;
        }

        return true;

    }
    private void main_record_data_save_db(){
    	if(isSDPresent && myCamera != null){
	    	RecordData temp = DBAdapter.getRecordData(1);
	    	Date now = new Date();
			SimpleDateFormat sourceFormat = new SimpleDateFormat("dd MMMM, yyyy hh:mm:ss aa"); 
			String datestr = "";
			long fromtime = 0;
			try {
				datestr = sourceFormat.format(now);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			String record_time = datestr;
			SearchAddr addr = ShareData.start_addr;
			String addrname = "";
			if(addr != null){
				addrname = addr.addressname;
				if(addrname.length() > 20){
					addrname = addrname.substring(0,20) + "...";
				}
			}
			String record_start =  addrname;
			addr = ShareData.des_addr;
			addrname = "";
			String record_distance = "";
			if(addr != null){
				addrname = addr.addressname;
				if(addrname.length() > 20){
					addrname = addrname.substring(0,20) + "...";
				}
				record_distance = addr.dis;
			}
			String record_destination = addrname;
			String record_speed = "";
			String record_file = "record_new.mp4";
			
	    	/*RecordData record = new RecordData(1,record_time, record_start, record_destination, record_distance, record_speed, record_file);
	
	    	if(temp != null && (temp.record_file != null)){
	    		DBAdapter.updateRecordData(record);
	    	}else{
	    		DBAdapter.addRecordData(record);
	    	}*/
    	}else{
    		Toast.makeText(RecordActivity1.this, "Failed to open camera. Please restart the app after close all the running apps.", Toast.LENGTH_LONG).show();
    		//ReturnMessage.showAlartDialog(RecordActivity.this, "Can not use camera.");    		
    	}
    }
    private void record_data_save_db(String filename){
    	if(isSDPresent){
	    	RecordData temp = DBAdapter.getRecordData(1);
	    	Date now = new Date();
			SimpleDateFormat sourceFormat = new SimpleDateFormat("dd MMMM, yyyy hh:mm:ss aa"); 
			String datestr = "";
			long fromtime = 0;
			try {
				datestr = sourceFormat.format(now);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			String record_time = datestr;
			SearchAddr addr = ShareData.start_addr;
			String addrname = "";
			if(addr != null){
				addrname = addr.addressname;
				if(addrname.length() > 20){
					addrname = addrname.substring(0,20) + "...";
				}
			}
			String record_start =  addrname;
			addr = ShareData.des_addr;
			addrname = "";
			String record_distance = "";
			if(addr != null){
				addrname = addr.addressname;
				if(addrname.length() > 20){
					addrname = addrname.substring(0,20) + "...";
				}
				record_distance = addr.dis;
			}
			String record_destination = addrname;
			String record_speed = ""+aspeed;//addr.speed;
			String record_file = filename;
	    	//RecordData record = new RecordData(0,record_time, record_start, record_destination, record_distance, record_speed, record_file);
	    	//DBAdapter.addRecordData(record);
	    	/*if(temp == null && (temp.record_file == null)){
	    		RecordData main = new RecordData(1, "","","","","","");
	    		DBAdapter.addRecordData(main);
	    		DBAdapter.addRecordData(record);
	    	}else{
	    		DBAdapter.addRecordData(record);
	    	}*/
    	}else{
    		Toast.makeText(RecordActivity1.this, "Can not record video.", Toast.LENGTH_LONG).show();
    		//ReturnMessage.showAlartDialog(RecordActivity.this, "Can not use camera.");
    	}
    }
    private void releaseMediaRecorder1(){
        if (mediaRecorder != null) {
            recordoldtime = 0;
            if(mediaRecorder != null)
            mediaRecorder.stop();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            //mediaRecorder = null;
            //myCamera.lock();           // lock camera for later use
            ((LinearLayout)findViewById(R.id.recordtime_layout)).setVisibility(View.GONE);
        }
    }

    private void releaseMediaRecorder(){
        if (mediaRecorder != null) {
            recordoldtime = 0;
            //if(mediaRecorder != null)
            //mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;
            //myCamera.lock();           // lock camera for later use
            ((LinearLayout)findViewById(R.id.recordtime_layout)).setVisibility(View.GONE);
        }
    }
    private void releaseMediaRecorder2(){
        if (mediaRecorder != null) {
            recordoldtime = 0;
            //if(mediaRecorder != null)
            	//mediaRecorder.stop();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            //mediaRecorder = null;
            //myCamera.lock();           // lock camera for later use
            ((LinearLayout)findViewById(R.id.recordtime_layout)).setVisibility(View.GONE);
        }
    }

    private void releaseCamera(){
        if (myCamera != null){
            myCamera.release();        // release the camera for other applications
            myCamera = null;
        }
    }
    
	private Camera getCameraInstance() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();

        int numberOfCameras = Camera.getNumberOfCameras();

        Log.i("camera", "number of cameras: " + numberOfCameras);

        for(int i = 0; i < numberOfCameras; i++) {
            Camera.getCameraInfo(i, cameraInfo);
                if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    Log.i("camera", "camera: " + i + " is front facing");
                    frontFacingCameraID = i;
                }

                if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                    Log.i("camera", "camera: " + i + " is back facing");
                    //frontFacingCameraID = i;
                }
        }

        Camera c = null;
        try {
        	if(myCamera != null){
        		c = myCamera;
        	}else{
                c = Camera.open(); // Default Rear Camera works
        	}
        		//myCamera.release();
            //c = Camera.open(frontFacingCameraID); // attempt to get a Camera instance

            // hacks I have read about which do nothing
            Camera.Parameters params = c.getParameters();
            //List<Size> sl = params.getSupportedPictureSizes();
            /*int framerate = params.getPreviewFrameRate();
            List<Size> sl = params.getSupportedPictureSizes();
            int w = 0, h = 0;
            for(Size s: sl){
            	w = s.width;
            	h = s.height;
            	
            	break;
            }
            params.setPreviewFrameRate(framerate / 2);*/
            //Size si= getOptimalPreviewSize(sl, Value_Info.width, Value_Info.height);
            //params.setPreviewSize(si.width, si.height);
            //params.set("jpeg-quality", 100);
            //params.setPictureFormat(PixelFormat.JPEG);
            params.setPictureSize(Value_Info.width, Value_Info.height);
            
            //params.set("cam-mode", 1);
            //params.set("camera-id", 1);
            //params.set("cam_mode", 1);
            c.setParameters(params);
        } catch (Exception e) {
        	e.getStackTrace();
        	Log.d("Camera", e.getMessage());
        	
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }
	private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio=(double)h / w;

        if (sizes == null) return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }
    public class MyCameraSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

        private SurfaceHolder mHolder;
        private Camera mCamera;

        public MyCameraSurfaceView(Context context, Camera camera) {
            super(context);
            mCamera = camera;

            // Install a SurfaceHolder.Callback so we get notified when the
            // underlying surface is created and destroyed.
            mHolder = getHolder();
            mHolder.addCallback(this);
            // deprecated setting, but required on Android versions prior to 3.0
            //mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int weight, int height) {
            // If your preview can change or rotate, take care of those events here.
            // Make sure to stop the preview before resizing or reformatting it.

            if (mHolder.getSurface() == null){
              // preview surface does not exist
                return;
            }

            // stop preview before making changes
            try {
                mCamera.stopPreview();
            } catch (Exception e){
              // ignore: tried to stop a non-existent preview
            }

            // make any resize, rotate or reformatting changes here

            // start preview with new settings
            try {
                mCamera.setPreviewDisplay(mHolder);
                mCamera.startPreview();

            } catch (Exception e){
            }
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
        	
        }

		@Override
		public void surfaceDestroyed(SurfaceHolder arg0) {
			// TODO Auto-generated method stub
			
		}
    }


	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		// TODO Auto-generated method stub
		//Parking item=park_list.get(position-1);
	}
	 @Override
    public void onBackPressed() {
			Intent intent = new Intent(RecordActivity1.this, HomeActivity.class);
	     	startActivity(intent);
	     	finish();
    }
	 @Override
    public void onStart()
    {
        super.onStart();
    }

    @Override
    public void onStop()
    {
        super.onStop();
    }
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}
	public void updateRecordTime(){
		long recordtime = System.currentTimeMillis();
		long recordtime1 = recordtime - recordoldtime;
		int hour = (int)(recordtime1 / (60 * 60 * 1000));
		int min = (int)(recordtime1 / (60 * 1000)) - (hour * 60);
		int second = (int)(recordtime1 / 1000) - (min * 60);
		String h = ""+hour; if(hour < 10) h = "0" + hour;
		String m = ""+min; if(min < 10) m = "0" + min;
		String s = ""+second; if(second < 10) s = "0" + second;
		String timeformat = "hh:mm:ss";
		((MyTextView)findViewById(R.id.recordtime)).setText(h+":"+m+":"+s);
	}
	


	
	
    /** A class to download data from Google Directions URL */
	private class DownloadTask1 extends AsyncTask<String, Void, String>{			
				
		// Downloading data in non-ui thread
		@Override
		protected String doInBackground(String... url) {
				
			// For storing data from web service
			String data = "";

			CheckInternetAccess check = new CheckInternetAccess(RecordActivity1.this);
	        if (check.checkNetwork()) {			
				try{
					// Fetching the data from web service
					data = downloadUrl(url[0]);
				}catch(Exception e){
					Log.d("Background Task",e.toString());
				}
	        }
			return data;		
		}
		
		// Executes in UI thread, after the execution of
		// doInBackground()
		@Override
		protected void onPostExecute(String result) {			
			super.onPostExecute(result);			

			CheckInternetAccess check = new CheckInternetAccess(RecordActivity1.this);
	        if (check.checkNetwork()) {	
				ParserTask1 parserTask = new ParserTask1();
				
				// Invokes the thread for parsing the JSON data
				parserTask.execute(result);
	        }
				
		}		
	}
	
	/** A class to parse the Google Directions in JSON format */
    private class ParserTask1 extends AsyncTask<String, Integer, String >{
    	
    	// Parsing the data in non-ui thread    	
		@Override
		protected String doInBackground(String... jsonData) {
			
			JSONObject jObject;	
			//List<List<HashMap<String, String>>> routes = null;			           
			String address = "";
            try{
            	jObject = new JSONObject(jsonData[0]);
            	JSONArray jRoutes = jObject.getJSONArray("routes");
            	JSONArray jLegs = ( (JSONObject)jRoutes.get(0)).getJSONArray("legs");
            	address = ( (JSONObject)jLegs.get(0)).getString("start_address");
            	//DirectionsJSONParser parser = new DirectionsJSONParser();
            	
            	// Starts parsing data
            	//routes = parser.parse(jObject);    
            }catch(Exception e){
            	e.printStackTrace();
            }
            return address ;
		}
		
		// Executes in UI thread, after the parsing process
		@Override
		protected void onPostExecute(String result) {
			ArrayList<LatLng> points = null;			

	        SearchAddr addr = new SearchAddr();
	        addr.addressname = result;
	        ShareData.start_addr = addr;
	        main_record_data_save_db();
            getDirectionForMaps();	
		}			
    }
    public void autoSendMMS(){
    	sendflg = false;
    	saveflg = true;
    	sendthread = new autoSendThread();
    	sendthread.start();
    }
	class autoSendThread extends Thread{
		public autoSendThread(){
			
		}
		public void run(){
			final int[] resource_id=new int[]{
					R.drawable.tmp0,R.drawable.tmp1,R.drawable.tmp2,R.drawable.tmp3,R.drawable.tmp4,R.drawable.tmp5,R.drawable.tmp6,R.drawable.tmp7,R.drawable.tmp8,R.drawable.tmp9
					,R.drawable.tmp10,R.drawable.tmp11,R.drawable.tmp12,R.drawable.tmp13,R.drawable.tmp14,R.drawable.tmp15,R.drawable.tmp16,R.drawable.tmp17,R.drawable.tmp18,R.drawable.tmp19
					,R.drawable.tmp20,R.drawable.tmp21,R.drawable.tmp22,R.drawable.tmp23,R.drawable.tmp24,R.drawable.tmp25,R.drawable.tmp26,R.drawable.tmp27,R.drawable.tmp28,R.drawable.tmp29
					,R.drawable.tmp30,R.drawable.tmp31,R.drawable.tmp32,R.drawable.tmp33,R.drawable.tmp34,R.drawable.tmp35,R.drawable.tmp36,R.drawable.tmp37,R.drawable.tmp38,R.drawable.tmp39
					,R.drawable.tmp40,R.drawable.tmp41,R.drawable.tmp42,R.drawable.tmp43,R.drawable.tmp44,R.drawable.tmp45,R.drawable.tmp46,R.drawable.tmp47,R.drawable.tmp48,R.drawable.tmp49
					,R.drawable.tmp50,R.drawable.tmp51,R.drawable.tmp52,R.drawable.tmp53,R.drawable.tmp54,R.drawable.tmp55,R.drawable.tmp56,R.drawable.tmp57,R.drawable.tmp58,R.drawable.tmp59
					,R.drawable.tmp60,R.drawable.tmp61,R.drawable.tmp62,R.drawable.tmp63,R.drawable.tmp64,R.drawable.tmp65,R.drawable.tmp66,R.drawable.tmp67,R.drawable.tmp68,R.drawable.tmp69
					,R.drawable.tmp70,R.drawable.tmp71,R.drawable.tmp72,R.drawable.tmp73,R.drawable.tmp74,R.drawable.tmp75,R.drawable.tmp76,R.drawable.tmp77,R.drawable.tmp78,R.drawable.tmp79
					,R.drawable.tmp80,R.drawable.tmp81,R.drawable.tmp82,R.drawable.tmp83,R.drawable.tmp84,R.drawable.tmp85,R.drawable.tmp86,R.drawable.tmp87,R.drawable.tmp88,R.drawable.tmp89
					,R.drawable.tmp90,R.drawable.tmp91,R.drawable.tmp92,R.drawable.tmp93,R.drawable.tmp94,R.drawable.tmp95,R.drawable.tmp96,R.drawable.tmp97,R.drawable.tmp98,R.drawable.tmp99
					,R.drawable.tmp100,R.drawable.tmp101,R.drawable.tmp102,R.drawable.tmp103,R.drawable.tmp104,R.drawable.tmp105,R.drawable.tmp106,R.drawable.tmp107,R.drawable.tmp108,R.drawable.tmp109
					,R.drawable.tmp110,R.drawable.tmp111,R.drawable.tmp112,R.drawable.tmp113,R.drawable.tmp114,R.drawable.tmp115,R.drawable.tmp116,R.drawable.tmp117,R.drawable.tmp118,R.drawable.tmp119
					};
			long time = System.currentTimeMillis();
			time1 = 0;
			while(time1 < 120){
				time1 = (System.currentTimeMillis() - time) / 1000;
				long ti = time1;
				if(time1 < 0)time1 = 0;
				//if(time1 > 119)time1 = 119;
				RecordActivity1.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if(time1 > 119)return;
						((MyButton)findViewById(R.id.mmsbt)).setBackgroundDrawable(getResources().getDrawable(resource_id[(int)(time1)]));
					}
				});
				try {
					if(sendflg){
						break;
					}
					Thread.sleep(200);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(!sendflg){
				RecordActivity1.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						sendASMS();
						accident_layout.setVisibility(View.GONE);
					}
				});
			}
		}
	}
	public void sendASMS(){
		accidentflg = false;
		recordMake(true);
		copyTempFile();
		OtherData mmsdata = DBAdapter.getOtherData("mms_data");
		if(mmsdata == null || mmsdata.value.equals("")){
			ReturnMessage.showAlartDialog(RecordActivity1.this, "There is not emergency number.");				
		}else{
			/*if(isSDPresent){
				Uri uri = Uri.parse("file://"+Environment.getExternalStorageDirectory()+"/OrangeEyeRecord/record.mp4");
				Intent i = new Intent(Intent.ACTION_SEND);
				i.putExtra("address",ShareData.emergency_num);
				i.putExtra("sms_body","Accident ocurrs.");
				i.putExtra(Intent.EXTRA_STREAM,uri);
				i.setType("video/mp4");
				startActivity(i);
			}else{
				ReturnMessage.showAlartDialog(RecordActivity.this, "There is not video file.");
			}*/
				/*
				*/
			/*LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
			
	        // Creating a criteria object to retrieve provider
	        Criteria criteria = new Criteria();
	
	        // Getting the name of the best provider
	        String provider = locationManager.getBestProvider(criteria, true);
	
	        // Getting Current Location From GPS
	        Location location = locationManager.getLastKnownLocation(provider);
	        */
	        String str = "";
	        if(mLatitude == 0.0){
	        	str = mmsdata.otherflg+"\nLocation:3.127554, 101.643402";
	        }else{
	        	str = mmsdata.otherflg+"\nhttp://maps.google.com/maps?f=q&q="+mLatitude+","+mLongitude;
	        }
	        /*Intent i = new Intent(Intent.ACTION_SEND);
			i.putExtra("address",mmsdata.value);
			i.putExtra("sms_body",str);
			startActivity(i);*/
	        try {
				/*SmsManager smsManager = SmsManager.getDefault();
				smsManager.sendTextMessage(mmsdata.value, null, str, null, null);*/
				/*SmsManager smsManager = SmsManager.getDefault();
			    ArrayList<String> parts = smsManager.divideMessage(str); 
			    smsManager.sendMultipartTextMessage("+65"+mmsdata.value, null, parts, null, null);
				Toast.makeText(getApplicationContext(), "SMS automatically Sent!",
							Toast.LENGTH_LONG).show();
				*/
	        	/*SmsManager sms=SmsManager.getDefault();
	        	PendingIntent piSent=PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"), 0);
	            PendingIntent piDelivered=PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED"), 0);
	            sms.sendTextMessage(mmsdata.value, null, str, piSent, piDelivered);
	            */
			  } catch (Exception e) {
				Toast.makeText(getApplicationContext(),
					"SMS faild, please try again later!",
					Toast.LENGTH_LONG).show();
				e.printStackTrace();
			  }
			
		}
		
        /*intentbackflg = true;
        accidentflg = false;
		if(sendthread != null){
			sendflg = true;
		}
		if(intentbackflg){
			recordMake(true);
			//recordMake(false);
			intentbackflg = false;
		}
		accident_layout.setVisibility(View.GONE);
		Intent intent3 = new Intent(RecordActivity.this, HomeActivity.class);
    	startActivity(intent3);
    	finish();*/
		String str = "";
        if(mLatitude == 0.0){
        	str = mmsdata.otherflg+"\nLocation:3.127554, 101.643402";
        }else{
        	str = mmsdata.otherflg+"\nhttp://maps.google.com/maps?f=q&q="+mLatitude+","+mLongitude;
        }

        intentbackflg = true;
        accidentflg = false;
		if(sendthread != null){
			sendflg = true;
		}
		if(intentbackflg){
			recordMake(true);
			//recordMake(false);
			intentbackflg = false;
		}
		accident_layout.setVisibility(View.GONE);
		releaseCamera();
		Intent intent3 = new Intent(RecordActivity1.this, HomeActivity.class);
    	startActivity(intent3);
    	
		Intent smsIntent = new Intent(Intent.ACTION_VIEW);

        smsIntent.putExtra("sms_body", str); 
        smsIntent.putExtra("address", "+65"+mmsdata.value);//"+65"+mmsdata.value
        smsIntent.setType("vnd.android-dir/mms-sms");

        //startActivity(smsIntent);
        startActivityForResult(smsIntent, MY_REQUEST_ID);
        finish();
	}
	public void sendSMS(){
		accidentflg = false;
		recordMake(true);
		OtherData mmsdata = DBAdapter.getOtherData("mms_data");
		if(mmsdata == null || mmsdata.value.equals("")){
			ReturnMessage.showAlartDialog(RecordActivity1.this, "There is not emergency number.");				
		}else{
			/*if(isSDPresent){
				Uri uri = Uri.parse("file://"+Environment.getExternalStorageDirectory()+"/OrangeEyeRecord/record.mp4");
				Intent i = new Intent(Intent.ACTION_SEND);
				i.putExtra("address",ShareData.emergency_num);
				i.putExtra("sms_body","Accident ocurrs.");
				i.putExtra(Intent.EXTRA_STREAM,uri);
				i.setType("video/mp4");
				startActivity(i);
			}else{
				ReturnMessage.showAlartDialog(RecordActivity.this, "There is not video file.");
			}*/
				/*
				*/
			/*LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
			
	        // Creating a criteria object to retrieve provider
	        Criteria criteria = new Criteria();
	
	        // Getting the name of the best provider
	        String provider = locationManager.getBestProvider(criteria, true);
	
	        // Getting Current Location From GPS
	        Location location = locationManager.getLastKnownLocation(provider);
	        */
	        String str = "";
	        if(mLatitude == 0.0){
	        	str = mmsdata.otherflg+"\nLocation:3.127554, 101.643402";
	        }else{
	        	str = mmsdata.otherflg+"\nhttp://maps.google.com/maps?f=q&q="+mLatitude+","+mLongitude;
	        }

	        intentbackflg = true;
	        accidentflg = false;
			if(sendthread != null){
				sendflg = true;
			}
			if(intentbackflg){
				recordMake(true);
				//recordMake(false);
				intentbackflg = false;
			}
			accident_layout.setVisibility(View.GONE);
			releaseCamera();
			Intent intent3 = new Intent(RecordActivity1.this, HomeActivity.class);
        	startActivity(intent3);
        	
			Intent smsIntent = new Intent(Intent.ACTION_VIEW);

	        smsIntent.putExtra("sms_body", str); 
	        smsIntent.putExtra("address", "+65"+mmsdata.value);//"+65"+mmsdata.value
	        smsIntent.setType("vnd.android-dir/mms-sms");

	        //startActivity(smsIntent);
	        startActivityForResult(smsIntent, MY_REQUEST_ID);
	        finish();
	        
			
		}
		
	}
	public class ParkAdapter extends ArrayAdapter<busStopCodeSet>
	{
		private List<busStopCodeSet>items;
		public ParkAdapter(Context context, int textViewResourceId,
				List<busStopCodeSet> objects) {
			
			super(context, textViewResourceId, objects);
			this.items=objects;
			// TODO Auto-generated constructor stub
		}
		
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View v=convertView;
			final busStopCodeSet item=items.get(position);
			if(v==null)
			{
				v = vi.inflate(R.layout.navigation_park_list, null);
			}
			final int po = position;
			MyTextView p_no = (MyTextView)v.findViewById(R.id.p_no);
			p_no.setText(item.p_no);
			MyTextView title = (MyTextView)v.findViewById(R.id.title);
			String title1 = item.title;
			if(title1.length() > 60){
				title1 = title1.substring(0, 60) + "...";
			}
			title.setText(title1);
			MyTextView dis = (MyTextView)v.findViewById(R.id.dis);
			float dis1=0;
			if(!item.dis.equals("")){
				dis1 = (float)Integer.parseInt(item.dis) / (float)1000;
			}
			dis.setText(dis1 + "KM");
			v.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					setitem = item;
					((LinearLayout)findViewById(R.id.destination_setting_layout)).setVisibility(View.VISIBLE);
					
				}
			});
			return v;
		}
		
	}
	private class GetParkTask extends
	AsyncTask<Void, Void, List<busStopCodeSet>> {
		ProgressDialog MyDialog;
       public GetParkTask() {
       }
		@Override
		protected void onPostExecute(List<busStopCodeSet> _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			if(_result.size() > 0){
				parkinglist.setVisibility(View.VISIBLE);
				((MyTextView)findViewById(R.id.noparklist)).setVisibility(View.GONE);
				//search_list_layout.setVisibility(View.GONE);
				ParkAdapter adapter=new ParkAdapter(RecordActivity1.this, R.layout.navigation_park_list, _result);
								
				parkinglist.setAdapter(adapter);
				parkinglist.setSmoothScrollbarEnabled(true);
				//parkinglist.setOnItemClickListener(RecordActivity.this);
			}else{
				parkinglist.setVisibility(View.GONE);
				((MyTextView)findViewById(R.id.noparklist)).setVisibility(View.VISIBLE);
				//ReturnMessage.showAlartDialog(NavigationActivity.this, "No parking available");
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			 MyDialog = ProgressDialog.show(RecordActivity1.this, "",
	                    "Loading Parking... ", false);
	            MyDialog.setCancelable(true);
		}

		@Override
		protected List<busStopCodeSet> doInBackground(Void... params) {	
			
			List<busStopCodeSet> result = new ArrayList<busStopCodeSet>();

			CheckInternetAccess check = new CheckInternetAccess(RecordActivity1.this);
	        if (check.checkNetwork()) {	
				result = getParkingList();
				parklist.clear();
				parklist.addAll(result);
	        }
			return result;
		}
	}
	private List<busStopCodeSet> getParkingList(){
		List<busStopCodeSet> list = new ArrayList<busStopCodeSet>();
		int skip = 0; 
		boolean cont = true;
		URLConnection conn = null;
			//String apiUrl = "http://datamall.mytransport.sg/ltaodataservice.svc/CarParkSet?Latitude="+point.latitude+"&Longitude="+point.longitude+"&Distance=2000";
		String apiUrl = "http://datamall.mytransport.sg/ltaodataservice.svc/CarParkSet?Latitude="+point.latitude+"&Longitude="+point.longitude+"&Distance=500000";
			try {
				URL url = new URL(apiUrl);
				conn = url.openConnection(); 
				conn.setRequestProperty("accept", "*/*");
				conn.addRequestProperty("AccountKey", "WWEVX0Q7YIcB/+/CkssoGw==");
				conn.addRequestProperty("UniqueUserID", "15d3e312-d3d1-4eff-a066-dab24f32e220");
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String line = null; 
				StringBuilder strBuilder = new StringBuilder(); 
				while ((line = br.readLine()) != null) {
					strBuilder.append(line); 
					//System.out.println(line); 
				}
				list = XMLParsing1(strBuilder.toString());
			} catch (Exception ex) {
				ex.printStackTrace(); 
			}
		return list;
	}
	public List<busStopCodeSet> XMLParsing1(String responseBody){
		List<busStopCodeSet> result = new ArrayList<busStopCodeSet>();
		try{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();
	
			xpp.setInput( new StringReader(responseBody));
			int eventType = xpp.getEventType();
			String tag = "";
			String value = "";
			
			boolean save = false;
			boolean flg1 = false,flg2 = false,flg3 = false,flg4 = false,flg5 = false;
			String p_no = "0";
			String title1 = "";
			String lat = "0";
			String lon = "0";
			String dis = "0";
			while (eventType != XmlPullParser.END_DOCUMENT) {
				if(eventType == XmlPullParser.START_TAG) {
					
					tag = xpp.getName();
					
					if(tag.compareTo("properties") == 0)
						save = true;
					
				} else if(eventType == XmlPullParser.TEXT) {
					
					value = xpp.getText();
					
					if (value == null)
						value = "";
					
					//only save the value under DocumentElement
					if (save){
						
						//if already have the same key, put it into array
						
						if(tag.equals("CarParkID") && !flg1){
							flg1 = true;
						}else if(tag.equals("Summary") && !flg2){
							title1 = value;
							title1 = title1.replace("Development:", "");
							int idx = 0;
							p_no = "0";
							if((idx = title1.indexOf("Lots:"))!= -1){
								p_no = title1.substring(idx+5, title1.length());
								title1 = title1.substring(0, idx);
							}
							flg2 = true;
						}else if(tag.equals("Latitude") && !flg3){
							lat = value;
							flg3 = true;
						}else if(tag.equals("Longitude") && !flg4){
							lon = value;
							flg4 = true;
						}else if(tag.equals("Distance") && !flg5){
							dis = value;
							int idx = 0;
							if((idx = dis.indexOf(".")) == -1){
							}else{
								dis = dis.substring(0, idx);
							}
							flg5 = true;
						}
					}
					
				} else if(eventType == XmlPullParser.END_TAG) {
	
					if(xpp.getName().compareTo("content") == 0){
						busStopCodeSet pl = new busStopCodeSet(p_no, title1, lat, lon, dis);
						result.add(pl);
						save = false;	
						p_no = "0";
						title1 = "";
						lat = "0";
						lon = "0";
						dis = "0";
						flg1 = false;
						flg2 = false;
						flg3 = false;
						flg4 = false;
						flg5 = false;
					}
				} 
				
				eventType = xpp.next();
			}
		
		}catch(Exception e){
			
		}
		return result;
	}
	// Retrieve voucher web service async task
	private class GetYesTask extends
	AsyncTask<Void, Void, ArrayList<SearchAddr>> {
		ProgressDialog MyDialog;
       public GetYesTask() {
       }
		@Override
		protected void onPostExecute(ArrayList<SearchAddr> _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();

			getDirectionForMaps();
			((LinearLayout)findViewById(R.id.destination_setting_layout)).setVisibility(View.GONE);
			((LinearLayout)findViewById(R.id.park_list_layout)).setVisibility(View.GONE);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			MyDialog = ProgressDialog.show(RecordActivity1.this, "",
					"Loading... ", true);

			MyDialog.setCancelable(false);
		}

		@Override
		protected ArrayList<SearchAddr> doInBackground(Void... params) {	
			
			ArrayList<SearchAddr> result = new ArrayList<SearchAddr>();

			double lat1 = 0, lon1 = 0;
			if(!setitem.lat.equals("")){
				lat1 = Double.parseDouble(setitem.lat);
			}
			if(!setitem.lon.equals("")){
				lon1 = Double.parseDouble(setitem.lon);
			}
			if(lat1 != 0){
				point2 = new LatLng(lat1, lon1);
				SearchAddr parkaddr = new SearchAddr();
				GetSearchLocation getaddress = new GetSearchLocation();
				String dis2 = getaddress.getDistance(point1, point2);
				String dis1 = getaddress.getRealDis();
				String interval1 = getaddress.getInterval();
				float d1 = 0, in1 = 0;
				if(!dis1.equals("")){
					d1 = (float)Double.parseDouble(dis1);
				}
				if(!interval1.equals("")){
					in1 = (float)Double.parseDouble(interval1);
				}
				if(d1 != 0 && in1 != 0){
					float m = (d1 / 1000.0f) / (in1 / 3600.0f);
					String s = "" + m;
					int idx = 0;
					if((idx = s.indexOf("."))!=-1){
						s = s.substring(0, idx+2);
					}
					parkaddr.speed = s;
				}else{
					parkaddr.speed = "0";
				}
				ShareData.destination = point2;
				parkaddr.addressname = setitem.title;
				parkaddr.dis = dis2;
				parkaddr.lat = lat1;
				parkaddr.lng = lon1;
				ShareData.des_addr = parkaddr;

			}
			return result;
		}
	}// Retrieve voucher web service async task
	private class GetSpeedTask extends
	AsyncTask<Void, Void, String> {
		ProgressDialog MyDialog;
       public GetSpeedTask() {
       }
		@Override
		protected void onPostExecute(String _result) {
			super.onPostExecute(_result);
			if(lastlocationtime1 == 0){
				km = "0";
				((MyTextView)findViewById(R.id.speed)).setText(km);
			}else{
				if(_result != null && !_result.equals("")){
					try{
						float k = ((float)Double.parseDouble(_result) / 1000.0f) / (((float)(currentlocationtime1 - lastlocationtime1) / 1000) / 3600);
						if(k > 120) k = 120.0f;
						k = (kk + k) / 2.0f;
						kk = k;
						String s = "" + k;
						int idx = 0;
						if((idx = s.indexOf(".")) != -1){
							km = s.substring(0, idx+2);
							((MyTextView)findViewById(R.id.speed)).setText(km);
							aspeed = (int)((float)aspeed + k) / 2;
						}
					}catch(Exception e){
						
					}
				}
			}
		}

		@Override
		protected String doInBackground(Void... params) {	
			
			ArrayList<SearchAddr> result = new ArrayList<SearchAddr>();
			String dis = null;

			CheckInternetAccess check = new CheckInternetAccess(RecordActivity1.this);
	        if (check.checkNetwork()) {	
				GetSearchLocation getaddress = new GetSearchLocation();
				if(po11 != null && po21 != null){
					dis = getaddress.getDistance1(po11, po21);
				}
	        }
			
			return dis;
		}
	}
    @Override
    protected void onDestroy() {
    	if(gpsManager != null){
            gpsManager.stopListening();
            gpsManager.setGPSCallback(null);
            
            gpsManager = null;
            
    	}
    	//screen unlock
        KeyguardManager keyguardManager = (KeyguardManager)getSystemService(Activity.KEYGUARD_SERVICE); 
        KeyguardLock lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE); 
        lock.reenableKeyguard();
            super.onDestroy();
    }

    private double convertSpeed(double speed){
            return ((speed * Constants.HOUR_MULTIPLIER) * Constants.UNIT_MULTIPLIERS[measurement_index]); 
    }

    private String measurementUnitString(int unitIndex){
            String string = "";
            
            switch(unitIndex)
            {
                    case Constants.INDEX_KM:                string = "km/h";        break;
                    case Constants.INDEX_MILES:     string = "mi/h";        break;
            }
            
            return string;
    }

    private double roundDecimal(double value, final int decimalPlace)
    {
            BigDecimal bd = new BigDecimal(value);
            
            bd = bd.setScale(decimalPlace, RoundingMode.HALF_UP);
            value = bd.doubleValue();
            
            return value;
    }

    private void setSpeedText(int textid,String text)
    {
            Spannable span = new SpannableString(text);
            int firstPos = text.indexOf(32);
            
            span.setSpan(sizeSpanLarge, 0, firstPos,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            span.setSpan(sizeSpanSmall, firstPos + 1, text.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            
            TextView tv = ((TextView)findViewById(textid));
            
            tv.setText(span);
    }
}
