package com.orangeeye;

import com.orangeeye.db.DBAdapter;
import com.orangeeye.db.OtherData;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Intents;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

public class ContactsActivity extends Activity implements OnClickListener{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_main);
        ((MyButton)findViewById(R.id.yesbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.nobt)).setOnClickListener(this);

    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.yesbt:
			//Value_Info.incomeflg = false;
	        /*DBAdapter.init(this);
	        OtherData setdata1 = DBAdapter.getOtherData("ntuc_data");
	        if(setdata1 == null){
	        	OtherData other = new OtherData(0,"ntuc_data", "0", "");
	        	DBAdapter.addOtherData(other);
	        }else{
	        	OtherData other = new OtherData(setdata1._id,"ntuc_data", "0", "");
	        	DBAdapter.updateOtherData(other);
	        }*/
			SharedPreferences sharedPreferences = PreferenceManager
            .getDefaultSharedPreferences(this);
		    Editor editor = sharedPreferences.edit();
		    editor.putString("contact_added","1");
		    editor.commit();
			Intent intent1 = new Intent(ContactsActivity.this, MMSActivity.class);
        	startActivity(intent1);
        	Intent intent = new Intent(Intents.Insert.ACTION);
        	// Sets the MIME type to match the Contacts Provider
        	intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
        	intent.putExtra(Intents.Insert.NAME, "Orange Force");
        	intent.putExtra(Intents.Insert.PHONE_TYPE, Phone.TYPE_MOBILE);
	    	intent.putExtra(Intents.Insert.SECONDARY_PHONE, "+6590882947");
        	intent.putExtra(Intents.Insert.PHONE_TYPE, Phone.TYPE_WORK);
        	intent.putExtra(Intents.Insert.PHONE, "+6567895000");
        	startActivity(intent);
        	finish();
			break;
		case R.id.nobt:
			//Value_Info.incomeflg = true;
	        /*DBAdapter.init(this);
	        OtherData setdata = DBAdapter.getOtherData("ntuc_data");
	        if(setdata == null){
	        	OtherData other = new OtherData(0,"ntuc_data", "1", "");
	        	DBAdapter.addOtherData(other);
	        }else{
	        	OtherData other = new OtherData(setdata._id,"ntuc_data", "1", "");
	        	DBAdapter.updateOtherData(other);
	        }*/
			Intent intent5 = new Intent(ContactsActivity.this, MMSActivity.class);
        	startActivity(intent5);
        	finish();
			break;
		}
	}

    
}
