package com.orangeeye.lib;

import java.util.HashMap;
import java.util.Map;
import java.net.URLEncoder;

public class RestParams {

	private Map<String, String> mParams;
	
	public RestParams(){
		mParams = null;
	}
	
	public void checkParamList(){
		if (mParams == null)
			mParams = new HashMap<String, String>();
	}
	
	public void addParam(String key, String value){
		
		checkParamList();
		
		mParams.put(key, value);		
	}
	
	public byte[] serializeParams(){
				
		return serializeAsString().getBytes();		
	}
	
	public String serializeAsString(){
		
		String strParamList = "";
		
		checkParamList();
		
		for(Map.Entry<String, String> entry: mParams.entrySet()){
			
			strParamList = strParamList + entry.getKey().toString() + "=" + URLEncoder.encode(entry.getValue().toString()) + "&";
		}
		
		return strParamList;
	}
}

