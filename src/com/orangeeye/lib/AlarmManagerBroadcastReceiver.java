package com.orangeeye.lib;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.orangeeye.OrangeSplashActivity;
import com.orangeeye.R;
import com.orangeeye.ReminderActivity;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class AlarmManagerBroadcastReceiver extends BroadcastReceiver {

 final public static String ONE_TIME = "onetime";


 @Override
 public void onReceive(Context context, Intent intent) {
   PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
         PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "YOUR TAG");
         //Acquire the lock
         wl.acquire();

         //You can do the processing here.

 		SharedPreferences sharedPreferences = PreferenceManager
                 .getDefaultSharedPreferences(context);
 		String sv = sharedPreferences.getString("set_month", "");
         StringBuilder msgStr = new StringBuilder();
         String flg = "0";
         if(intent != null){
        	 flg = intent.getStringExtra("flg");
         }
         if(intent != null && intent.getBooleanExtra(ONE_TIME, Boolean.FALSE)){
          //Make sure this intent has been sent by the one-time timer button.
          msgStr.append("One time Timer : ");
         }
         if(flg == null || flg.equals("0"))return;
         Format formatter = new SimpleDateFormat("hh:mm:ss a");
         msgStr.append(formatter.format(new Date()));

         //Toast.makeText(context, msgStr, Toast.LENGTH_LONG).show();
         int MY_NOTIFICATION_ID = 1;
     	NotificationManager notificationManager = null;
     	Notification myNotification = null;
     	String myBlog = "abc";
     	notificationManager = (NotificationManager)   context.getSystemService(Context.NOTIFICATION_SERVICE);
     	myNotification = new Notification(R.drawable.ic_launcher,
                 "Orange Reminder", System.currentTimeMillis());
         
         String notificationTitle = "Orange Eye";
         String notificationText = "";
         if(flg.equals("1")){
        	 notificationText = "Your policy is expiring in "+sv+"! Please call for plan renewal.";
         }else{
        	 notificationText = "Your road tax is expiring in "+sv+"! Please renew your road tax soon.";
         }
         Intent myIntent = new Intent(context, OrangeSplashActivity.class);
         PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, myIntent,
                 Intent.FLAG_ACTIVITY_NEW_TASK);
         myNotification.defaults |= Notification.DEFAULT_SOUND;
         myNotification.flags |= Notification.FLAG_AUTO_CANCEL;
         myNotification.setLatestEventInfo(context, notificationTitle,
                 notificationText, pendingIntent);
         notificationManager.notify(MY_NOTIFICATION_ID, myNotification);
         //Release the lock
         wl.release();
 }

 public void SetAlarm(Context context)
    {
        AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
        intent.putExtra(ONE_TIME, Boolean.FALSE);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        //After after 5 seconds
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+10000, 1000 * 5 , pi); 
    }

    public void CancelAlarm(Context context)
    {
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
        intent.putExtra("flg", "1");
        intent.setAction("com.orangeeye.action.ALERM");
        intent.putExtra(ONE_TIME, Boolean.TRUE);
        PendingIntent sender = PendingIntent.getBroadcast(context, 1, intent, 1);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
    public void CancelAlarm1(Context context)
    {
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
        intent.putExtra("flg", "2");
        intent.setAction("com.orangeeye.action.ALERM");
        intent.putExtra(ONE_TIME, Boolean.TRUE);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    public void setOnetimeTimer(Context context, long time, long time1){
     AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
        intent.putExtra("flg", "1");
        intent.setAction("com.orangeeye.action.ALERM");
        intent.putExtra(ONE_TIME, Boolean.TRUE);
        PendingIntent pi = PendingIntent.getBroadcast(context, 1, intent, 1);
        am.set(AlarmManager.RTC_WAKEUP, time, pi);
        am.set(AlarmManager.RTC_WAKEUP, time1, pi);
    }
    public void setOnetimeTimer1(Context context, long time, long val){
        AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
           Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
           intent.putExtra("flg", "2");
           intent.setAction("com.orangeeye.action.ALERM");
           intent.putExtra(ONE_TIME, Boolean.TRUE);
           PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
           am.set(AlarmManager.RTC_WAKEUP, time, pi);
           am.set(AlarmManager.RTC_WAKEUP, val, pi);
   }
}
