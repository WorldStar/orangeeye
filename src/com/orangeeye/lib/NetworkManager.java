package com.orangeeye.lib;
import java.util.List;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class NetworkManager
{
        private static final int gpsMinTime = 100;
        private static final int gpsMinDistance = 0;
        
        private static LocationManager locationManager = null;
        private static LocationListener locationListener = null;
        private static GPSCallback gpsCallback = null;
        
        public NetworkManager()
        {       
                NetworkManager.locationListener = new LocationListener()
                {
                        @Override
                        public void onLocationChanged(final Location location)
                        {
                                if (NetworkManager.gpsCallback != null)
                                {
                                        NetworkManager.gpsCallback.onGPSUpdate(location);
                                }
                        }
                        
                        @Override
                        public void onProviderDisabled(final String provider)
                        {
                        }
                        
                        @Override
                        public void onProviderEnabled(final String provider)
                        {
                        }
                        
                        @Override
                        public void onStatusChanged(final String provider, final int status, final Bundle extras)
                        {
                        }
                };
        }
        
        public GPSCallback getGPSCallback()
        {
                return NetworkManager.gpsCallback;
        }
        
        public void setGPSCallback(final GPSCallback gpsCallback)
        {
                NetworkManager.gpsCallback = gpsCallback;
        }
        
        public void startListening(final Context context)
        {        	
                if (NetworkManager.locationManager == null)
                {
                        NetworkManager.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                }
                
                final Criteria criteria = new Criteria();
                
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                criteria.setSpeedRequired(true);
                criteria.setAltitudeRequired(false);
                criteria.setBearingRequired(false);
                criteria.setCostAllowed(true);
                criteria.setPowerRequirement(Criteria.POWER_LOW);
                
                final String bestProvider = NetworkManager.locationManager.getBestProvider(criteria, true);
                
                if (bestProvider != null && bestProvider.length() > 0)
                {
                        NetworkManager.locationManager.requestLocationUpdates(bestProvider, NetworkManager.gpsMinTime,
                                        NetworkManager.gpsMinDistance, NetworkManager.locationListener);
                }
                else
                {
                        final List<String> providers = NetworkManager.locationManager.getProviders(true);
                        
                        for (final String provider : providers)
                        {
                                NetworkManager.locationManager.requestLocationUpdates(provider, NetworkManager.gpsMinTime,
                                                NetworkManager.gpsMinDistance, NetworkManager.locationListener);
                        }
                }
        }
        
        public void stopListening()
        {
                try
                {
                        if (NetworkManager.locationManager != null && NetworkManager.locationListener != null)
                        {
                                NetworkManager.locationManager.removeUpdates(NetworkManager.locationListener);
                        }
                        
                        NetworkManager.locationManager = null;
                }
                catch (final Exception ex)
                {
                        
                }
        }
}
