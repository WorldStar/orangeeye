package com.orangeeye.lib;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;
import com.orangeeye.data.SearchAddr;
import com.orangeeye.data.ShareData;

public class GetSearchLocation {
	public static String country = "MY";
	public String interval = "";
	public String dis1 = "";
	public void GetSearchLocation(){
		
	}
	public static JSONObject getLocationInfo(String address, int attempts, boolean success) {
		JSONObject jsonObject = null;

		while (success != true && attempts < 3){
			  attempts += 1;
			  HttpGet httpGet = new HttpGet("http://maps.google.com/maps/api/geocode/json?address=" +address+"&ka&sensor=false");
				HttpClient client = new DefaultHttpClient();
				HttpResponse response;
				StringBuilder stringBuilder = new StringBuilder();

				try {
					response = client.execute(httpGet);
					HttpEntity entity = response.getEntity();
					InputStream stream = entity.getContent();
					int b;
					while ((b = stream.read()) != -1) {
						stringBuilder.append((char) b);
					}
				} catch (ClientProtocolException e) {
				} catch (IOException e) {
				}

				jsonObject = new JSONObject();
				try {
					jsonObject = new JSONObject(stringBuilder.toString());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			  }
			  if (stringBuilder.toString().contains("OVER_QUERY_LIMIT")){
				  if(attempts == 3){
					  // error
					  jsonObject = null; break;
				  }
				  try {
					Thread.sleep(500);
					//getLocationInfo(address, attempts, success);
					//break;
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					 //retry
				 
			  }else{
				  success = true; break;
			  }
			  
		}
		

		return jsonObject;
	}
	public static void getCountryCode(LatLng point){
		JSONObject jsonObject = null;
		HttpGet httpGet = new HttpGet("http://maps.googleapis.com/maps/api/geocode/json?latlng="+point.latitude+","+point.longitude+"&sensor=true");
		HttpClient client = new DefaultHttpClient();
		HttpResponse response;
		StringBuilder stringBuilder = new StringBuilder();

		try {
			response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			InputStream stream = entity.getContent();
			int b;
			while ((b = stream.read()) != -1) {
				stringBuilder.append((char) b);
			}
		} catch (ClientProtocolException e) {
		} catch (IOException e) {
		}

		jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject(stringBuilder.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JSONArray arraylist = (JSONArray)jsonObject.get("results");
			for(int i = 0; i < arraylist.length(); i++){
				String aa = arraylist.getJSONObject(i).getString("address_components");
				if(aa.contains("MY")){
					ShareData.country = "MY";
					break;
				}else if(aa.contains("SG")){
					ShareData.country = "SG";
					break;
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static JSONObject getLocationInfo1(String address, int attempts, boolean success, LatLng po) {
		JSONObject jsonObject = null;
		
			  //HttpGet httpGet = new HttpGet("http://maps.googleapis.com/maps/api/geocode/json?address=" +address+"&components=country:"+ShareData.country+"&ka&sensor=false");
			  String s = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" +address+"&location="+po.latitude+","+po.longitude+"&radius=1000000&sensor=true&key=AIzaSyBo7cxgyEZMrHKypxA9_2JJSu7uvzH0khE";
				//HttpsClient client = new DefaultHttpClient();
				
				HttpResponse response;
				StringBuilder stringBuilder = new StringBuilder();

				try {
					URL url = new URL(s);
			        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			        conn.setReadTimeout(10000 /* milliseconds */);
			        conn.setConnectTimeout(15000 /* milliseconds */);
			        conn.setRequestMethod("GET");
			        conn.setDoInput(true);
			        // Starts the query
			        conn.connect();
					InputStream stream = null;
			        int response1 = conn.getResponseCode();
			        stream = conn.getInputStream();
					int b;
					while ((b = stream.read()) != -1) {
						stringBuilder.append((char) b);
					}
				} catch (ClientProtocolException e) {
				} catch (IOException e) {
				}

				jsonObject = new JSONObject();
				try {
					jsonObject = new JSONObject(stringBuilder.toString());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			    }
		

		return jsonObject;
	}

	public static JSONObject getLocationInfo3() {
		JSONObject jsonObject = null;
		
			  //HttpGet httpGet = new HttpGet("http://maps.googleapis.com/maps/api/geocode/json?address=" +address+"&components=country:"+ShareData.country+"&ka&sensor=false");
			  //String s = "http://tygra.mavenm.com/tygra/rest/promotion/listing?udid=NTUCINCOMEMLB-UUID-5514A254-94B8-45E0-96FB-22F54EE6F086";
			String s = "http://tygra.mavenm.com/tygra/rest/promotion/listing";
			//String s = "http://uat.splashinteractive.com.my/NTUC/deals.htm";
				//HttpsClient client = new DefaultHttpClient();
				
				HttpResponse response;
				StringBuilder stringBuilder = new StringBuilder();

				try {
					URL url = new URL(s);
			        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			        conn.setReadTimeout(10000 /* milliseconds */);
			        conn.setConnectTimeout(15000 /* milliseconds */);
			        conn.setRequestMethod("GET");
			        conn.setDoInput(true);
			        // Starts the query
			        conn.connect();
					InputStream stream = null;
			        int response1 = conn.getResponseCode();
			        stream = conn.getInputStream();
					int b;
					while ((b = stream.read()) != -1) {
						stringBuilder.append((char) b);
					}
				} catch (ClientProtocolException e) {
				} catch (IOException e) {
				}

				jsonObject = new JSONObject();
				try {
					//int idx = stringBuilder.toString().indexOf("message");
					//String m = stringBuilder.toString().substring(idx-2, stringBuilder.toString().length());
					//jsonObject = new JSONObject(m);
					
					jsonObject = new JSONObject(stringBuilder.toString());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			    }
		

		return jsonObject;
	}

	public static JSONObject getLocationInfo2(String address, int attempts, boolean success, SearchAddr searchaddr) {
		JSONObject jsonObject = null;

			  String s = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+searchaddr.lat+","+searchaddr.lng+"&radius=10000&sensor=true&name="+address+"&key=AIzaSyBo7cxgyEZMrHKypxA9_2JJSu7uvzH0khE";
				//HttpClient client = new DefaultHttpClient();
				//HttpResponse response;
				StringBuilder stringBuilder = new StringBuilder();

				try {
					URL url = new URL(s);
			        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			        conn.setReadTimeout(10000 /* milliseconds */);
			        conn.setConnectTimeout(15000 /* milliseconds */);
			        conn.setRequestMethod("GET");
			        conn.setDoInput(true);
			        // Starts the query
			        conn.connect();
					InputStream stream = null;
			        int response1 = conn.getResponseCode();
			        stream = conn.getInputStream();
					int b;
					while ((b = stream.read()) != -1) {
						stringBuilder.append((char) b);
					}
				} catch (ClientProtocolException e) {
				} catch (IOException e) {
				}

				jsonObject = new JSONObject();
				try {
					jsonObject = new JSONObject(stringBuilder.toString());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			    }
		

		return jsonObject;
	}
	
	public static LatLng getGeoPoint(JSONObject jsonObject) {

		Double lon = new Double(0);
		Double lat = new Double(0);

		try {

			lon = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
				.getJSONObject("geometry").getJSONObject("location")
				.getDouble("lng");

			lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
				.getJSONObject("geometry").getJSONObject("location")
				.getDouble("lat");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new LatLng(lat, lon);//;GeoPoint((int) (lat * 1E6), (int) (lon * 1E6));

	}
	public String getInterval(){
		return interval;
	}
	public String getRealDis(){
		return dis1;
	}
	public String getDistance(LatLng pos1, LatLng pos2){
		StringBuffer jsonString = new StringBuffer();

		HttpPost httpPost = new HttpPost("http://maps.googleapis.com/maps/api/directions/json?origin="+pos1.latitude+","+pos1.longitude+"&destination="+pos2.latitude+","+pos2.longitude+"&sensor=false&mode=driving");
		HttpClient client = new DefaultHttpClient();
		HttpResponse httpResponse;
		String dis = "";
		        try {
		            //httpPost.setEntity(new UrlEncodedFormEntity(params));
		             httpResponse = client.execute(httpPost);

		            InputStream in = httpResponse.getEntity().getContent();
		            int ch = 0;
		            while ((ch = in.read()) != -1) {
		                jsonString.append((char) ch);
		            }
		            in.close();

		            JSONObject jsonObject = new JSONObject(jsonString.toString());
		            JSONObject jsonob = ((JSONArray)jsonObject.get("routes")).getJSONObject(0);
		            JSONArray ja = ((JSONArray)jsonob.get("legs"));
		            JSONObject ja2 = ja.getJSONObject(0);
		            JSONObject ja1 = ((JSONObject)ja2.get("distance"));
		            dis = ja1.getString("text");
		            dis1 = ja1.getString("value");
		            JSONObject ja3 = ((JSONObject)ja2.get("duration"));
		            interval = ja3.getString("value");
		            
		            
		            if(dis == null)dis = "";
		            return dis;



		        } catch (Exception e) {
		            // TODO Auto-generated catch block
		            e.printStackTrace();
		        }
		        return dis;

		}
	public String getDistance1(LatLng pos1, LatLng pos2){
		StringBuffer jsonString = new StringBuffer();

		HttpPost httpPost = new HttpPost("http://maps.googleapis.com/maps/api/directions/json?origin="+pos1.latitude+","+pos1.longitude+"&destination="+pos2.latitude+","+pos2.longitude+"&sensor=false&mode=driving");
		HttpClient client = new DefaultHttpClient();
		HttpResponse httpResponse;
		String dis = "";
		        try {
		            //httpPost.setEntity(new UrlEncodedFormEntity(params));
		             httpResponse = client.execute(httpPost);

		            InputStream in = httpResponse.getEntity().getContent();
		            int ch = 0;
		            while ((ch = in.read()) != -1) {
		                jsonString.append((char) ch);
		            }
		            in.close();

		            JSONObject jsonObject = new JSONObject(jsonString.toString());
		            JSONObject jsonob = ((JSONArray)jsonObject.get("routes")).getJSONObject(0);
		            JSONArray ja = ((JSONArray)jsonob.get("legs"));
		            JSONObject ja2 = ja.getJSONObject(0);
		            JSONObject ja1 = ((JSONObject)ja2.get("distance"));
		            dis = ja1.getString("value");
		            
		            
		            if(dis == null)dis = "";
		            return dis;



		        } catch (Exception e) {
		            // TODO Auto-generated catch block
		            e.printStackTrace();
		        }
		        return dis;

		}
	//GeoPoint srcGeoPoint =getGeoPoint(getLocationInfo(fromAddress.replace("\n"," ").replace(" ", "%20")));
			//GeoPoint destGeoPoint =getGeoPoint(getLocationInfo(CalDescription.toAddress.replace("\n"," ").replace(" ", "%20")));
}
