package com.orangeeye.lib;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.orangeeye.OrangeSplashActivity;
import com.orangeeye.R;
import com.orangeeye.ReminderActivity;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.widget.Toast;

public class AlarmManagerBroadcastReceiver1 extends BroadcastReceiver {

 final public static String ONE_TIME = "onetime1";

 @Override
 public void onReceive(Context context, Intent intent) {
   PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
         PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "YOUR TAG");
         //Acquire the lock
         wl.acquire();

         //You can do the processing here.
         Bundle extras = intent.getExtras();
         StringBuilder msgStr = new StringBuilder();
         
         if(extras != null && extras.getBoolean(ONE_TIME, Boolean.FALSE)){
          //Make sure this intent has been sent by the one-time timer button.
          msgStr.append("One time Timer : ");
         }
         Format formatter = new SimpleDateFormat("hh:mm:ss a");
         msgStr.append(formatter.format(new Date()));

         //Toast.makeText(context, msgStr, Toast.LENGTH_LONG).show();
         int MY_NOTIFICATION_ID = 1;
     	NotificationManager notificationManager = null;
     	Notification myNotification = null;
     	String myBlog = "abc";
     	notificationManager = (NotificationManager)   context.getSystemService(Context.NOTIFICATION_SERVICE);
     	myNotification = new Notification(R.drawable.ic_launcher,
                 "Orange Reminder", System.currentTimeMillis());
         
         String notificationTitle = "Orange Eye";
         String notificationText = "Your road tax is expiring on (according to settings)! Please renew your road tax soon.";
         Intent myIntent = new Intent(context, OrangeSplashActivity.class);
         PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, myIntent,
                 Intent.FLAG_ACTIVITY_NEW_TASK);
         myNotification.defaults |= Notification.DEFAULT_SOUND;
         myNotification.flags |= Notification.FLAG_AUTO_CANCEL;
         myNotification.setLatestEventInfo(context, notificationTitle,
                 notificationText, pendingIntent);
         notificationManager.notify(MY_NOTIFICATION_ID, myNotification);
         //Release the lock
         wl.release();
 }

 public void SetAlarm(Context context)
    {
        AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver1.class);
        intent.putExtra(ONE_TIME, Boolean.FALSE);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        //After after 5 seconds
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+10000, 1000 * 5 , pi); 
    }

    public void CancelAlarm1(Context context)
    {
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver1.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    public void setOnetimeTimer1(Context context, long time){
        AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
           Intent intent = new Intent(context, AlarmManagerBroadcastReceiver1.class);
           intent.putExtra(ONE_TIME, Boolean.TRUE);
           PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
           am.set(AlarmManager.RTC_WAKEUP, time, pi);
   }
}
