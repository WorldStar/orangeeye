package com.orangeeye.utils;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XMLParserHelper {

	
	public static Document getDocumentFromString(String content) throws Exception{
		
		DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
		
		DocumentBuilder builder = df.newDocumentBuilder();
		
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(content));
		
		Document doc = builder.parse(is);
		
		return doc;
	}
	
	public static String getStringIfSingleNode(Node node) throws Exception{
		
		int childElementsCount = XMLParserHelper.getChildElementsCount(node);
		
		Node firstChild = node.getFirstChild();
		
		if (childElementsCount == 0 && firstChild.getNodeType() == Node.TEXT_NODE){
			return firstChild.getNodeValue();
		}
		
		return null; 
	}
	
	public static String getStringOfSingleNode(Node node) throws Exception{
		
		Node firstChild = node.getFirstChild();
		
		if (firstChild != null && firstChild.getNodeType() == Node.TEXT_NODE) return firstChild.getNodeValue();
		
		return "";
		
	}
	
	public static short getShortOfSingleNode(Node node) throws Exception{
		
		String val = getStringOfSingleNode(node).trim();
		
		if (val == "") return -1;
		
		try{
			return Short.parseShort(val);
		}
		catch(Exception ex){
			return -1;
		}
		
	}
	
	public static int getChildElementsCount(Node node) throws Exception{
	
		int count = 0;
		
		NodeList list = node.getChildNodes();
		
		for (int i = 0 ; i < list.getLength(); i++){
			
			Node item = list.item(i);
			
			if (item.getNodeType() == Node.ELEMENT_NODE)
				count++;
		}
		
		return count;
	}
	
	public static Node getChildNodeByTagName(Node node, String tagName) throws Exception{
		
		NodeList list = node.getChildNodes();
		
		for (int i = 0 ; i < list.getLength(); i++){
			
			Node item = list.item(i);
			
			if (item.getNodeType() != Node.ELEMENT_NODE) continue;
					
			if (((Element)item).getTagName().trim().toLowerCase().compareTo(tagName.trim().toLowerCase()) == 0)
				return item;
		}
		
		return null;
	}
	/*************Sin updated 2013-10-28**********************************/
	public static Node getChildLifeNodeByTagName(Node node, String tagName, int k) throws Exception{
		
		NodeList list = node.getChildNodes();
		int m = 0;
		for (int i = 0 ; i < list.getLength(); i++){
			
			Node item = list.item(i);
			
			if (item.getNodeType() != Node.ELEMENT_NODE) continue;
					
			if (((Element)item).getTagName().trim().toLowerCase().compareTo(tagName.trim().toLowerCase()) == 0){
				if(k == m){
					return item;
				}
				m++;
			}
		}
		
		return null;
	}
	/*************Sin updated end**********************************/
}
