package com.orangeeye;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.orangeeye.data.ShareData;
import com.orangeeye.db.DBAdapter;
import com.orangeeye.db.OtherData;
import com.orangeeye.db.RecordData;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;
import android.widget.VideoView;

public class GalleryVideoActivity extends Activity implements OnClickListener,OnCompletionListener, OnErrorListener, OnInfoListener,
OnPreparedListener, OnSeekCompleteListener, OnVideoSizeChangedListener,OnSeekBarChangeListener,
SurfaceHolder.Callback, MediaController.MediaPlayerControl{
	LinearLayout share_screen_layout;
	Gallery gallery;
	Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    int pos = 0;
    boolean playflg = false;
    boolean startflg = false;
    MediaController mediaController;
    RelativeLayout bottom_layout;
    Display currentDisplay;

	SurfaceView surfaceView;
	SurfaceHolder surfaceHolder;

	MediaPlayer mediaPlayer;
    MediaController controller;

	int videoWidth = 0;
	int videoHeight = 0;

	boolean readyToPlay = false;
	SeekBar seekbar;
	public final static String LOGTAG = "CUSTOM_VIDEO_PLAYER";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_screen);
        ((MyButton)findViewById(R.id.backbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.playbt)).setOnClickListener(this);
        bottom_layout = (RelativeLayout)findViewById(R.id.bottom_layout);
        
    }
    @Override
    public void onResume(){
    	super.onResume();
    	playflg = false;
    	startflg = false;
    	surfaceView = (SurfaceView) this.findViewById(R.id.SurfaceView);
		surfaceHolder = surfaceView.getHolder();
		surfaceHolder.addCallback(this);
		surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		String dd = ShareData.selected_record.record_time;
		int idx = dd.indexOf(":");
		String s = dd.substring(idx-2,idx+3 ) + dd.substring(dd.length()-3,dd.length() );
		String s2 = dd.substring(0, idx-3);
		((MyTextView)findViewById(R.id.timetx)).setText(s+" "+ s2);
		((MyTextView)findViewById(R.id.speedtx)).setText(ShareData.selected_record.record_speed);
		((MyTextView)findViewById(R.id.desttx)).setText(ShareData.selected_record.record_destination);
		((MyTextView)findViewById(R.id.starttx)).setText(ShareData.selected_record.record_start);
		((MyButton)findViewById(R.id.playbt)).setVisibility(View.VISIBLE);
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setOnCompletionListener(this);
		mediaPlayer.setOnErrorListener(this);
		mediaPlayer.setOnInfoListener(this);
		mediaPlayer.setOnPreparedListener(this);
		mediaPlayer.setOnSeekCompleteListener(this);
		mediaPlayer.setOnVideoSizeChangedListener(this);
		mediaPlayer.setScreenOnWhilePlaying(true);
		String filePath = Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + ShareData.selected_record.record_file;
		try {
			mediaPlayer.setDataSource(filePath);
		} catch (IllegalArgumentException e) {
			Log.v(LOGTAG, e.getMessage());
			finish();
		} catch (IllegalStateException e) {
			Log.v(LOGTAG, e.getMessage());
			finish();
		} catch (IOException e) {
			Log.v(LOGTAG, e.getMessage());
			finish();
		}

	    controller = new MediaController(this);

		currentDisplay = getWindowManager().getDefaultDisplay();
		 
		seekbar = (SeekBar)findViewById(R.id.seekBar1);
		seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				int s = seekbar.getProgress();
				seekTo(seekbar.getProgress());
				//start();
			}
			
		});
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.backbt:
			startflg = false;
			if (mediaPlayer.isPlaying()) {
	            mediaPlayer.pause();
	        }
			mediaPlayer.release();
			Intent intent = new Intent(GalleryVideoActivity.this, GalleryPreActivity.class);
        	startActivity(intent);
        	finish();
			break;
		case R.id.playbt:
			if(playflg){
				((MyButton)findViewById(R.id.playbt)).setVisibility(View.GONE);
				start();
				startflg = true;
				new SeekThread().start();
			}
			break;
		}
	}

	 @Override
	 public void onBackPressed() {
			startflg = false;
		mediaPlayer.release();
		Intent intent1 = new Intent(GalleryVideoActivity.this, GalleryPreActivity.class);
    	startActivity(intent1);
    	finish();
	 }

	@Override
	public boolean canPause() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean canSeekBackward() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean canSeekForward() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public int getBufferPercentage() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getCurrentPosition() {
		// TODO Auto-generated method stub
		return mediaPlayer.getCurrentPosition();
	}

	@Override
	public int getDuration() {
		// TODO Auto-generated method stub
		return mediaPlayer.getDuration();
	}

	@Override
	public boolean isPlaying() {
		// TODO Auto-generated method stub
		return mediaPlayer.isPlaying();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }
	}

	@Override
	public void seekTo(int pos) {
		// TODO Auto-generated method stub
		mediaPlayer.seekTo(pos);
	}
	@Override
	public void onPause(){
		startflg = false;
		mediaPlayer.release();
		super.onPause();
	}
	@Override
	public void start() {
		// TODO Auto-generated method stub
		((MyButton)findViewById(R.id.playbt)).setVisibility(View.GONE);
		surfaceHolder.setFixedSize(Value_Info.width, Value_Info.height);
		mediaPlayer.start();
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		Log.v(LOGTAG, "surfaceCreated Called");

		mediaPlayer.setDisplay(holder);

		try {
			mediaPlayer.prepare();
		} catch (IllegalStateException e) {
			Log.v(LOGTAG, e.getMessage());
			Toast.makeText(GalleryVideoActivity.this, "Can not play.", Toast.LENGTH_SHORT);
			Intent intent = new Intent(GalleryVideoActivity.this, GalleryPreActivity.class);
	    	startActivity(intent);
			finish();
		} catch (IOException e) {
			Log.v(LOGTAG, e.getMessage());
			Toast.makeText(GalleryVideoActivity.this, "Can not play.", Toast.LENGTH_SHORT);
			Intent intent = new Intent(GalleryVideoActivity.this, GalleryPreActivity.class);
	    	startActivity(intent);
			finish();
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSeekComplete(MediaPlayer mp) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		// TODO Auto-generated method stub
		Log.v(LOGTAG, "onPrepared Called");
		videoWidth = mp.getVideoWidth();
		videoHeight = mp.getVideoHeight();

		if (videoWidth > currentDisplay.getWidth()
				|| videoHeight > currentDisplay.getHeight()) {
			float heightRatio = (float) videoHeight
					/ (float) currentDisplay.getHeight();
			float widthRatio = (float) videoWidth
					/ (float) currentDisplay.getWidth();

			if (heightRatio > 1 || widthRatio > 1) {
				if (heightRatio > widthRatio) {
					videoHeight = (int) Math.ceil((float) videoHeight
							/ (float) heightRatio);
					videoWidth = (int) Math.ceil((float) videoWidth
							/ (float) heightRatio);
				} else {
					videoHeight = (int) Math.ceil((float) videoHeight
							/ (float) widthRatio);
					videoWidth = (int) Math.ceil((float) videoWidth
							/ (float) widthRatio);
				}
			}
		}
		((MyButton)findViewById(R.id.playbt)).setVisibility(View.GONE);
		surfaceView.setLayoutParams(new LinearLayout.LayoutParams(videoWidth * Value_Info.height / videoHeight,
				Value_Info.height));
		//surfaceHolder.setFixedSize(Value_Info.width, Value_Info.height);
		mp.start();

		startflg = true;
		new SeekThread().start();
		seekbar.setMax(mediaPlayer.getDuration());
		displayDuration(mediaPlayer.getDuration());
		controller.setMediaPlayer(this);
	    controller.setAnchorView(this.findViewById(R.id.MainView));
	    controller.setEnabled(true);
	    //controller.show(10000);
	    
	    playflg = true;
	}
	private String getTimeString(long millis) {
	    StringBuffer buf = new StringBuffer();

	    int hours = (int)(millis / (1000*60*60));
	    int minutes = (int)(( millis % (1000*60*60) ) / (1000*60));
	    int seconds = (int)(( ( millis % (1000*60*60) ) % (1000*60) ) / 1000);

	    buf .append(String.format("%02d", minutes))
	        .append(":")
	        .append(String.format("%02d", seconds));

	    return buf.toString();
	}
	public void displayDuration(int duration){		
		
		((MyTextView)findViewById(R.id.durationtx)).setText(getTimeString(duration));
	}
	@Override
	public void onCompletion(MediaPlayer mp) {
		// TODO Auto-generated method stub
		//Intent intent = new Intent(GalleryVideoActivity.this, GalleryActivity.class);
    	//startActivity(intent);
		//finish();
		int pro = mediaPlayer.getCurrentPosition();
		startflg = false;
		((MyButton)findViewById(R.id.playbt)).setVisibility(View.VISIBLE);
	}

	@Override
	public boolean onInfo(MediaPlayer mp, int what, int extra) {
		// TODO Auto-generated method stub
		if (what == MediaPlayer.MEDIA_INFO_BAD_INTERLEAVING) {
			Log.v(LOGTAG, "Media Info, Media Info Bad Interleaving " + extra);
		} else if (what == MediaPlayer.MEDIA_INFO_NOT_SEEKABLE) {
			Log.v(LOGTAG, "Media Info, Media Info Not Seekable " + extra);
		} else if (what == MediaPlayer.MEDIA_INFO_UNKNOWN) {
			Log.v(LOGTAG, "Media Info, Media Info Unknown " + extra);
		} else if (what == MediaPlayer.MEDIA_INFO_VIDEO_TRACK_LAGGING) {
			Log.v(LOGTAG, "MediaInfo, Media Info Video Track Lagging " + extra);
			/*
			 * Android Version 2.0 and Higher } else if (whatInfo ==
			 * MediaPlayer.MEDIA_INFO_METADATA_UPDATE) {
			 * Log.v(LOGTAG,"MediaInfo, Media Info Metadata Update " + extra);
			 */
		}
		return false;
	}

	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		// TODO Auto-generated method stub
		if (what == MediaPlayer.MEDIA_ERROR_SERVER_DIED) {
			Log.v(LOGTAG, "Media Error, Server Died " + extra);
		} else if (what == MediaPlayer.MEDIA_ERROR_UNKNOWN) {
			Log.v(LOGTAG, "Media Error, Error Unknown " + extra);
		}

		return false;
	}
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
        if (controller.isShowing()) {
        	//controller.show(10000);
        } else {
           // controller.show(10000);
        }
        return false;
    }

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}
	class SeekThread extends Thread{
		public SeekThread(){
			
		}
		public void run(){
			while(startflg){
				final int pro = mediaPlayer.getCurrentPosition();
				seekbar.setProgress(pro);
				runOnUiThread(new Runnable(){

				    @Override
				    public void run() {
						((MyTextView)findViewById(R.id.seektx)).setText(getTimeString(pro) + " :");
				    }
				});
			}
		}
	}
}
