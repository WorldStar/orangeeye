package com.orangeeye;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Simon on 20/4/2014.
 */
public class BlankActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blank);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}