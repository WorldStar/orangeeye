package com.orangeeye;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.FacebookRequestError;
import com.facebook.LoggingBehavior;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.RequestBatch;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.facebook.UiLifecycleHelper;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;
import com.facebook.android.Util;
import com.facebook.internal.Utility;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.orangeeye.data.SearchAddr;
import com.orangeeye.data.ShareData;
import com.orangeeye.db.DBAdapter;
import com.orangeeye.db.RecordData;
import com.orangeeye.ffmpeg.FfmpegJob;
import com.orangeeye.ffmpeg.Utils;
import com.orangeeye.lib.CheckInternetAccess;
import com.orangeeye.lib.ReturnMessage;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;
import com.orangeeye.youtube.YoutubeUploadRequest;
import com.orangeeye.youtube.YoutubeUploader;
import com.orangeeye.youtube.YoutubeUploader.ProgressListner;

import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.StatFs;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.MediaStore.Video;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.app.KeyguardManager.KeyguardLock;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Paint.Align;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

public class GalleryPreActivity extends Activity implements OnClickListener{
	LinearLayout share_screen_layout;
	Gallery gallery;
	List<RecordData> recordlist = new ArrayList<RecordData>();
	int pos = 0;
    private String mFfmpegInstallPath;
	private String mMarkInstallPath;
	private String recordfilename;
	private String cutfilename;
	private String tempfilename;
	private String realrecordfilepath;
	ProgressDialog progressDialog;
	public GraphUser mFBUserInfo = null;
	Session session;
    private UiLifecycleHelper uiHelper;
    Bundle bundle;
    public static boolean login_flg = false;
    public static boolean doneflg = false;
    float depth = 0;
    public ProgressBar progressBar;
    SharedPreferences sharedPreferences;
    Editor editor;
	Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
	private Session.StatusCallback statusCallback = new SessionStatusCallback();
	private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = savedInstanceState;
        sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
		depth = metrics.density;
        setContentView(R.layout.gallery_screen);
        ((MyButton)findViewById(R.id.rightbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.leftbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.backbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.sharebt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.playbt)).setOnClickListener(this);
        ((ImageView)findViewById(R.id.play1bt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.deletebt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.cancelbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.emailbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.incomebt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.fbbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.youtubebt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.instagrambt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.whatsappbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.linebt)).setOnClickListener(this);
        ((Button)findViewById(R.id.uploadbt)).setOnClickListener(this);
        ((Button)findViewById(R.id.cancelyoutubebt)).setOnClickListener(this);
        ((ScrollView)findViewById(R.id.scrollView1)).setVisibility(View.GONE);
        try{
	        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
	        double sdAvailsize = (double)stat.getAvailableBlocks() * (double)stat.getBlockSize();
	        double megasize = sdAvailsize *1014 / 1073741824;
	        String size = "" + megasize;
	        int index = 0;
	        if((index = size.indexOf(".")) != -1){
	        	size = size.substring(0,index);
	        }
	        if(!size.equals("") && Integer.parseInt(size) < 40){
		        ReturnMessage.showAlartDialog(this, "Not enough disk space to start video recording. Please delete old or unnecessary files and try again.");
	        	Intent intent = new Intent(GalleryPreActivity.this, HomeActivity.class);
	        	startActivity(intent);
	        	finish();
	        }
        }catch(Exception e){            
        }
        share_screen_layout = (LinearLayout)findViewById(R.id.share_screen_layout);
        share_screen_layout.setVisibility(View.GONE);
        DBAdapter.init(this);
        new GetDataTask().execute();
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        //Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);

 		//uiHelper = new UiLifecycleHelper(this, callback);
        //uiHelper.onCreate(bundle);
		
		
        /*Session session = Session.getActiveSession();
        if (session == null) {
            if (savedInstanceState != null) {
                session = Session.restoreSession(this, null, statusCallback, savedInstanceState);
            }
            if (session == null) {
                session = new Session(this);
            }
            Session.setActiveSession(session);
            if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
                session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
            }
        }*/
    }

	private class GetDataTask extends AsyncTask<Void, Void, List<RecordData>>{			
				
		// Downloading data in non-ui thread
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected List<RecordData> doInBackground(Void... params) {
				
			// For storing data from web service
			List<RecordData> data = new ArrayList<RecordData>();
			data = DBAdapter.getAllRecordData();
			return data;		
		}
		
		// Executes in UI thread, after the execution of
		// doInBackground()
		@Override
		protected void onPostExecute(List<RecordData> result) {			
			super.onPostExecute(result);			
			recordlist.clear();
			recordlist.addAll(result);	
			if(result.size() > 0){
				for(int i = 0; i < recordlist.size(); i++){
					RecordData dd = recordlist.get(i);
					String path = Environment.getExternalStorageDirectory() + "/OrangeEyeRecord/"+ dd.record_file;
					File f = new File(path);
					if(!f.exists()){
						DBAdapter.deleteRecordData(recordlist.get(i));
						recordlist.remove(i);						
					}
				}
				if(ShareData.selected_record_id > 0){
					pos = ShareData.selected_record_id;
					displayRecordData(ShareData.selected_record_id);
				}else{
					pos = 0;
					displayRecordData(0);
				}
			}else{
				ReturnMessage.showAlartDialog(GalleryPreActivity.this, "Sorry!", "No videos to display.");
				pos = -1;
				((LinearLayout)findViewById(R.id.g_layout)).setVisibility(View.GONE);
		        ((MyButton)findViewById(R.id.sharebt)).setClickable(false);
		        ((MyButton)findViewById(R.id.playbt)).setClickable(false);
		        ((ImageView)findViewById(R.id.play1bt)).setClickable(false);
		        ((MyButton)findViewById(R.id.deletebt)).setClickable(false);
			}
		}		
	}
	public void displayRecordData(int position){
		if(isSDPresent){
		// image thumbnail get
			if(recordlist.size() == 0){
				ReturnMessage.showAlartDialog(this, "Sorry!", "No videos to display.");
				((MyTextView)findViewById(R.id.gallery_title)).setText("No record data");
				((LinearLayout)findViewById(R.id.g_layout)).setVisibility(View.GONE);
		        ((MyButton)findViewById(R.id.sharebt)).setClickable(false);
		        ((MyButton)findViewById(R.id.playbt)).setClickable(false);
		        ((ImageView)findViewById(R.id.play1bt)).setClickable(false);
		        ((MyButton)findViewById(R.id.deletebt)).setClickable(false);
				return;
			}
			((LinearLayout)findViewById(R.id.g_layout)).setVisibility(View.VISIBLE);
			// text
			RecordData recorddata = recordlist.get(position);
			//ReturnMessage.showAlartDialog(this, ""+recordlist.size()+":pos="+position+":file="+recorddata.record_file);
			String path = Environment.getExternalStorageDirectory() + "/OrangeEyeRecord/"+ recorddata.record_file;
			Bitmap thumb = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Images.Thumbnails.MINI_KIND);
			Drawable d =new BitmapDrawable(thumb);
			int idx = recorddata.record_time.indexOf(":");
			int idx1 = recorddata.record_time.indexOf(",");
			String s = recorddata.record_time.substring(idx-2,idx+3 ) + recorddata.record_time.substring(recorddata.record_time.length()-3,recorddata.record_time.length() );
			String s2 = recorddata.record_time.substring(0, idx1+6);
			((MyTextView)findViewById(R.id.gallery_title)).setText("Recorded on "+ s + " " + s2);
			((LinearLayout)findViewById(R.id.thumbnail)).setBackgroundDrawable(d);
			((MyTextView)findViewById(R.id.startPos)).setText(recorddata.record_start);
			((MyTextView)findViewById(R.id.destinationPos)).setText(recorddata.record_destination);
			((MyTextView)findViewById(R.id.distance)).setText("Distance: "+recorddata.record_distance);
			((MyTextView)findViewById(R.id.average)).setText("Average Speed: "+recorddata.record_speed+" KM/H");
			recordfilename = Environment.getExternalStorageDirectory() + "/OrangeEyeRecord/OrangeEye.mp4";
			tempfilename = Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file;
			//File f = new File(recordfilename);
			//if(f.exists()){
				//f.delete();
			//}
		}
			
	}

    @Override
    public void onResume() {
    	getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onResume();
        

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Override
    public void onPause() {
        super.onPause();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        //Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
        //facebook_video_upload(recordfilename);
        //video_upload();
    	//super.onActivityResult(requestCode, resultCode, data);
        //uiHelper.onActivityResult(requestCode, resultCode, data);

      if (resultCode == RESULT_OK) {
        String myValue = data.getStringExtra("valueName"); 
        // use 'myValue' return value here
      }
    }

	@Override
	public void onSaveInstanceState(Bundle outState) {
	    //super.onSaveInstanceState(outState);
	    //uiHelper.onSaveInstanceState(outState);
	}

    private class SessionStatusCallback implements Session.StatusCallback {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
        	if (!session.isOpened() && !session.isClosed()) {
        		
        	}else{
        		//facebook_video_upload(recordfilename);
        	}
        	//facebook_video_upload(recordfilename);
        }
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.backbt:
			Intent intent = new Intent(GalleryPreActivity.this, HomeActivity.class);
        	startActivity(intent);
        	finish();
			break;
		case R.id.rightbt:
			if(pos < recordlist.size() - 1){
				pos++;
				displayRecordData(pos);
			}
			break;
		case R.id.leftbt:
			if(pos > 0){
				pos--;
				displayRecordData(pos);
			}
			break;
		case R.id.sharebt:

			CheckInternetAccess check = new CheckInternetAccess(this);
	        if (!check.checkNetwork()) {
	        	ReturnMessage.showAlartDialog(this, "No internet connection.");
	        	return;
	        }
	        ((MyButton)findViewById(R.id.rightbt)).setClickable(false);
	        ((MyButton)findViewById(R.id.leftbt)).setClickable(false);
	        ((MyButton)findViewById(R.id.backbt)).setClickable(false);
	        ((MyButton)findViewById(R.id.sharebt)).setClickable(false);
	        ((MyButton)findViewById(R.id.playbt)).setClickable(false);
	        ((ImageView)findViewById(R.id.play1bt)).setClickable(false);
	        ((MyButton)findViewById(R.id.deletebt)).setClickable(false);
			share_screen_layout.setVisibility(View.VISIBLE);
			processVideo();
			break;
		case R.id.playbt:
			if(recordlist.size() == 0)return;
			if(isSDPresent){
				if(recordlist.get(pos).record_start == null || recordlist.get(pos).record_start.equals("")){
					ReturnMessage.showAlartDialog(GalleryPreActivity.this, "Location information not available.");
					return;
				}
				if(recordlist.get(pos).record_destination == null || recordlist.get(pos).record_destination.equals("")){
					ReturnMessage.showAlartDialog(GalleryPreActivity.this, "Location information not available.");
					return;
				}
				ShareData.selected_record = recordlist.get(pos);
				ShareData.selected_record_id = pos;
				/*Intent intent11 = new Intent(GalleryActivity.this, GalleryVideoActivity.class);
	        	startActivity(intent11);
	        	finish();
	        	*/
				CheckInternetAccess check1 = new CheckInternetAccess(this);
		        if (!check1.checkNetwork()) {
		        	ReturnMessage.showAlartDialog(this, "No internet connection.");
		        	return;
		        }
				Intent intent3 = new Intent(GalleryPreActivity.this, NavigationActivity.class);
	        	startActivity(intent3);
				finish();
			}else{
				Toast.makeText(this, "No SD Card.", 500);
			}
			break;
		case R.id.play1bt:
			if(recordlist.size() == 0)return;
			if(isSDPresent){
				ShareData.selected_record = recordlist.get(pos);
				ShareData.selected_record_id = pos;
				Intent intent12 = new Intent(GalleryPreActivity.this, GalleryVideoActivity.class);
	        	startActivity(intent12);
	        	finish();
			}else{
				Toast.makeText(this, "No SD Card.", 500);
			}
			break;
		case R.id.deletebt:				
			delete_func();
			break;
		case R.id.cancelbt:
			if(cutfilename != null && !cutfilename.equals("")){
				File f = new File(cutfilename);
				if(f.exists()){
					f.delete();
				}
			}
			String newfile = Environment.getExternalStorageDirectory() + "/OrangeEyeRecord/watermark.mp4";
	
			if(newfile != null && !newfile.equals("")){
				File f = new File(newfile);
				if(f.exists()){
					f.delete();
				}
			}
	        ((MyButton)findViewById(R.id.rightbt)).setOnClickListener(this);
	        ((MyButton)findViewById(R.id.leftbt)).setOnClickListener(this);
	        ((MyButton)findViewById(R.id.backbt)).setOnClickListener(this);
	        ((MyButton)findViewById(R.id.sharebt)).setOnClickListener(this);
	        ((MyButton)findViewById(R.id.playbt)).setOnClickListener(this);
	        ((ImageView)findViewById(R.id.play1bt)).setOnClickListener(this);
	        ((MyButton)findViewById(R.id.deletebt)).setOnClickListener(this);
			share_screen_layout.setVisibility(View.GONE);
			break;
		case R.id.emailbt:
			/*Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
		            "mailto","abc@gmail.com", null));
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, "EXTRA_SUBJECT");
			startActivity(Intent.createChooser(emailIntent, "Send email..."));
			*/
			if(pos > -1){
				ArrayList<String> file = new ArrayList<String>();
				//file.add(recordlist.get(pos).record_file);
				file.add("OrangeEye.mp4");
				try {
					sendEmailMulipleFiles(this,"", "Orange Eye", "", file);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					 ReturnMessage.showAlartDialog(GalleryPreActivity.this, "Caon not selected file.");
				}
			}
			break;
		case R.id.incomebt:
			/*Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
		            "mailto","abc@gmail.com", null));
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, "EXTRA_SUBJECT");
			startActivity(Intent.createChooser(emailIntent, "Send email..."));
			*/	
			cutVideoMark();
			break;
		case R.id.fbbt:
			/*Uri uri = Uri.parse("https://m.facebook.com/moolamalaysia");
			 Intent intent1 = new Intent(Intent.ACTION_VIEW, uri);
			 startActivity(intent1);*/
			boolean flg = false;
			/*String s = "Check out Orange Eye by NTUC Income http://www.income.com.sg � it�s an in-car camera and more. With its accident detection and emergency call function, video footage sharing and policy renewal alerts, you may like it as much as I do!";
			 Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
			 shareIntent.setType("text/plain");
			 shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, s);
			 PackageManager pm = v.getContext().getPackageManager();
			 List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
			 for (final ResolveInfo app : activityList) {
			     if ((app.activityInfo.name).contains("facebook")) {
			    	 flg = true;
			         final ActivityInfo activity = app.activityInfo;
			         final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
			         shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
			         shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |             Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
			         shareIntent.setComponent(name);
			         v.getContext().startActivity(shareIntent);
			         break;
			    }
			 }
			 if(!flg){
				 ReturnMessage.showAlartDialog(this, "Share Failed", "The selected application is not installed in this phone.");
			 }*/
			//String imagePath1 = "file:///"+Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/OrangeEye.mp4";

			//Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/photo.php?v="+recordfilename));
			  //startActivity(browserIntent); 
			if(pos > -1){
				deleteContentVideo();
				watermarkfilecopy_reverse();
				ContentValues content = new ContentValues();
	            content.put(MediaStore.Video.VideoColumns.DATE_ADDED,
	            System.currentTimeMillis() / 1000);
	            content.put(MediaStore.Video.Media.MIME_TYPE, "video/*");
	            //String ss = Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file;
	            String ss = recordfilename;
	            content.put(MediaStore.Video.Media.DATA, ss);
	            Uri videoURI = getBaseContext().getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, content);
	            PackageManager pm = v.getContext().getPackageManager();
                Intent intent3 = new Intent(Intent.ACTION_SEND);
                intent3.setType("video/*");
				 List<ResolveInfo> activityList = pm.queryIntentActivities(intent3, 0);
				 boolean flg3 = false;
				 for (final ResolveInfo app : activityList) {
				     if ((app.activityInfo.name).contains("facebook")) {
				    	 	try {
			                    //intent.setAction(Intent.ACTION_SEND);
			                    intent3.setPackage(app.activityInfo.packageName);
			                    intent3.putExtra(Intent.EXTRA_STREAM, videoURI);
			                    startActivity(Intent.createChooser(intent3,"Upload video via:"));
				    	 		ShareData.selected_record_id = pos;
				    	 		/*Session session = Session.getActiveSession();
				    	        if (!session.isOpened() && !session.isClosed()) {
				    	            session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
				    	        } else {
				    	            Session.openActiveSession(this, true, statusCallback);
				    	        }*/
				    	 		//facebook_check();
				    	 		//facebook_upload();
				    	        
				    	 		
			                    flg3 = true; break;
			                } catch (android.content.ActivityNotFoundException ex) {
			                	flg3 = false; break;
			                }
				     }
				 }
				 //flg3 = true;
				 //if(flg3){
					 //Intent intent5 = new Intent(GalleryPreActivity.this, GalleryActivity.class);
		            	//startActivity(intent5);
		            	//finish();
				 //}
				 if(!flg3){
					 /*Intent share = new Intent(Intent.ACTION_SEND);

	                 // If you want to share a png image only, you can do:
	                 // setType("image/png"); OR for jpeg: setType("image/jpeg");
	                 share.setType("video/*");

	                 // Make sure you put example png image named myImage.png in your
	                 // directory
	                 String imagePath = "file:///"+Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file;

	                 //File imageFileToShare = new File(imagePath);

	                 Uri uri = Uri.parse(imagePath);
	                 share.putExtra(Intent.EXTRA_STREAM, uri);
	                 //share.setData(Uri.parse("https://www.facebook.com"));

	                 startActivity(Intent.createChooser(share, "Share Record video file!"));
                     */
					 /*Intent i= new Intent(Intent.ACTION_VIEW);
					 i.setType("video/*");
					 //String imagePath = "file:///"+Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file;
					 String imagePath = "file:///"+Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/OrangeEye.mp4";

                     //File imageFileToShare = new File(imagePath);

                     Uri uri = Uri.parse(imagePath);
                     i.putExtra(Intent.EXTRA_STREAM, uri);
				        i.setData(Uri.parse("https://www.facebook.com/photo.php?v="+recordfilename));
				        startActivity(i);
				     */
					 ReturnMessage.showAlartDialog(GalleryPreActivity.this, "Application not installed.");
				        
				 }else{
					 //returnScreen();
				 }
	            //Uri videoURI = Uri.fromFile(new File(filename));
	                
			}
			break;
		case R.id.youtubebt:
			/*String videoId = "Fee5vbFLYM4";
			Intent intent2 = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:"+videoId)); 
			intent2.putExtra("VIDEO_ID", videoId); 
			startActivity(intent2); 
			*/
			/*if(pos > -1){
				ContentValues content = new ContentValues(4);
				content.put(Video.VideoColumns.TITLE, "Test");
				content.put(Video.VideoColumns.DATE_ADDED,
				System.currentTimeMillis() / 1000);
				content.put(Video.Media.MIME_TYPE, "video/mp4");
				content.put(MediaStore.Video.Media.DATA, Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file);
				ContentResolver resolver = this.getContentResolver();
				Uri uri = resolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, content);
			}*/
			if(pos > -1){
				deleteContentVideo();
				watermarkfilecopy_reverse();
				File sdfile = new File(recordfilename); 
			ContentValues content = new ContentValues();
            content.put(MediaStore.Video.VideoColumns.DATE_ADDED,
            System.currentTimeMillis() / 1000);
            //content.put(MediaStore.MediaColumns.DATA, sdfile.getAbsolutePath());    
            //content.put(MediaStore.MediaColumns.TITLE, sdfile.getName());    
            //values.put(MediaStore.MediaColumns.MIME_TYPE, mimeType);  
            content.put(MediaStore.MediaColumns.SIZE, sdfile.length());   
            
            content.put(MediaStore.Video.Media.MIME_TYPE, "video/*");
            //content.put(MediaStore.Video.Media.DATA, Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file);
            content.put(MediaStore.Video.Media.DATA, recordfilename);
            Uri uri = MediaStore.Video.Media.getContentUri(sdfile.getAbsolutePath());
            Uri uri2 = MediaStore.Audio.Media.getContentUriForPath(sdfile.getAbsolutePath());
            
            
            Uri videoURI = getBaseContext().getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, content);
            /*if(videoURI == null){
            	if(getBaseContext().getContentResolver()!= null){
                	//content://media/external/audio/media
                	//getVideoIdFromFilePath
                	Uri videoUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                	Uri itemUri = ContentUris.withAppendedId(videoUri, getVideoIdFromFilePath(recordfilename, getBaseContext().getContentResolver()));
                	getContentResolver().delete(itemUri, null, null);
                	//Uri uri1 = Uri.parse("content://media/external/video/media");
                    //getBaseContext().getContentResolver().delete(uri, null, null);
                }
            	content = new ContentValues();
                content.put(MediaStore.Video.VideoColumns.DATE_ADDED,
                System.currentTimeMillis() / 1000);
                //content.put(MediaStore.MediaColumns.DATA, sdfile.getAbsolutePath());    
                //content.put(MediaStore.MediaColumns.TITLE, sdfile.getName());    
                //values.put(MediaStore.MediaColumns.MIME_TYPE, mimeType);  
                content.put(MediaStore.MediaColumns.SIZE, sdfile.length());   
                
                content.put(MediaStore.Video.Media.MIME_TYPE, "video/*");
                //content.put(MediaStore.Video.Media.DATA, Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file);
                content.put(MediaStore.Video.Media.DATA, recordfilename);
            	videoURI = getBaseContext().getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, content);
                
            }*/
            Intent intent3 = new Intent(Intent.ACTION_SEND);
            intent3.setType("video/*");
            PackageManager pm = v.getContext().getPackageManager();
            List<ResolveInfo> activityList = pm.queryIntentActivities(intent3, 0);
			 boolean flg3 = false;
			 for (final ResolveInfo app : activityList) {
			     if ((app.activityInfo.name).contains("youtube")) { 
			    	 try {
		                    //intent.setAction(Intent.ACTION_SEND);
		                    
		                    intent3.setPackage(app.activityInfo.packageName);
		                    intent3.putExtra(Intent.EXTRA_STREAM, videoURI);
		                    startActivity(Intent.createChooser(intent3,"Upload video via:"));
		                    //setResult(Activity.RESULT_OK, Intent.createChooser(intent3,"Upload video via:"));
		                    flg3 = true; break;
		            } catch (android.content.ActivityNotFoundException ex) {
		            	flg3 = false; break;
		            }
			     }
			 }
			 if(!flg3){
				 

				 /*Intent i= new Intent(Intent.ACTION_VIEW);
				 i.setType("video/*");
				 //String imagePath = "file:///"+Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file;
				 String imagePath = "file:///"+Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/OrangeEye.mp4";

                 //File imageFileToShare = new File(imagePath);

                 Uri uri1 = Uri.parse(imagePath);
                 i.putExtra(Intent.EXTRA_STREAM, uri1);
			        i.setData(Uri.parse("https://www.youtube.com"));
			        startActivity(i);
			        */
				 ReturnMessage.showAlartDialog(GalleryPreActivity.this, "Application not installed.");
			 }else{
				 //returnScreen();
				 
			 }
            //Uri videoURI = Uri.fromFile(new File(filename));
				//sharedPreferences
				//((EditText)findViewById(R.id.emailtxt)).setText(sharedPreferences.getString("youtube_email", ""));
				//((EditText)findViewById(R.id.passtxt)).setText(sharedPreferences.getString("youtube_pass", ""));
				//((EditText)findViewById(R.id.titletxt)).setText(((MyTextView)findViewById(R.id.gallery_title)).getText().toString()+" by OrangeEye.");
				//((ScrollView)findViewById(R.id.scrollView1)).setVisibility(View.VISIBLE);
		        
			}
			break;
		case R.id.uploadbt:
			final Uri data = Uri.parse(recordfilename);
	        uploadYoutube(data);
			break;
		case R.id.cancelyoutubebt:
			((ScrollView)findViewById(R.id.scrollView1)).setVisibility(View.GONE);
			break;
		case R.id.instagrambt:	
			if(pos > -1){
			ContentValues content = new ContentValues(4);
            content.put(MediaStore.Video.VideoColumns.DATE_ADDED,
            System.currentTimeMillis() / 1000);
            content.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
            //content.put(MediaStore.Video.Media.DATA, Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file);
            content.put(MediaStore.Video.Media.DATA, recordfilename);
            Uri videoURI = getBaseContext().getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, content);
            Intent intent3 = new Intent(Intent.ACTION_SEND);
            intent3.setType("video/mp4");
            PackageManager pm = v.getContext().getPackageManager();
            List<ResolveInfo> activityList = pm.queryIntentActivities(intent3, 0);
			 boolean flg3 = false;
			 for (final ResolveInfo app : activityList) {
			     if ((app.activityInfo.name).contains("instagram")) { 
		                try {
		                    intent3.setPackage("com.instagram.android");
		                    intent3.putExtra(Intent.EXTRA_STREAM, videoURI);
		                    startActivity(Intent.createChooser(intent3,"Upload video via:"));
		                    flg3 = true; break;
		                } catch (android.content.ActivityNotFoundException ex) {
		                	flg3 = false; break;
		                }
			     }
			 }
			 if(!flg3){
				 /*Intent share = new Intent(Intent.ACTION_SEND);

                 // If you want to share a png image only, you can do:
                 // setType("image/png"); OR for jpeg: setType("image/jpeg");
                 share.setType("video/mp4");

                 // Make sure you put example png image named myImage.png in your
                 // directory
                 String imagePath = Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file;

                 File imageFileToShare = new File(imagePath);

                 Uri uri = Uri.fromFile(imageFileToShare);
                 share.putExtra(Intent.EXTRA_STREAM, uri);

                 startActivity(Intent.createChooser(share, "Share Record video file!"));
                 */

				 Intent i= new Intent(Intent.ACTION_VIEW);
				 i.setType("video/*");
				 //String imagePath = "file:///"+Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file;
				 String imagePath = "file:///"+Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/OrangeEye.mp4";

                 //File imageFileToShare = new File(imagePath);

                 Uri uri = Uri.parse(imagePath);
                 i.putExtra(Intent.EXTRA_STREAM, uri);
			        i.setData(Uri.parse("http://www.instagram.com/"));
			        startActivity(i);
			 }
             //Uri videoURI = Uri.fromFile(new File(filename));
			}
			break;
		case R.id.whatsappbt:
			if(pos > -1){
				deleteContentVideo();
				watermarkfilecopy_reverse();
				ContentValues content = new ContentValues();
	            content.put(MediaStore.Video.VideoColumns.DATE_ADDED,
	            System.currentTimeMillis() / 1000);
	            content.put(MediaStore.Video.Media.MIME_TYPE, "video/*");
	            //content.put(MediaStore.Video.Media.DATA, Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file);
	            content.put(MediaStore.Video.Media.DATA, recordfilename);
	            Uri videoURI = getBaseContext().getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, content);
	             //Uri videoURI = Uri.fromFile(new File(filename));
	            Intent intent3 = new Intent(Intent.ACTION_SEND);
	            intent3.setType("video/*");
	            PackageManager pm = v.getContext().getPackageManager();
	            List<ResolveInfo> activityList = pm.queryIntentActivities(intent3, 0);
				 boolean flg3 = false;
				 for (final ResolveInfo app : activityList) {
				     if ((app.activityInfo.name).contains("whatsapp")) { 
			                try {
			                    intent3.setPackage(app.activityInfo.packageName);
			                    intent3.putExtra(Intent.EXTRA_STREAM, videoURI);
			                    startActivity(Intent.createChooser(intent3,"Upload video via:"));
			                    flg3 = true; break;
			                } catch (android.content.ActivityNotFoundException ex) {
			                	flg3 = false; break;
			                }
				     }
				 }
				 if(!flg3){
					 /*Intent share = new Intent(Intent.ACTION_SEND);

	                 // If you want to share a png image only, you can do:
	                 // setType("image/png"); OR for jpeg: setType("image/jpeg");
	                 share.setType("video/mp4");

	                 // Make sure you put example png image named myImage.png in your
	                 // directory
	                 String imagePath = Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file;

	                 File imageFileToShare = new File(imagePath);

	                 Uri uri = Uri.fromFile(imageFileToShare);
	                 share.putExtra(Intent.EXTRA_STREAM, uri);

	                 startActivity(Intent.createChooser(share, "Share Record video file!"));
	                 */

					 /*Intent i= new Intent(Intent.ACTION_VIEW);
					 i.setType("video/*");
					 //String imagePath = "file:///"+Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file;
					 String imagePath = "file:///"+Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/OrangeEye.mp4";

	                 //File imageFileToShare = new File(imagePath);

	                 Uri uri = Uri.parse(imagePath);
	                 i.putExtra(Intent.EXTRA_STREAM, uri);
				        i.setData(Uri.parse("http://www.whatsapp.com/"));
				        startActivity(i);
				     */
					 ReturnMessage.showAlartDialog(GalleryPreActivity.this, "Application not installed.");
				 }else{
					 //returnScreen();
				 }
				}
			break;
		case R.id.linebt:	
			if(pos > -1){
				ContentValues content = new ContentValues(4);
	            content.put(MediaStore.Video.VideoColumns.DATE_ADDED,
	            System.currentTimeMillis() / 1000);
	            content.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
	            //content.put(MediaStore.Video.Media.DATA, Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file);
	            content.put(MediaStore.Video.Media.DATA, recordfilename);
	            Uri videoURI = getBaseContext().getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, content);
	             //Uri videoURI = Uri.fromFile(new File(filename));
	            Intent intent3 = new Intent(Intent.ACTION_SEND);
	            intent3.setType("video/mp4");
	            PackageManager pm = v.getContext().getPackageManager();
	            List<ResolveInfo> activityList = pm.queryIntentActivities(intent3, 0);
				 boolean flg3 = false;
				 for (final ResolveInfo app : activityList) {
				     if ((app.activityInfo.name).contains("line1")) { 
			                try {
			                    intent3.putExtra(Intent.EXTRA_STREAM, videoURI);
			                    startActivity(Intent.createChooser(intent3,"Upload video via:"));
			                    flg3 = true; break;
			                } catch (android.content.ActivityNotFoundException ex) {
			                	flg3 = false; break;
			                }
				     }
				 }
				 if(!flg3){
					 /*Intent share = new Intent(Intent.ACTION_SEND);

	                 // If you want to share a png image only, you can do:
	                 // setType("image/png"); OR for jpeg: setType("image/jpeg");
	                 share.setType("video/mp4");

	                 // Make sure you put example png image named myImage.png in your
	                 // directory
	                 String imagePath = Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file;

	                 File imageFileToShare = new File(imagePath);

	                 Uri uri = Uri.fromFile(imageFileToShare);
	                 share.putExtra(Intent.EXTRA_STREAM, uri);

	                 startActivity(Intent.createChooser(share, "Share Record video file!"));
	                 */

					 Intent i= new Intent(Intent.ACTION_VIEW);
					 i.setType("video/*");
					 //String imagePath = "file:///"+Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file;
					 String imagePath = "file:///"+Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/OrangeEye.mp4";

	                 //File imageFileToShare = new File(imagePath);

	                 Uri uri = Uri.parse(imagePath);
	                 i.putExtra(Intent.EXTRA_STREAM, uri);
				        //i.setData(Uri.parse("https://www.facebook.com"));
				        startActivity(i);
				 }
				}
			break;
		
		}
	}
	private void returnScreen(){
		((MyButton)findViewById(R.id.rightbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.leftbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.backbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.sharebt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.playbt)).setOnClickListener(this);
        ((ImageView)findViewById(R.id.play1bt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.deletebt)).setOnClickListener(this);
		share_screen_layout.setVisibility(View.GONE);
	}
	private long getVideoIdFromFilePath(String filePath,
	        ContentResolver contentResolver) {


	    long videoId;
	    //Log.d(TAG,"Loading file " + filePath);

	            // This returns us content://media/external/videos/media (or something like that)
	            // I pass in "external" because that's the MediaStore's name for the external
	            // storage on my device (the other possibility is "internal")
	    Uri videosUri = MediaStore.Video.Media.getContentUri("external");

	    //Log.d(TAG,"videosUri = " + videosUri.toString());

	    String[] projection = {MediaStore.Video.VideoColumns._ID};

	    // TODO This will break if we have no matching item in the MediaStore.
	    Cursor cursor = contentResolver.query(videosUri, projection, MediaStore.Video.VideoColumns.DATA + " LIKE ?", new String[] { filePath }, null);
	    cursor.moveToFirst();

	    int columnIndex = cursor.getColumnIndex(projection[0]);
	    videoId = cursor.getLong(columnIndex);

	    //Log.d(TAG,"Video ID is " + videoId);
	    cursor.close();
	    return videoId;
	}
	public void deleteContentVideo(){
		ContentValues content1 = new ContentValues();
        content1.put(MediaStore.Video.VideoColumns.DATE_ADDED,
        System.currentTimeMillis() / 1000);
        //content.put(MediaStore.MediaColumns.DATA, sdfile.getAbsolutePath());    
        //content.put(MediaStore.MediaColumns.TITLE, sdfile.getName());    
        //values.put(MediaStore.MediaColumns.MIME_TYPE, mimeType);    
        
        content1.put(MediaStore.Video.Media.MIME_TYPE, "video/*");
        //content.put(MediaStore.Video.Media.DATA, Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file);
        content1.put(MediaStore.Video.Media.DATA, recordfilename);
        
        
        Uri videoURI1 = getBaseContext().getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, content1);
        if(videoURI1 == null){
        	if(getBaseContext().getContentResolver()!= null){
            	//content://media/external/audio/media
            	//getVideoIdFromFilePath
            	Uri videoUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            	Uri itemUri = ContentUris.withAppendedId(videoUri, getVideoIdFromFilePath(recordfilename, getBaseContext().getContentResolver()));
            	getContentResolver().delete(itemUri, null, null);
            	//Uri uri1 = Uri.parse("content://media/external/video/media");
                //getBaseContext().getContentResolver().delete(uri, null, null);
            }
        }
	}
	public void processVideo(){
		installFfmpeg();
		recordfilename = Environment.getExternalStorageDirectory() + "/OrangeEyeRecord/OrangeEye.mp4";
		tempfilename = Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file;
		File f = new File(recordfilename);
		if(f.exists()){
			f.delete();
			progressDialog = ProgressDialog.show(this, "", "Please wait while your video is being processed.", 
					true, false);
			progressDialog.setCancelable(true);
			drawMarkBitmap();
			drawVideoMark();
		}else{
			progressDialog = ProgressDialog.show(this, "", "Please wait while your video is being processed.", 
					true, false);
			progressDialog.setCancelable(true);
			drawMarkBitmap();
			drawVideoMark();
		}
	}

	private void drawMarkBitmap(){
		MediaPlayer player = MediaPlayer.create(GalleryPreActivity.this, Uri.fromFile(new File(tempfilename)));
		int msec1 = player.getDuration();
		int videowidth = player.getVideoWidth();
		int videoheight = player.getVideoHeight();
		float textsize = 15;
		player.reset();
		player.release();
		Bitmap bitmap = null;
		Date now = new Date();
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		float depth = metrics.density;
		float screenX = videowidth;
		float screenY = videoheight;
		
		float by = screenY / 5;
		float bx = screenX / 20;
		if(by > 150) by = 150;
		//by = 170 * 2.0f / depth;
		float de = 1;
		if(depth < 2.0){
			bx = screenX / 14;
			de = depth / 2.2f;
		}
		bx = 10;
		bitmap = Bitmap.createBitmap((int)screenX, (int)by,
                Bitmap.Config.ARGB_4444);	
		bitmap.eraseColor(Color.TRANSPARENT);
		Canvas c = new Canvas(bitmap);
		Bitmap logo1=BitmapFactory.decodeResource(getResources(), R.drawable.logo_watermark);
		
		Bitmap logo = Bitmap.createScaledBitmap(logo1, (int)(logo1.getWidth() * (int)((int)by/3) /  logo1.getHeight()), (int)(by/3), true);
		float logoX=logo.getWidth();
		float logoY=logo.getHeight();
		float interval = by / 5;
		Paint p = new Paint();
		p.setColor(getResources().getColor(R.color.trans_black));
		c.drawRect(0, 0, screenX, by, p);
		int tsize = (int)((textsize + 3.0f) * de);
		p.setColor(Color.WHITE);
		p.setTextAlign(Align.LEFT);
		Typeface tf = Typeface.createFromAsset(getAssets(),
		        "fonts/HelveticaNeueLTStd-MdCn.ttf");
		p.setTypeface(tf);
		p.setTextSize(tsize);
		c.drawText("Recorded on", bx, interval * 3, p);
		c.drawText("Average speed", bx, interval, p);
		tsize = (int)((textsize + 5.0f) * de);
		p.setTextSize(tsize);
		
		String dd = recordlist.get(pos).record_time;
		int idx = dd.indexOf(":");		
		String datestr = dd.substring(idx-2,idx+3 ) + dd.substring(dd.length()-3,dd.length() ) + " " + dd.substring(0, idx-3);
		Rect bounds6 = new Rect();
		p.getTextBounds(datestr, 0, datestr.length(), bounds6);
		c.drawText(datestr, bx, interval * 4, p);
		
		//datestr = dd.substring(0, idx-3);
		
				
		//tsize = (int)(28.0f * de);
		//p.setTextSize(tsize);
		
		//c.drawText(datestr, bx, screenY - by + 90, p);

		tsize = (int)((textsize + 5.0f) * de);
		p.setTextSize(tsize);
		c.drawText(recordlist.get(pos).record_speed +" km/h", bx , interval * 2, p);

		tsize = (int)(textsize * de);
		p.setTextSize(tsize);
		Rect bounds = new Rect();
		p.getTextBounds("From", 0, "From".length(), bounds);
		c.drawText("From", bx + bounds6.width() + 30 , interval, p);

		tsize = (int)((textsize + 3.0f) * de);
		p.setTextSize(tsize);
		Rect bounds1 = new Rect();
		/*SearchAddr addr = ShareData.des_addr;
		String addrname = "";
		if(addr != null){
			addrname = addr.addressname;
			if(addrname.length() > 20){
				addrname = addrname.substring(0,20) + "...";
			}
		}*/
		String addrname = recordlist.get(pos).record_start;
		p.getTextBounds(addrname, 0, addrname.length(), bounds1);
		c.drawText(addrname, bx + bounds6.width() + 30, interval * 2, p);
		
		tsize = (int)(textsize * de);
		p.setTextSize(tsize);
		Rect bounds2 = new Rect();
		p.getTextBounds("To", 0, "To".length(), bounds2);
		c.drawText("To", bx + bounds6.width() + 30 , interval * 3, p);

		tsize = (int)((textsize + 3.0f) * de);
		p.setTextSize(tsize);
		Rect bounds3 = new Rect();
		/*SearchAddr addr = ShareData.des_addr;
		String addrname = "";
		if(addr != null){
			addrname = addr.addressname;
			if(addrname.length() > 20){
				addrname = addrname.substring(0,20) + "...";
			}
		}*/
		addrname = recordlist.get(pos).record_destination;
		p.getTextBounds(addrname, 0, addrname.length(), bounds3);
		c.drawText(addrname, bx + bounds6.width() + 30 , interval * 4, p);
		
		c.drawBitmap(logo, screenX - logoX - 10, (by - logoY) / 2, null);
		
		try {
			File markfile = new File(Environment.getExternalStorageDirectory() + "/OrangeEyeRecord/mark.png");
			//File f = new File(markfile);
			markfile.createNewFile();

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			bitmap.compress(CompressFormat.PNG, 0 , bos);
			byte[] bitmapdata = bos.toByteArray();

			//write the bytes in file
			FileOutputStream fos;
			fos = new FileOutputStream(markfile);
			fos.write(bitmapdata);
			fos.close();
			bos.close();

			//File markfile = new File(getCacheDir(), "logo_small.png");
			mMarkInstallPath = markfile.toString();
			//Log.d(TAG, "mark install path: " + mMarkInstallPath);
			
			if (!markfile.exists()) {
				/*try {
					markfile.createNewFile();
				} catch (IOException e) {
					//Log.e(TAG, "Failed to create new file!", e);
				}
				Utils.installBinaryFromAssets(RecordActivity.this, R.raw.logo_small, markfile);
				*/
				mMarkInstallPath = "";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void drawVideoMark(){
		//loadJob(job);		
		//MediaPlayer mediaPlayer = new MediaPlayer();
		//String filePath = Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/record.mp4";
		/*try {
			mediaPlayer.setDataSource(filePath);
		} catch (IllegalArgumentException e) {
			finish();
		} catch (IllegalStateException e) {
			finish();
		} catch (IOException e) {
			finish();
		}*/
		int msec = MediaPlayer.create(GalleryPreActivity.this, Uri.fromFile(new File(tempfilename))).getDuration();
		StringBuffer buf = new StringBuffer();
		long millis = msec;
	    int hours = (int)(millis / (1000*60*60));
	    int minutes = (int)(( millis % (1000*60*60) ) / (1000*60));
	    int seconds = (int)(( ( millis % (1000*60*60) ) % (1000*60) ) / 1000);
	    boolean ff = false;
	    if(msec - 30000 > 0){
	    		ff = true;
	    }
	    if(!ff){  	
			
	    	String endtime = hours+":"+minutes+":"+seconds;
		    final FfmpegJob job = new FfmpegJob(mFfmpegInstallPath, mMarkInstallPath, recordfilename, tempfilename, "",endtime, "");
			
		    //final ProgressDialog progressDialog = ProgressDialog.show(this, "", "Processing Video...", 
					//true, false);
			
			new AsyncTask<Void, Void, Void>() {

				@Override
				protected Void doInBackground(Void... arg0) {
					job.create().run();
					return null;
				}
				
				@Override
				protected void onPostExecute(Void result) {
					if(progressDialog != null)
						progressDialog.dismiss();
					File fc = new File(mMarkInstallPath);
					if(fc.exists()){
						fc.delete();
					}
					watermarkfilecopy();
					//record_data_save_db(realrecordfilepath);
					//sendToActivity();
					//Toast.makeText(RecordActivity.this, "Video Edit .", Toast.LENGTH_SHORT).show();
				}
				
			}.execute();
	    }else{
	    	String endtime = hours+":"+minutes+":"+seconds;
			millis = msec - 60000;
		    hours = (int)(millis / (1000*60*60));
		    minutes = (int)(( millis % (1000*60*60) ) / (1000*60));
		    seconds = (int)(( ( millis % (1000*60*60) ) % (1000*60) ) / 1000);
	    	String starttime = hours+":"+minutes+":"+seconds;
	    	
		    //final FfmpegJob job = new FfmpegJob(mFfmpegInstallPath, mMarkInstallPath, recordfilename, tempfilename, starttime, endtime);
	    	final FfmpegJob job = new FfmpegJob(mFfmpegInstallPath, mMarkInstallPath, recordfilename, tempfilename, "", endtime, "");//starttime
			
		    //final ProgressDialog progressDialog = ProgressDialog.show(this, "", "Processing Video...", 
					//true, false);
			
			new AsyncTask<Void, Void, Void>() {
				@Override
				protected Void doInBackground(Void... arg0) {
					job.create().run();
					return null;
				}
				
				@Override
				protected void onPostExecute(Void result) {
					if(progressDialog != null)
						progressDialog.dismiss();
					File fc = new File(mMarkInstallPath);
					if(fc.exists()){
						fc.delete();
					}
					watermarkfilecopy();
					//record_data_save_db(realrecordfilepath);
					//sendToActivity();
					//Toast.makeText(RecordActivity.this, "Video Edit .", Toast.LENGTH_SHORT).show();
				}
				
			}.execute();
	    }
	}
	private void watermarkfilecopy(){
		File f = new File(recordfilename);
		File newfile = new File(Environment.getExternalStorageDirectory() + "/OrangeEyeRecord/watermark.mp4");
		if(f.exists()){
			try{
				byte[] buf = new byte[1024];
				int len = 0;
					if(newfile.exists()){
						newfile.delete();
					}
					InputStream in = new FileInputStream(f);
					OutputStream out = new FileOutputStream(newfile);
					while((len = in.read(buf)) > 0){
						out.write(buf, 0, len);
					}
					in.close();
					out.close();
			}catch(Exception e){
				
			}
		}
	}
	private void watermarkfilecopy_reverse(){
		File f = new File(recordfilename);
		File newfile = new File(Environment.getExternalStorageDirectory() + "/OrangeEyeRecord/watermark.mp4");
		if(newfile.exists()){
			try{
				byte[] buf = new byte[1024];
				int len = 0;
				if(f.exists()){
					f.delete();
				}
					InputStream in = new FileInputStream(newfile);
					OutputStream out = new FileOutputStream(f);
					while((len = in.read(buf)) > 0){
						out.write(buf, 0, len);
					}
					in.close();
					out.close();
			}catch(Exception e){
				
			}
		}
	}
	private void cutVideoMark(){
		//loadJob(job);		
		//MediaPlayer mediaPlayer = new MediaPlayer();
		//String filePath = Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/record.mp4";
		/*try {
			mediaPlayer.setDataSource(filePath);
		} catch (IllegalArgumentException e) {
			finish();
		} catch (IllegalStateException e) {
			finish();
		} catch (IOException e) {
			finish();
		}*/
		progressDialog = ProgressDialog.show(this, "", "Please wait while your video is being trimmed.", 
				true, false);
		progressDialog.setCancelable(true);
		int msec = 0;
		File filenew = new File(recordfilename);
	    int file_size = Integer.parseInt(String.valueOf(filenew.length()/1024));
	    int cutsize = (int)(8.5f * 1020f);
	    boolean ff = false;
	    float pro = 0;
	    if(cutsize > file_size){
	    	ff = false;
	    }else{
	    	pro = (float)cutsize / (float)file_size;
	    	ff = true;
	    }
		try{
			MediaPlayer player = MediaPlayer.create(GalleryPreActivity.this, Uri.fromFile(new File(recordfilename)));
			msec = player.getDuration();
			player.release();
			player = null;
		}catch(Exception e){
			try{
				MediaPlayer player = MediaPlayer.create(GalleryPreActivity.this, Uri.fromFile(new File(recordfilename)));
				msec = player.getDuration();
				player.release();
				player = null;
			}catch(Exception e1){
				
			}
		}
		StringBuffer buf = new StringBuffer();
		long millis = msec;
	    int hours = (int)(millis / (1000*60*60));
	    int minutes = (int)(( millis % (1000*60*60) ) / (1000*60));
	    int seconds = (int)(( ( millis % (1000*60*60) ) % (1000*60) ) / 1000);
	    
	    //if(msec - 60000 > 0){
	    		//ff = true;
	    //}
	    cutfilename = Environment.getExternalStorageDirectory() + "/OrangeEyeRecord/OrangeEye_NTUC.mp4";
	    File cutf = new File(cutfilename);
	    if(cutf.exists()){
	    	cutf.delete();
	    }
	    if(!ff){  	
			
	    	String endtime = hours+":"+minutes+":"+seconds;
		    //final FfmpegJob job = new FfmpegJob(mFfmpegInstallPath, "", cutfilename, recordfilename, "00:00:00",endtime, "");
			
		    //final ProgressDialog progressDialog = ProgressDialog.show(this, "", "Processing Video...", 
					//true, false);
		    try{
		    	File fc = new File(cutfilename);
				byte[] buf1 = new byte[1024];
				int len = 0;
					if(fc.exists()){
						fc.delete();
					}
					InputStream in = new FileInputStream(recordfilename);
					OutputStream out = new FileOutputStream(cutfilename);
					while((len = in.read(buf1)) > 0){
						out.write(buf1, 0, len);
					}
					in.close();
					out.close();
			}catch(Exception e){
				
			}
			incomeSend();
			if(progressDialog != null)
				progressDialog.dismiss();
			/*new AsyncTask<Void, Void, Void>() {

				@Override
				protected Void doInBackground(Void... arg0) {
					job.create().run();
					return null;
				}
				
				@Override
				protected void onPostExecute(Void result) {
					if(progressDialog != null)
						progressDialog.dismiss();
					//File fc = new File(mMarkInstallPath);
					//if(fc.exists()){
						//fc.delete();
					//}
					//record_data_save_db(realrecordfilepath);
					//sendToActivity();
					//Toast.makeText(RecordActivity.this, "Video Edit .", Toast.LENGTH_SHORT).show();
				}
				
			}.execute();
			*/
	    }else{
	    	String endtime = hours+":"+minutes+":"+seconds;
	    	int mean = minutes;
	    	int sec = seconds;
	    	float f = (float)msec * (1f - pro);
			millis = (int)f;
		    hours = (int)(millis / (1000*60*60));
		    minutes = (int)(( millis % (1000*60*60) ) / (1000*60));
		    seconds = (int)(( ( millis % (1000*60*60) ) % (1000*60) ) / 1000);
	    	String starttime = hours+":"+minutes+":"+seconds;
	    	if(mean > 0){
	    		int mc = mean - 1;
	    		starttime = hours+":"+mc+":" + sec;
	    	}
	    	
		    //final FfmpegJob job = new FfmpegJob(mFfmpegInstallPath, mMarkInstallPath, recordfilename, tempfilename, starttime, endtime);
	    	final FfmpegJob job = new FfmpegJob(mFfmpegInstallPath, "", cutfilename, recordfilename, starttime, endtime, "");//
			
		    //final ProgressDialog progressDialog = ProgressDialog.show(this, "", "Processing Video...", 
					//true, false);
			
			new AsyncTask<Void, Void, Void>() {
				@Override
				protected Void doInBackground(Void... arg0) {
					job.create().run();
					return null;
				}
				
				@Override
				protected void onPostExecute(Void result) {
					if(progressDialog != null)
						progressDialog.dismiss();
					incomeSend();
					//File fc = new File(mMarkInstallPath);
					//if(fc.exists()){
						//fc.delete();
					//}
					//record_data_save_db(realrecordfilepath);
					//sendToActivity();
					//Toast.makeText(RecordActivity.this, "Video Edit .", Toast.LENGTH_SHORT).show();
				}
				
			}.execute();
	    }
	}
	private void incomeSend(){
		if(pos > -1){		
			ArrayList<String> file1 = new ArrayList<String>();
			String s1 = "Dear NTUC Income, \n I would like to submit the attached footage for [indicate purpose] and here are my contact details: \n 1.       Name of Policyholder - \n 2.       Vehicle number registered under the policy - \n 3.       Contact number - \n 4.       Email Address -	";
			//file1.add(recordlist.get(pos).record_file);
			file1.add("OrangeEye_NTUC.mp4");
			try {
				sendEmailMulipleFiles(this,"motorvideo@income.com.sg", "Submission of Orange Eye Video", s1, file1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				 ReturnMessage.showAlartDialog(GalleryPreActivity.this, "Can not selected file.");
			}
		}
	}
	private void installFfmpeg() {
		File ffmpegFile = new File(getCacheDir(), "ffmpeg");
		mFfmpegInstallPath = ffmpegFile.toString();
		//Log.d(TAG, "ffmpeg install path: " + mFfmpegInstallPath);
		
		if (!ffmpegFile.exists()) {
			try {
				ffmpegFile.createNewFile();
			} catch (IOException e) {
				//Log.e(TAG, "Failed to create new file!", e);
			}
			Utils.installBinaryFromRaw(GalleryPreActivity.this, R.raw.ffmpeg, ffmpegFile);
		}
		
		ffmpegFile.setExecutable(true);

		
	}
	public static void sendEmailMulipleFiles(Context context, String toAddress, String subject, String body, ArrayList<String> attachmentPath) throws Exception {
	    try {
	        Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
	        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { toAddress });
	        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
	        intent.putExtra(Intent.EXTRA_TEXT, body);
	        intent.setType("message/rfc822");
	        PackageManager pm = context.getPackageManager();
	        List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
	        ResolveInfo best = null;
	        for (final ResolveInfo info : matches) {
	            if (info.activityInfo.packageName.contains(".gm.") || info.activityInfo.name.toLowerCase().contains("gmail"))
	                best = info;
	        }
	        ArrayList<Uri> uri = new ArrayList<Uri>();
	        for (int i = 0; i < attachmentPath.size(); i++) {
	            File file = new File(Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" +attachmentPath.get(i));
	            uri.add(Uri.fromFile(file));
	        }

	        intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uri);


	        if (best != null)
	            intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);

	        context.startActivity(Intent.createChooser(intent, "Choose an email application..."));
	    } catch (Exception ex) {
	        ex.printStackTrace();	       
	        throw ex;
	    }
	}

	public static void sendEmailMulipleFiles1(Context context, String toAddress, String subject, String body, ArrayList<String> attachmentPath) throws Exception {
	    try {
	        Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
	        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { toAddress });
	        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
	        intent.putExtra(Intent.EXTRA_TEXT, body);
	        intent.setType("message/rfc822");
	        PackageManager pm = context.getPackageManager();
	        List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
	        ResolveInfo best = null;
	        for (final ResolveInfo info : matches) {
	            if (info.activityInfo.packageName.contains(".gm.") || info.activityInfo.name.toLowerCase().contains("gmail"))
	                best = info;
	        }
	        ArrayList<Uri> uri = new ArrayList<Uri>();
	        for (int i = 0; i < attachmentPath.size(); i++) {
	            File file = new File(Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" +attachmentPath.get(i));
	            uri.add(Uri.fromFile(file));
	        }

	        intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uri);


	        if (best != null)
	            intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);

	        context.startActivity(Intent.createChooser(intent, "Choose an email application..."));
	    } catch (Exception ex) {
	        ex.printStackTrace();	       
	        throw ex;
	    }
	}
	public void delete_func(){
		try{
			if(pos > -1){
				confirmDialog(this, "", "Do you want to delete this video?");
			}else{
				Toast.makeText(this, "Can not this video.", 500);
			}
		}catch(Exception e){
			
		}
	}
	public void deleteRecord(){
		DBAdapter.deleteRecordData(recordlist.get(pos));
		if(isSDPresent){
			File delfile = new File(Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" + recordlist.get(pos).record_file);
			
			if(delfile.exists()){
				delfile.delete();
			}
			if(recordfilename != null && !recordfilename.equals("")){
				File delfile1 = new File(recordfilename);
				if(delfile1.exists()){
					delfile1.delete();
				}
			}
		}
		recordlist.remove(pos);
		if(recordlist.size() == 0){
			if(isSDPresent){
				File delfile = new File(Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/record.mp4");
				if(delfile.exists()){
					delfile.delete();
				}
				File delfile1 = new File(Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/record_new.mp4");
				if(delfile1.exists()){
					delfile1.delete();
				}
			}
		}
		if(pos > recordlist.size() - 1){
			pos = recordlist.size() - 1;
		}
		if(pos < 0){
			pos = 0;
		}
		
		displayRecordData(pos);
	}
	 @Override
	 public void onBackPressed() {
		 ShareData.selected_record_id = 0;
		Intent intent3 = new Intent(GalleryPreActivity.this, HomeActivity.class);
      	startActivity(intent3);
		finish();
	 }
	 public void confirmDialog(Context context, String title, String content){
			new AlertDialog.Builder(context)
			.setTitle(title)
			.setMessage(
					content)
			.setPositiveButton("YES",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							deleteRecord();							
						}
					})
			.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog4, int which) {
					// TODO Auto-generated method stub
					dialog4.cancel();
				}
			}).show();
	 }
	 public void facebook_upload(String dataPath){
		 byte[] data = null;
		 String dataMsg = "Recording video file upload.";
		 Bundle param;
		 Facebook facebook = new Facebook("777673488923807");
		 AsyncFacebookRunner mAsyncRunner = new AsyncFacebookRunner(facebook);
		 InputStream is = null;
		 try {
		     is = new FileInputStream(dataPath);
		     data = readBytes(is);
		     param = new Bundle();
		     param.putString("message", dataMsg);
		     param.putByteArray("video", data);
		     mAsyncRunner.request("me/videos", param, "POST", null, null);
		 }
		 catch (FileNotFoundException e) {
		    e.printStackTrace();
		 }
		 catch (IOException e) {
		    e.printStackTrace();
		 }
	 }
	 public byte[] readBytes(InputStream inputStream) throws IOException {
		    // This dynamically extends to take the bytes you read.
		    ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

		    // This is storage overwritten on each iteration with bytes.
		    int bufferSize = 1024;
		    byte[] buffer = new byte[bufferSize];

		    // We need to know how may bytes were read to write them to the byteBuffer.
		    int len = 0;
		    while ((len = inputStream.read(buffer)) != -1) {
		        byteBuffer.write(buffer, 0, len);
		    }

		    // And then we can return your byte array.
		    return byteBuffer.toByteArray();
		}
	 public void facebook_video_upload(String path){
		 //String path;
	        //get the current active facebook session
	        Session session = Session.getActiveSession();
	        //If the session is open
	        //if(session.isOpened()) {
	            //Get the list of permissions associated with the session
	            List<String> permissions = session.getPermissions();
	            //if the session does not have video_upload permission
	            if(!permissions.contains("video_upload")) {
	                //Get the permission from user to upload the video to facebook
	                Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(GalleryPreActivity.this, Arrays.asList("video_upload"));
	                session.requestNewReadPermissions(newPermissionsRequest);
	            }


	            //Create a new file for the video 
	            File file = new File(path);
	            try {
	                //create a new request to upload video to the facebook
	                Request videoRequest = Request.newUploadVideoRequest(session, file, new Request.Callback() {

	                    @Override
	                    public void onCompleted(Response response) {

	                        if(response.getError()==null)
	                        {
	                            Toast.makeText(GalleryPreActivity.this, "video shared successfully", Toast.LENGTH_SHORT).show();
	                        }
	                        else
	                        {
	                            Toast.makeText(GalleryPreActivity.this, response.getError().getErrorMessage(), Toast.LENGTH_SHORT).show();
	                        }
	                    }
	                });

	                //Execute the request in a separate thread
	                videoRequest.executeAsync();

	            } catch (FileNotFoundException e) {
	                e.printStackTrace();
	            }
	       // }

	        //Session is not open
	        //else {
	            //Toast.makeText(getApplicationContext(), "Please login to facebook first", Toast.LENGTH_SHORT).show();
	       // }
	 }
	 public void video_upload1(){
		 
	 }
	 public void video_upload(){
		 File tempFile;
		 try {
		     tempFile = createTempFileFromAsset(GalleryPreActivity.this, recordfilename);
		     Request request5 = Request.newUploadVideoRequest(session,
		                     tempFile, callback5);
		     RequestAsyncTask task5 = new RequestAsyncTask(request5);
		     task5.execute();
		 } catch (IOException e) {
		     e.printStackTrace();
		 }
	 }
	 Request.Callback callback5 = new Request.Callback() {
		    public void onCompleted(Response response) {    
		        // response will have an id if successful
		    	String s = "get";
		    }
	};
	public static File createTempFileFromAsset(Context context, String assetPath) {
		    InputStream inputStream = null;
		    FileOutputStream outStream = null;

		    try {
		        //AssetManager assets = context.getResources().getAssets();
		        inputStream = new FileInputStream(assetPath);
		    	File outputFile = new File(context.getCacheDir(), "OrangeEye.mp4");
		        //File sendfile = new File(assetPath);
		        File outputDir = context.getCacheDir(); // context being the
		                                                // Activity pointer
		        //File outputFile = File.createTempFile("prefix", assetPath,
		       //         outputDir);
		        outStream = new FileOutputStream(outputFile);

		        final int bufferSize = 1024 * 2;
		        byte[] buffer = new byte[bufferSize];
		        int n = 0;
		        while ((n = inputStream.read(buffer)) != -1) {
		            outStream.write(buffer, 0, n);
		        }

		        return outputFile;
		    }catch(Exception e){
		    	e.fillInStackTrace();
		    	return null;
		    } finally {
		        //Utility.closeQuietly(outStream);
		       // Utility.closeQuietly(inputStream);
		    }
		}
		private void onSessionStateChange(Session session, SessionState state, Exception exception) {
			if (session != null){
				if(state.isOpened()) {
				
					makeMeRequest(session);
				}
	    	}
	    }
		private void makeMeRequest(final Session session) {
			
	        Request request = Request.newMeRequest(session, new MyGrpahUserCallBack1(GalleryPreActivity.this, session,true));
	        request.executeAsync();
	    }
		class MyGrpahUserCallBack1 implements Request.GraphUserCallback
		{
			GalleryPreActivity mParentActivity;
			//Session session;
			boolean isNeedFBRegister=false;
			public MyGrpahUserCallBack1(GalleryPreActivity context, Session session,boolean isNeedFBRegister){
				mParentActivity = context;
				mParentActivity.session = session;
			}
			
			@Override
		    public void onCompleted(GraphUser user, Response response) {
				
				
		        if (session == Session.getActiveSession()) {
		            if (user != null) {
		            	mParentActivity.mFBUserInfo = user;
		            	facebook_video_upload(recordfilename);
		            }
		        }
		        
		        if (response.getError() != null) {
		           
		        }
		    }
			
		}
	public void facebook_check(){
		/*ShareData.selected_record_id = pos;
		Session session = new Session(this);
		List<String> permissions = new ArrayList<String>();
		Session.OpenRequest request = new Session.OpenRequest(this);
		//request.setCallback(callback);
		//session.openForRead(new Session.OpenRequest(this).setPermissions(Arrays.asList("email")));
		Session.setActiveSession(session);
		session.openForRead(request);
		*/
		Session session = new Session(GalleryPreActivity.this);
		List<String> permissions = new ArrayList<String>();
		permissions.add("email");
		Session.OpenRequest request = new Session.OpenRequest(GalleryPreActivity.this).setPermissions(permissions);
		request.setCallback(callback);
		//session.openForRead(new Session.OpenRequest(this).setPermissions(Arrays.asList("email")));
		Session.setActiveSession(session);
		session.openForRead(request);
	}
	public void facebook_upload(){
		//ByteArrayOutputStream stream = new ByteArrayOutputStream();
		//bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream); //compress to which format you want.
		
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httpost = new HttpPost("http://graph.facebook.com/me/videos");
		HttpContext localContext = new BasicHttpContext();
		MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		File file=new File(recordfilename);
		byte[] data = null;
		try {
			data = FileUtils.readFileToByteArray(file);
			entity.addPart("source", new ByteArrayBody(data, "OrangeEye.mp4"));
			entity.addPart("title", new StringBody("Record Video"));
			entity.addPart("Description", new StringBody("Uploaded from Orange Eye App"));
			httpost.setEntity(entity);
			HttpResponse response;
			 response = httpclient.execute(httpost,localContext);
			String s = convertResponseToString(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
	public String convertResponseToString(HttpResponse response) throws IllegalStateException, IOException{
		 
        String res = "";
        InputStream inputStream;
        StringBuffer buffer = new StringBuffer();
        inputStream = response.getEntity().getContent();
        int contentLength = (int) response.getEntity().getContentLength(); //getting content length�..
        
     
        if (contentLength < 0){
        }
        else{
               byte[] data = new byte[512];
               int len = 0;
               try
               {
                   while (-1 != (len = inputStream.read(data)) )
                   {
                       buffer.append(new String(data, 0, len)); //converting to string and appending  to stringbuffer�..
                   }
               }
               catch (IOException e)
               {
                   e.printStackTrace();
               }
               try
               {
                   inputStream.close(); // closing the stream�..
               }
               catch (IOException e)
               {
                   e.printStackTrace();
               }
               res = buffer.toString();     // converting stringbuffer to string�..

               
               //System.out.println("Response => " +  EntityUtils.toString(response.getEntity()));
        }
        return res;
	}
	private void uploadYoutube(final Uri data) {
		progressBar.setVisibility(View.VISIBLE);
        new AsyncTask<Void, Integer, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                YoutubeUploadRequest request = new YoutubeUploadRequest();
                request.setUri(data);
                //request.setCategory(category);
                //request.setTags(tags);
                editor.putString("youtube_email", ((EditText)findViewById(R.id.emailtxt)).getText().toString());
                editor.putString("youtube_pass", ((EditText)findViewById(R.id.passtxt)).getText().toString());
				editor.commit();
                request.setTitle(((EditText)findViewById(R.id.titletxt)).getText().toString());
                request.setDescription(((EditText)findViewById(R.id.desctxt)).getText().toString());
                request.setEmail(((EditText)findViewById(R.id.emailtxt)).getText().toString());
                request.setPass(((EditText)findViewById(R.id.passtxt)).getText().toString());

                YoutubeUploader.upload(request, new ProgressListner() {

                    @Override
                    public void onUploadProgressUpdate(int progress) {

                        publishProgress(progress);
                    }
                }, GalleryPreActivity.this);
                return null;
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                progressBar.setProgress(values[0]);

                if(values[0] == 100){
                	progressBar.setVisibility(View.GONE);
                	((ScrollView)findViewById(R.id.scrollView1)).setVisibility(View.GONE);
                	Toast.makeText(GalleryPreActivity.this, "Successfully uploaded.", 500);
                }
            };
        }.execute();
    }
}
