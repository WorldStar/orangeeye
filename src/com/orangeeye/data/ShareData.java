package com.orangeeye.data;

import android.app.Activity;

import com.google.android.gms.maps.model.LatLng;
import com.orangeeye.db.RecordData;

public class ShareData {

	public static LatLng destination = null;
	public static SearchAddr des_addr = null;
	public static SearchAddr start_addr = null;
	public static long video_record_length = 0;
	public static String emergency_num = "";
	public static boolean auto_120min_flg = false;
	public static RecordData selected_record = new RecordData();
	public static int selected_record_id = -1;
	public static String country = "MY";
	public static LatLng desti_pos = null;
	//hoe screen count
	public static Activity homeActive = null;
}
