package com.orangeeye.data;

import com.google.android.gms.maps.model.LatLng;

public class SearchAddr {
	public String addressname = "";
	public Double lat = 0.0;
	public Double lng = 0.0;
	public String dis = "";
	public String speed = "";
}
