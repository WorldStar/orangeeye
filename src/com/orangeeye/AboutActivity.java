package com.orangeeye;

import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;

public class AboutActivity extends Activity implements OnClickListener{
	Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_screen);
        ((MyButton)findViewById(R.id.backbt)).setOnClickListener(this);
        ((MyTextView)findViewById(R.id.aboutclick)).setOnClickListener(this);
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.backbt:
			intent = new Intent(AboutActivity.this, SettingsActivity.class);
        	startActivity(intent);
        	finish();
			break;
		case R.id.aboutclick:
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.income.com.sg"));
			  startActivity(browserIntent); 
        	finish();
			break;
		}
	}

	 @Override
	 public void onBackPressed() {
		Intent intent3 = new Intent(AboutActivity.this, SettingsActivity.class);
		startActivity(intent3);
		finish();
	 }
    
}
