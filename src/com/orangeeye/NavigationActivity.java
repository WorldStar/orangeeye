package com.orangeeye;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.maps.GeoPoint;
import com.orangeeye.data.SearchAddr;
import com.orangeeye.data.ShareData;
import com.orangeeye.data.busStopCodeSet;
import com.orangeeye.db.RecordData;
import com.orangeeye.lib.AppSettings;
import com.orangeeye.lib.CheckInternetAccess;
import com.orangeeye.lib.Constants;
import com.orangeeye.lib.DirectionsJSONParser;
import com.orangeeye.lib.GPSCallback;
import com.orangeeye.lib.GPSManager;
import com.orangeeye.lib.GetSearchLocation;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;
import com.orangeeye.utils.ReturnMessage;
import com.orangeeye.utils.XMLParserHelper;

import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.style.AbsoluteSizeSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class NavigationActivity extends FragmentActivity implements GPSCallback, OnClickListener, LocationListener, OnItemClickListener{
	GoogleMap mGoogleMap;
	ArrayList<LatLng> mMarkerPoints;
	double mLatitude=0;
    double mLongitude=0;
    LatLng point2;// = new LatLng(3.030469,101.708626);//3.127554,101.643402);
    LatLng point1;
    LatLng point;
    String searchaddress = "";
    LinearLayout search_list_layout;
    ListView searchlist;
    ListView park_list;
	LayoutInflater vi;
	ArrayList<SearchAddr> searchaddr = new ArrayList<SearchAddr>();
	List<busStopCodeSet> parklist = new ArrayList<busStopCodeSet>();
	busStopCodeSet setitem;
	EditText searchtx;
	String search_data = "";
	private GPSManager gpsManager = null;
    private int measurement_index = Constants.INDEX_KM;
    private AbsoluteSizeSpan sizeSpanLarge = null;
    private AbsoluteSizeSpan sizeSpanSmall = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_screen);
        RecordData data = ShareData.selected_record;        
        String s_lat = data.record_s_lat;
        String s_lng = data.record_s_lng;
        String e_lat = data.record_e_lat;
        String e_lng = data.record_e_lng;
        
        if(s_lat != null && !s_lat.equals("")){
        	if(s_lng != null && !s_lng.equals("")){
        		point1 = new LatLng(Double.parseDouble(s_lat), Double.parseDouble(s_lng));
        	}
        }
        if(e_lat != null && !e_lat.equals("")){
        	if(e_lng != null && !e_lng.equals("")){
        		point2 = new LatLng(Double.parseDouble(e_lat), Double.parseDouble(e_lng));
        	}
        }
        //int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        ((MyButton)findViewById(R.id.backbt)).setOnClickListener(this);
        /*((MyButton)findViewById(R.id.back1bt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.back2bt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.back3bt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.carparkbt)).setOnClickListener(this);
        ((ImageView)findViewById(R.id.clearbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.startrecordbt)).setOnClickListener(this);
        ((LinearLayout)findViewById(R.id.destination_park_layout)).setOnClickListener(this);
        ((LinearLayout)findViewById(R.id.destination_park_layout)).setSelected(true);
        ((LinearLayout)findViewById(R.id.current_park_layout)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.yes2bt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.no2bt)).setOnClickListener(this);
    	vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        search_list_layout = (LinearLayout)findViewById(R.id.search_list_layout);
        searchlist = (ListView)findViewById(R.id.serchlist);
        park_list = (ListView)findViewById(R.id.parkinglist);
        searchtx = (EditText)findViewById(R.id.searchbox);
        */
        /*searchtx.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                 if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                        actionId == EditorInfo.IME_ACTION_DONE ||
                        actionId == EditorInfo.IME_ACTION_GO ||
                        event.getAction() == KeyEvent.ACTION_DOWN &&
                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

                    // hide virtual keyboard
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(searchtx.getWindowToken(), 0);
                    
                    new SearchClicked().execute();
                    //searchtx.setText("", TextView.BufferType.EDITABLE);
                    return true;
                }
                return false;
            }
        });*/
        //searchtx.setTypeface(Typeface.createFromAsset(getAssets(), this.getResources().getString(R.string.font_string)));
        /*searchtx.addTextChangedListener(new TextWatcher(){

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				//getAddressList(s.toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
			}
        	
        });*/
        /*searchtx.setOnKeyListener(new View.OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)){
					search_data = searchtx.getText().toString();
					getAddressList(searchtx.getText().toString());
					//new SearchClicked().execute();
					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        			imm.hideSoftInputFromWindow(searchtx.getWindowToken(), 0);
					return true;
				}
				return false;
			}
		});*/
        
        
    }
    public class SearchClicked extends AsyncTask<Void, Void, Boolean> {
       
        private Address address;

        public SearchClicked() {
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            try {
                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.UK);
                String toSearch = search_data;
                List<Address> results = geocoder.getFromLocationName(toSearch, 1);

                if (results.size() == 0) {
                    return false;
                }

                address = results.get(0);

                  // Now do something with this GeoPoint:
                GeoPoint p = new GeoPoint((int) (address.getLatitude() * 1E6), (int) (address.getLongitude() * 1E6));

            } catch (Exception e) {
                Log.e("", "Something went wrong: ", e);
                return false;
            }
            return true;
        }
    }
    
    public void getDirectionForMaps(){
    	try{
	    	mGoogleMap.clear();
	    	mMarkerPoints.clear();
	    	if(point1 != null){
	    		drawMarker(point1);
				mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point1,12.0f));
	    	}
	    	if(point2 != null)
	    		drawMarker(point2);
			// Getting URL to the Google Directions API
	    	if(point1 != null && point2 != null){
				String url = getDirectionsUrl(point1, point2);				
				
				DownloadTask downloadTask = new DownloadTask();
				
				// Start downloading json data from Google Directions API
				downloadTask.execute(url);
	    	}
			// Setting onclick event listener for the map
			//mGoogleMap.setTrafficEnabled(true);
			mGoogleMap.setOnMapClickListener(new OnMapClickListener() {
				
				@Override
				public void onMapClick(LatLng point) {
					
					// Already map contain destination location	
					/*if(mMarkerPoints.size()>1){
						
						FragmentManager fm = getSupportFragmentManager();	
						mMarkerPoints.clear();
						mGoogleMap.clear();
						LatLng startPoint = new LatLng(mLatitude, mLongitude);
						drawMarker(startPoint);
					}
					
					drawMarker(point);
					
					// Checks, whether start and end locations are captured
					if(mMarkerPoints.size() >= 2){					
						LatLng origin = mMarkerPoints.get(0);
						LatLng dest = mMarkerPoints.get(1);
						
						// Getting URL to the Google Directions API
						String url = getDirectionsUrl(origin, dest);				
						
						DownloadTask downloadTask = new DownloadTask();
						
						// Start downloading json data from Google Directions API
						downloadTask.execute(url);
					}	*/				
				}
			});	
    	}catch(Exception e){
    		
    	}
    }
    @Override
    public void onResume(){
    	int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
    	// Initializing 
		mMarkerPoints = new ArrayList<LatLng>();
		
		// Getting reference to SupportMapFragment of the activity_main
		SupportMapFragment fm = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
		
		// Getting Map for the SupportMapFragment
		mGoogleMap = fm.getMap();
		
		// Enable MyLocation Button in the Map
		mGoogleMap.setMyLocationEnabled(true);
		mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(10));

        getDirectionForMaps();
    	//gpsManager = new GPSManager();
        
        //gpsManager.startListening(getApplicationContext());
        ///gpsManager.setGPSCallback(this);
        //measurement_index = AppSettings.getMeasureUnit(this);
    	/*if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available

            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();

        }else { // Google Play Services are available
        	// Initializing 
			mMarkerPoints = new ArrayList<LatLng>();
			
			// Getting reference to SupportMapFragment of the activity_main
			SupportMapFragment fm = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
			
			// Getting Map for the SupportMapFragment
			mGoogleMap = fm.getMap();
			
			// Enable MyLocation Button in the Map
			mGoogleMap.setMyLocationEnabled(true);
			mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(10));
			// Getting LocationManager object from System Service LOCATION_SERVICE
	        
	        LocationManager mlocManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);  
	        LocationListener mlocListener = new MyLocationListener();  
	        mlocManager.requestLocationUpdates( LocationManager.NETWORK_PROVIDER, 0, 0, this);  
	  
	        if (mlocManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {  
	            
	          } else {  
	              
	              Toast.makeText(this, "GPS is not turned on...", 500);
	          } 
	          
        }	*/	
    	super.onResume();
    }
    public class MyLocationListener implements LocationListener {  
    	  
        public double latitude;  
        public double longitude;  
      
        @Override  
        public void onLocationChanged(Location location)  
        {  
        	location.getLatitude();  
        	location.getLongitude();  
            latitude=location.getLatitude();  
            longitude=location.getLongitude(); 
            if(location != null){
    	        mLatitude = location.getLatitude();
    	        mLongitude = location.getLongitude();
    	        point1 = new LatLng(mLatitude, mLongitude);
    	        /*if(location!=null){
                    onLocationChanged(location);
                }*/

                //locationManager.requestLocationUpdates(provider, 2000, 0, this);
                
    			
    			drawMarker(point1);
            }
    		if(mMarkerPoints.size() < 2){
    			
    			mLatitude = location.getLatitude();
    	        mLongitude = location.getLongitude();
    	        LatLng point = new LatLng(mLatitude, mLongitude);
    	
    	        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
    	        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));        
            
            	drawMarker(point);			
            } 
        }  
      
        @Override  
        public void onProviderDisabled(String provider)  
        {  
            //print "Currently GPS is Disabled";  
        }  
        @Override  
        public void onProviderEnabled(String provider)  
        {  
            //print "GPS got Enabled";  
        }  
        @Override  
        public void onStatusChanged(String provider, int status, Bundle extras)  
        {  
        }  
    }  
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.backbt:
			Intent intent1 = new Intent(NavigationActivity.this, GalleryPreActivity.class);
        	startActivity(intent1);
        	finish();
			break;
		/*case R.id.clearbt:
			searchtx.setText("");
			break;
		case R.id.back1bt:
			break;
		case R.id.back2bt:
			((MyButton)findViewById(R.id.back3bt)).setVisibility(View.VISIBLE);
			((LinearLayout)findViewById(R.id.search_layout)).setVisibility(View.GONE);
			((LinearLayout)findViewById(R.id.park_list_layout)).setVisibility(View.GONE);
			break;
		case R.id.back3bt:
			((MyButton)findViewById(R.id.back3bt)).setVisibility(View.GONE);
			((LinearLayout)findViewById(R.id.search_layout)).setVisibility(View.VISIBLE);
			((LinearLayout)findViewById(R.id.park_list_layout)).setVisibility(View.GONE);
			break;
		case R.id.destination_park_layout:
			parklist.clear();
			ParkAdapter adapter=new ParkAdapter(NavigationActivity.this, R.layout.navigation_park_list, parklist);
			
			park_list.setAdapter(adapter);
			park_list.setSmoothScrollbarEnabled(true);
			park_list.setOnItemClickListener(NavigationActivity.this);
			//((LinearLayout)findViewById(R.id.current_park_layout)).setBackgroundColor(getResources().getColor(R.color.gray));
			//((LinearLayout)findViewById(R.id.destination_park_layout)).setBackgroundColor(getResources().getColor(R.color.blue_trans));
			((LinearLayout)findViewById(R.id.destination_park_layout)).setSelected(true);
			((LinearLayout)findViewById(R.id.current_park_layout)).setSelected(false);
			if(point2.latitude != 0){
				point = point2;
				new GetParkTask().execute();
			}
			break;
		case R.id.current_park_layout:
			parklist.clear();
			ParkAdapter adapter1=new ParkAdapter(NavigationActivity.this, R.layout.navigation_park_list, parklist);
			
			park_list.setAdapter(adapter1);
			park_list.setSmoothScrollbarEnabled(true);
			park_list.setOnItemClickListener(NavigationActivity.this);
			//((LinearLayout)findViewById(R.id.destination_park_layout)).setBackgroundColor(getResources().getColor(R.color.gray));
			//((LinearLayout)findViewById(R.id.current_park_layout)).setBackgroundColor(getResources().getColor(R.color.blue_trans));
			((LinearLayout)findViewById(R.id.destination_park_layout)).setSelected(false);
			((LinearLayout)findViewById(R.id.current_park_layout)).setSelected(true);
			//((LinearLayout)findViewById(R.id.current_park_layout)).setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_parking_nearuser_on));
			if(point1 != null && point1.latitude != 0){
				
				//point = new LatLng(1.298977, 103.809088);
				point = point1;
				new GetParkTask().execute();
			}
			break;
		case R.id.carparkbt:
			//point2 = new LatLng(1.298977, 103.809088);
			if(point2 != null && point2.latitude != 0){
				((MyButton)findViewById(R.id.back3bt)).setVisibility(View.GONE);
				((LinearLayout)findViewById(R.id.search_layout)).setVisibility(View.GONE);
				((LinearLayout)findViewById(R.id.park_list_layout)).setVisibility(View.VISIBLE);
				point = point2;
				new GetParkTask().execute();
			}else{
				ReturnMessage.showAlartDialog(this, "No parking available");
			}
			break;
		case R.id.startrecordbt:
			Intent intent = new Intent(NavigationActivity.this, RecordActivity.class);
        	startActivity(intent);
        	finish();
			break;
		case R.id.yes2bt:
			try{
				new GetYesTask().execute();
			}catch(Exception e){}
			break;
		case R.id.no2bt:
			((LinearLayout)findViewById(R.id.destination_setting_layout)).setVisibility(View.GONE);
			break;
			*/
		}
	}

    @Override
    public void onGPSUpdate(Location location) 
    {
            location.getLatitude();
            location.getLongitude();
            if(location != null){
    	        mLatitude = location.getLatitude();
    	        mLongitude = location.getLongitude();
    	        point1 = new LatLng(mLatitude, mLongitude);
    	        /*if(location!=null){
                    onLocationChanged(location);
                }*/

                //locationManager.requestLocationUpdates(provider, 2000, 0, this);
                
    			
    			drawMarker(point1);
            }
    		if(mMarkerPoints.size() < 2){
    			
    			mLatitude = location.getLatitude();
    	        mLongitude = location.getLongitude();
    	        LatLng point = new LatLng(mLatitude, mLongitude);
    	
    	        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
    	        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));        
            
            	drawMarker(point);			
            } 
    }
	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		// Draw the marker, if destination location is not set
		if(location != null){
	        mLatitude = location.getLatitude();
	        mLongitude = location.getLongitude();
	        point1 = new LatLng(mLatitude, mLongitude);
	        /*if(location!=null){
                onLocationChanged(location);
            }*/

            //locationManager.requestLocationUpdates(provider, 2000, 0, this);
            
			
			drawMarker(point1);
        }
		if(mMarkerPoints.size() < 2){
			
			mLatitude = location.getLatitude();
	        mLongitude = location.getLongitude();
	        LatLng point = new LatLng(mLatitude, mLongitude);
	
	        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
	        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));        
        
        	drawMarker(point);			
        } 
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	private String getDirectionsUrl(LatLng origin,LatLng dest){
					
		// Origin of route
		String str_origin = "origin="+origin.latitude+","+origin.longitude;
		
		// Destination of route
		String str_dest = "destination="+dest.latitude+","+dest.longitude;			
					
		// Sensor enabled
		String sensor = "sensor=false";			
					
		// Building the parameters to the web service
		String parameters = str_origin+"&"+str_dest+"&"+sensor;
					
		// Output format
		String output = "json";
		
		// Building the url to the web service
		String url = "http://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;		
		
		return url;
	}
	
	/** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException{
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
                URL url = new URL(strUrl);

                // Creating an http connection to communicate with url 
                urlConnection = (HttpURLConnection) url.openConnection();

                // Connecting to url 
                urlConnection.connect();

                // Reading data from url 
                iStream = urlConnection.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

                StringBuffer sb  = new StringBuffer();

                String line = "";
                while( ( line = br.readLine())  != null){
                        sb.append(line);
                }
                
                data = sb.toString();

                br.close();

        }catch(Exception e){
                Log.d("Exception while downloading url", e.toString());
        }finally{
                iStream.close();
                urlConnection.disconnect();
        }
        return data;
     }

	
	
    /** A class to download data from Google Directions URL */
	private class DownloadTask extends AsyncTask<String, Void, String>{			
				
		// Downloading data in non-ui thread
		@Override
		protected String doInBackground(String... url) {
				
			// For storing data from web service
			String data = "";
					
			try{
				// Fetching the data from web service
				data = downloadUrl(url[0]);
			}catch(Exception e){
				Log.d("Background Task",e.toString());
			}
			return data;		
		}
		
		// Executes in UI thread, after the execution of
		// doInBackground()
		@Override
		protected void onPostExecute(String result) {			
			super.onPostExecute(result);			
			
			ParserTask parserTask = new ParserTask();
			
			// Invokes the thread for parsing the JSON data
			parserTask.execute(result);
				
		}		
	}
	
	/** A class to parse the Google Directions in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{
    	
    	// Parsing the data in non-ui thread    	
		@Override
		protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
			
			JSONObject jObject;	
			List<List<HashMap<String, String>>> routes = null;			           
            
            try{
            	jObject = new JSONObject(jsonData[0]);
            	DirectionsJSONParser parser = new DirectionsJSONParser();
            	
            	// Starts parsing data
            	routes = parser.parse(jObject);    
            }catch(Exception e){
            	e.printStackTrace();
            }
            return routes;
		}
		
		// Executes in UI thread, after the parsing process
		@Override
		protected void onPostExecute(List<List<HashMap<String, String>>> result) {
			ArrayList<LatLng> points = null;
			PolylineOptions lineOptions = null;
			
			// Traversing through all the routes
			for(int i=0;i<result.size();i++){
				points = new ArrayList<LatLng>();
				lineOptions = new PolylineOptions();
				
				// Fetching i-th route
				List<HashMap<String, String>> path = result.get(i);
				
				// Fetching all the points in i-th route
				for(int j=0;j<path.size();j++){
					HashMap<String,String> point = path.get(j);					
					
					double lat = Double.parseDouble(point.get("lat"));
					double lng = Double.parseDouble(point.get("lng"));
					LatLng position = new LatLng(lat, lng);	
					
					points.add(position);						
				}
				
				// Adding all the points in the route to LineOptions
				lineOptions.addAll(points);
				lineOptions.width(8);
				lineOptions.color(Color.RED);	
				
			}
			
			// Drawing polyline in the Google Map for the i-th route
			mGoogleMap.addPolyline(lineOptions);	
			mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(13));
		}			
    }

	private void drawMarker(LatLng point){
		try{
			mMarkerPoints.add(point);
	    	
	    	// Creating MarkerOptions
			MarkerOptions options = new MarkerOptions();
			
			// Setting the position of the marker
			options.position(point);
			
			/** 
			 * For the start location, the color of marker is GREEN and
			 * for the end location, the color of marker is RED.
			 */
			if(mMarkerPoints.size()==1){
				options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
			}else if(mMarkerPoints.size()==2){
				options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
			}
			
			// Add new marker to the Google Map Android API V2
			mGoogleMap.addMarker(options);
		}catch(Exception e){
			
		}
		
	}
	
	//search process
	private List<Address> getAddressList(String address){
		
		List<Address>  addresslist = new ArrayList<Address>();
		CheckInternetAccess check = new CheckInternetAccess(this);
        if (!check.checkNetwork()) {
        	ReturnMessage.showAlartDialog(this, "No internet connection.");
        }else{
			try {
				search_list_layout.setVisibility(View.VISIBLE);
				searchaddr.clear();
				searchaddress = address;
				new GetAddressTask().execute();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.e("get address ", e.getMessage());
			}
        }
		return addresslist;
	}

	// Retrieve voucher web service async task
	private class GetYesTask extends
	AsyncTask<Void, Void, ArrayList<SearchAddr>> {
		ProgressDialog MyDialog;
       public GetYesTask() {
       }
		@Override
		protected void onPostExecute(ArrayList<SearchAddr> _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();

			getDirectionForMaps();
			search_list_layout.setVisibility(View.GONE);
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(searchtx.getWindowToken(), 0);
			((LinearLayout)findViewById(R.id.destination_setting_layout)).setVisibility(View.GONE);
			((MyButton)findViewById(R.id.back3bt)).setVisibility(View.VISIBLE);
			((LinearLayout)findViewById(R.id.park_list_layout)).setVisibility(View.GONE);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			MyDialog = ProgressDialog.show(NavigationActivity.this, "",
					"Loading... ", true);

			MyDialog.setCancelable(false);
		}

		@Override
		protected ArrayList<SearchAddr> doInBackground(Void... params) {	
			
			ArrayList<SearchAddr> result = new ArrayList<SearchAddr>();

			double lat1 = 0, lon1 = 0;
			if(!setitem.lat.equals("")){
				lat1 = Double.parseDouble(setitem.lat);
			}
			if(!setitem.lon.equals("")){
				lon1 = Double.parseDouble(setitem.lon);
			}
			if(lat1 != 0){
				point2 = new LatLng(lat1, lon1);
				SearchAddr parkaddr = new SearchAddr();
				GetSearchLocation getaddress = new GetSearchLocation();
				String dis2 = getaddress.getDistance(point1, point2);
				String dis1 = getaddress.getRealDis();
				String interval1 = getaddress.getInterval();
				float d1 = 0, in1 = 0;
				if(!dis1.equals("")){
					d1 = (float)Double.parseDouble(dis1);
				}
				if(!interval1.equals("")){
					in1 = (float)Double.parseDouble(interval1);
				}
				if(d1 != 0 && in1 != 0){
					float m = (d1 / 1000.0f) / (in1 / 3600.0f);
					String s = "" + m;
					int idx = 0;
					if((idx = s.indexOf("."))!=-1){
						s = s.substring(0, idx+2);
					}
					parkaddr.speed = s;
				}else{
					parkaddr.speed = "0";
				}
				ShareData.destination = point2;
				parkaddr.addressname = setitem.title;
				parkaddr.dis = dis2;
				parkaddr.lat = lat1;
				parkaddr.lng = lon1;
				ShareData.des_addr = parkaddr;

			}
			return result;
		}
	}
	// Retrieve voucher web service async task
	private class GetAddressTask extends
	AsyncTask<Void, Void, ArrayList<SearchAddr>> {
		ProgressDialog MyDialog;
       public GetAddressTask() {
       }
		@Override
		protected void onPostExecute(ArrayList<SearchAddr> _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			if(_result.size() > 0){

				if(_result.size() < 5 && _result.size() > 0){
					new GetAddressTask1().execute();
				}else{
					search_list_layout.setVisibility(View.VISIBLE);
					SearchAddrAdapter adapter=new SearchAddrAdapter(NavigationActivity.this, R.layout.navigate_serchlist, _result);
									
					searchlist.setAdapter(adapter);
					searchlist.setSmoothScrollbarEnabled(true);
					searchlist.setOnItemClickListener(NavigationActivity.this);
				}
			}else{
				search_list_layout.setVisibility(View.GONE);
				confirmDialog(NavigationActivity.this, "Destination not set.", "Would you like to set your destination?");
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			MyDialog = ProgressDialog.show(NavigationActivity.this, "",
					"Searching... ", true);

			MyDialog.setCancelable(false);
		}

		@Override
		protected ArrayList<SearchAddr> doInBackground(Void... params) {	
			
			ArrayList<SearchAddr> result = new ArrayList<SearchAddr>();
			CheckInternetAccess check = new CheckInternetAccess(NavigationActivity.this);
	        if (check.checkNetwork()) {
				GetSearchLocation getaddress = new GetSearchLocation();
				//getaddress.getCountryCode(point1);
				try{
					JSONObject data = getaddress.getLocationInfo1(searchaddress.replace("\n"," ").replace(" ", "+"), 0, false, point1);
				
					JSONArray arraylist = (JSONArray)data.get("results");
					for(int i = 0; i < arraylist.length(); i++){					
						SearchAddr add = new SearchAddr();
						add.addressname = arraylist.getJSONObject(i).getString("formatted_address");
						add.lat = arraylist.getJSONObject(i).getJSONObject("geometry").getJSONObject("location").getDouble("lat");
						add.lng = arraylist.getJSONObject(i).getJSONObject("geometry").getJSONObject("location").getDouble("lng");
						LatLng pos2 = new LatLng(add.lat, add.lng);
						add.dis = getaddress.getDistance(point1, pos2);
						String dis1 = getaddress.getRealDis();
						String interval1 = getaddress.getInterval();
						float d1 = 0, in1 = 0;
						if(!dis1.equals("")){
							d1 = (float)Double.parseDouble(dis1);
						}
						if(!interval1.equals("")){
							in1 = (float)Double.parseDouble(interval1);
						}
						if(d1 != 0 && in1 != 0){
							float m = (d1 / 1000.0f) / (in1 / 3600.0f);
							String s = "" + m;
							int idx = 0;
							if((idx = s.indexOf("."))!=-1){
								s = s.substring(0, idx+2);
							}
							add.speed = s;
						}else{
							add.speed = "0";
						}
						result.add(add);
					}
				}catch(Exception e){
					
				}
				searchaddr.addAll(result);
	        }
			return result;
		}
	}

	 public void confirmDialog(Context context, String title, String content){
			new AlertDialog.Builder(context)
			.setTitle(title)
			.setMessage(
					content)
			.setPositiveButton("YES",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();						
						}
					})
			.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog4, int which) {
					// TODO Auto-generated method stub
					Intent intent1 = new Intent(NavigationActivity.this, RecordActivity.class);
		        	startActivity(intent1);
		        	finish();
				}
			}).show();
	 }
	// Retrieve voucher web service async task
	private class GetAddressTask1 extends
	AsyncTask<Void, Void, ArrayList<SearchAddr>> {
		ProgressDialog MyDialog;
       public GetAddressTask1() {
       }
		@Override
		protected void onPostExecute(ArrayList<SearchAddr> _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			//if(_result.size() > 0){
				search_list_layout.setVisibility(View.VISIBLE);
				SearchAddrAdapter adapter=new SearchAddrAdapter(NavigationActivity.this, R.layout.navigate_serchlist, searchaddr);
								
				searchlist.setAdapter(adapter);
				searchlist.setSmoothScrollbarEnabled(true);
				searchlist.setOnItemClickListener(NavigationActivity.this);
			//}else{
				//search_list_layout.setVisibility(View.GONE);
			//}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			MyDialog = ProgressDialog.show(NavigationActivity.this, "",
					"Searching... ", true);

			MyDialog.setCancelable(false);
		}

		@Override
		protected ArrayList<SearchAddr> doInBackground(Void... params) {	
			
			ArrayList<SearchAddr> result = new ArrayList<SearchAddr>();
			CheckInternetAccess check = new CheckInternetAccess(NavigationActivity.this);
	        if (check.checkNetwork()) {
				GetSearchLocation getaddress = new GetSearchLocation();
				try{
					JSONObject data = getaddress.getLocationInfo2(searchaddress.replace("\n"," ").replace(" ", "+"), 0, false, searchaddr.get(0));
				
					JSONArray arraylist = (JSONArray)data.get("results");
					for(int i = 0; i < arraylist.length(); i++){					
						SearchAddr add = new SearchAddr();
						add.addressname = arraylist.getJSONObject(i).getString("name");
						add.lat = arraylist.getJSONObject(i).getJSONObject("geometry").getJSONObject("location").getDouble("lat");
						add.lng = arraylist.getJSONObject(i).getJSONObject("geometry").getJSONObject("location").getDouble("lng");
						LatLng pos2 = new LatLng(add.lat, add.lng);
						add.dis = getaddress.getDistance(point1, pos2);
						String dis1 = getaddress.getRealDis();
						String interval1 = getaddress.getInterval();
						float d1 = 0, in1 = 0;
						if(!dis1.equals("")){
							d1 = (float)Double.parseDouble(dis1);
						}
						if(!interval1.equals("")){
							in1 = (float)Double.parseDouble(interval1);
						}
						if(d1 != 0 && in1 != 0){
							float m = (d1 / 1000.0f) / (in1 / 3600.0f);
							String s = "" + m;
							int idx = 0;
							if((idx = s.indexOf("."))!=-1){
								s = s.substring(0, idx+2);
							}
							add.speed = s;
						}else{
							add.speed = "0";
						}
						result.add(add);
					}
				}catch(Exception e){
					
				}
				searchaddr.addAll(result);
	        }
			return result;
		}
	}
	public String getDistance1(LatLng pos1, LatLng pos2){
	        double d2r = Math.PI / 180;
	        double dLong = (pos2.longitude - pos1.longitude) * d2r;
	        double dLat = (pos2.latitude - pos1.latitude) * d2r;
	        double a = Math.pow(Math.sin(dLat / 2.0), 2) + Math.cos(pos1.latitude * d2r)
	                * Math.cos(pos2.latitude * d2r) * Math.pow(Math.sin(dLong / 2.0), 2);
	        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	        double d = 6367000 * c;
	        int index =  0;
	        String dis = "" + Math.round(d);
			try{
				if((index = dis.indexOf(".")) !=-1){
					String end = dis.substring(index+1,dis.length());
					dis = dis.substring(0,index+1);
					if(end.length()>1){
						end = end.substring(0,1);
					}
					dis = dis + end;
				}
			}catch(Exception e){
				dis = "";
			}
	        return dis;
	}
	public String getDistance(LatLng pos1, LatLng pos2){
		String latitude=""+pos1.latitude;
		String longitude=""+pos1.longitude;
		String dis = "";
		if(longitude!=null && latitude!=null)
		{
			String slatitude=latitude.substring(1);
			String slongitude=longitude.substring(1);
			float flatitude = Float.valueOf(slatitude);
			float flongitude = Float.valueOf(slongitude);
			Location shopLoc = new Location("");    
			shopLoc.setLatitude(pos2.latitude); //get this value from your xml file
			shopLoc.setLongitude(pos2.longitude); //get this value from your xml file
			
			Location userLoc = new Location("");
			userLoc.setLatitude(flatitude); //get this value from gps or other position device
			userLoc.setLongitude(flongitude); //get this value from gps or other position device
			//Log.d("distance", ""+lat+":"+lng);
			//Finaly get the distance with
			float distance = userLoc.distanceTo(shopLoc);
			Log.d("distance2", ""+distance);
			//if(distance > 9999999){
				//distance = 0.0f;
			//}else{
				distance = distance / 1000f;
			//}
			dis = "" + distance;
			//if(distance > 600){
				//dis = "";
			//}
			int index =  0;
			try{
				if((index = dis.indexOf(".")) !=-1){
					String end = dis.substring(index+1,dis.length());
					dis = dis.substring(0,index+1);
					if(end.length()>1){
						end = end.substring(0,1);
					}
					dis = dis + end;
				}
			}catch(Exception e){
				dis = "";
			}
		}
		return dis;
	}
	public class SearchAddrAdapter extends ArrayAdapter<SearchAddr>
	{
		private ArrayList<SearchAddr>items;
		public SearchAddrAdapter(Context context, int textViewResourceId,
				ArrayList<SearchAddr> objects) {
			
			super(context, textViewResourceId, objects);
			this.items=objects;
			// TODO Auto-generated constructor stub
		}
		
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View v=convertView;
			final SearchAddr item=items.get(position);
			if(v==null)
			{
				v = vi.inflate(R.layout.navigate_serchlist, null);
			}
			final int po = position;
			MyTextView address = (MyTextView)v.findViewById(R.id.address);
			String addr = item.addressname;
			if(item.addressname.length() > 60){
				addr = item.addressname.substring(0, 60) + "...";
			}
			address.setText(addr);
			MyTextView dis = (MyTextView)v.findViewById(R.id.distance);
			dis.setText(item.dis);
			MyButton go = (MyButton)v.findViewById(R.id.gobt);
			go.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try{
						SearchAddr addr = searchaddr.get(po);
						point2 = new LatLng(addr.lat, addr.lng);
						getDirectionForMaps();
						InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
	        			imm.hideSoftInputFromWindow(searchtx.getWindowToken(), 0);
						search_list_layout.setVisibility(View.GONE);
						ShareData.destination = point2;
						ShareData.des_addr = addr;
						((LinearLayout)findViewById(R.id.search_layout)).setVisibility(View.GONE);
						((MyButton)findViewById(R.id.back3bt)).setVisibility(View.VISIBLE);
					}catch(Exception e){
						
					}
				}
			});
			return v;
		}
		
	}

	public class ParkAdapter extends ArrayAdapter<busStopCodeSet>
	{
		private List<busStopCodeSet>items;
		public ParkAdapter(Context context, int textViewResourceId,
				List<busStopCodeSet> objects) {
			
			super(context, textViewResourceId, objects);
			this.items=objects;
			// TODO Auto-generated constructor stub
		}
		
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View v=convertView;
			final busStopCodeSet item=items.get(position);
			if(v==null)
			{
				v = vi.inflate(R.layout.navigation_park_list, null);
			}
			final int po = position;
			MyTextView p_no = (MyTextView)v.findViewById(R.id.p_no);
			p_no.setText(item.p_no);
			MyTextView title = (MyTextView)v.findViewById(R.id.title);
			String title1 = item.title;
			if(title1.length() > 60){
				title1 = title1.substring(0, 60) + "...";
			}
			title.setText(title1);
			MyTextView dis = (MyTextView)v.findViewById(R.id.dis);
			float dis1=0;
			if(!item.dis.equals("")){
				dis1 = (float)Integer.parseInt(item.dis) / (float)1000;
			}
			dis.setText(dis1 + "KM");
			v.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					setitem = item;
					((LinearLayout)findViewById(R.id.destination_setting_layout)).setVisibility(View.VISIBLE);
					
				}
			});
			return v;
		}
		
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		// TODO Auto-generated method stub
		// select processing
		
	}
	// Retrieve voucher web service async task
		private class GetParkTask extends
		AsyncTask<Void, Void, List<busStopCodeSet>> {
			ProgressDialog MyDialog;
	       public GetParkTask() {
	       }
			@Override
			protected void onPostExecute(List<busStopCodeSet> _result) {
				super.onPostExecute(_result);
				MyDialog.dismiss();
				if(_result.size() > 0){
					park_list.setVisibility(View.VISIBLE);
					((MyTextView)findViewById(R.id.noparklist)).setVisibility(View.GONE);
					//search_list_layout.setVisibility(View.GONE);
					ParkAdapter adapter=new ParkAdapter(NavigationActivity.this, R.layout.navigation_park_list, _result);
									
					park_list.setAdapter(adapter);
					park_list.setSmoothScrollbarEnabled(true);
					park_list.setOnItemClickListener(NavigationActivity.this);
				}else{
					park_list.setVisibility(View.GONE);
					((MyTextView)findViewById(R.id.noparklist)).setVisibility(View.VISIBLE);
					//ReturnMessage.showAlartDialog(NavigationActivity.this, "No parking available");
				}
			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				 MyDialog = ProgressDialog.show(NavigationActivity.this, "",
		                    "Loading Parking... ", false);
		            MyDialog.setCancelable(true);
			}

			@Override
			protected List<busStopCodeSet> doInBackground(Void... params) {	
				
				List<busStopCodeSet> result = new ArrayList<busStopCodeSet>();
				CheckInternetAccess check = new CheckInternetAccess(NavigationActivity.this);
		        if (check.checkNetwork()) {
					result = getParkingList();
					parklist.clear();
					parklist.addAll(result);
		        }
				return result;
			}
		}
	private List<busStopCodeSet> getParkingList(){
		List<busStopCodeSet> list = new ArrayList<busStopCodeSet>();
		int skip = 0; 
		boolean cont = true;
		URLConnection conn = null;
			//String apiUrl = "http://datamall.mytransport.sg/ltaodataservice.svc/CarParkSet?Latitude="+point.latitude+"&Longitude="+point.longitude+"&Distance=2000";
		String apiUrl = "http://datamall.mytransport.sg/ltaodataservice.svc/CarParkSet?Latitude="+point.latitude+"&Longitude="+point.longitude+"&Distance=500000";
			try {
				URL url = new URL(apiUrl);
				conn = url.openConnection(); 
				conn.setRequestProperty("accept", "*/*");
				conn.addRequestProperty("AccountKey", "WWEVX0Q7YIcB/+/CkssoGw==");
				conn.addRequestProperty("UniqueUserID", "15d3e312-d3d1-4eff-a066-dab24f32e220");
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String line = null; 
				StringBuilder strBuilder = new StringBuilder(); 
				while ((line = br.readLine()) != null) {
					strBuilder.append(line); 
					//System.out.println(line); 
				}
				list = XMLParsing1(strBuilder.toString());
			} catch (Exception ex) {
				ex.printStackTrace(); 
			}
		return list;
	}
	public List<busStopCodeSet> XMLParsing(String responseBody){
		List<busStopCodeSet> result = new ArrayList<busStopCodeSet>();
		Document doc;
		try {
			doc = XMLParserHelper.getDocumentFromString(responseBody);
			Node rootNode = doc.getFirstChild();
			
			String errorCode = XMLParserHelper.getStringIfSingleNode(rootNode);
			if (errorCode != null){
				
			}else{
				
				Node entry = XMLParserHelper.getChildNodeByTagName(rootNode, "entry");
						
				while (entry != null){
					Node content = XMLParserHelper.getChildNodeByTagName(entry, "content");
					Node property = XMLParserHelper.getChildNodeByTagName(content, "m:properties");
					Node park = XMLParserHelper.getChildNodeByTagName(property, "d:CarParkID ");
					String p_no = park.getNodeValue();
					Node summary1 = XMLParserHelper.getChildNodeByTagName(property, "d:Summary");
					String title1 = summary1.getNodeValue();
					title1 = title1.replace("Development:", "");
					Node lat1 = XMLParserHelper.getChildNodeByTagName(property, "d:Latitude");
					String lat = lat1.getNodeValue();
					Node lon1 = XMLParserHelper.getChildNodeByTagName(property, "d:Longitude");
					String lon = lon1.getNodeValue();
					Node dis1 = XMLParserHelper.getChildNodeByTagName(property, "d:Distance ");
					String dis = dis1.getNodeValue();
					int idx = 0;
					if((idx = dis.indexOf(".")) == -1){
					}else{
						dis = dis.substring(0, idx);
					}

					busStopCodeSet pl = new busStopCodeSet(p_no, title1, lat, lon, dis);
					result.add(pl);
					entry = XMLParserHelper.getChildNodeByTagName(rootNode, "entry");
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	public List<busStopCodeSet> XMLParsing1(String responseBody){
		List<busStopCodeSet> result = new ArrayList<busStopCodeSet>();
		try{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();
	
			xpp.setInput( new StringReader(responseBody));
			int eventType = xpp.getEventType();
			String tag = "";
			String value = "";
			
			boolean save = false;
			boolean flg1 = false,flg2 = false,flg3 = false,flg4 = false,flg5 = false;
			String p_no = "0";
			String title1 = "";
			String lat = "0";
			String lon = "0";
			String dis = "0";
			while (eventType != XmlPullParser.END_DOCUMENT) {
				if(eventType == XmlPullParser.START_TAG) {
					
					tag = xpp.getName();
					
					if(tag.compareTo("properties") == 0)
						save = true;
					
				} else if(eventType == XmlPullParser.TEXT) {
					
					value = xpp.getText();
					
					if (value == null)
						value = "";
					
					//only save the value under DocumentElement
					if (save){
						
						//if already have the same key, put it into array
						
						if(tag.equals("CarParkID") && !flg1){
							flg1 = true;
						}else if(tag.equals("Summary") && !flg2){
							title1 = value;
							title1 = title1.replace("Development:", "");
							int idx = 0;
							p_no = "0";
							if((idx = title1.indexOf("Lots:"))!= -1){
								p_no = title1.substring(idx+5, title1.length());
								title1 = title1.substring(0, idx);
							}
							//p_no = value;
							flg2 = true;
						}else if(tag.equals("Latitude") && !flg3){
							lat = value;
							flg3 = true;
						}else if(tag.equals("Longitude") && !flg4){
							lon = value;
							flg4 = true;
						}else if(tag.equals("Distance") && !flg5){
							dis = value;
							int idx = 0;
							if((idx = dis.indexOf(".")) == -1){
							}else{
								dis = dis.substring(0, idx);
							}
							flg5 = true;
						}
					}
					
				} else if(eventType == XmlPullParser.END_TAG) {
	
					if(xpp.getName().compareTo("content") == 0){
						busStopCodeSet pl = new busStopCodeSet(p_no, title1, lat, lon, dis);
						result.add(pl);
						save = false;	
						p_no = "0";
						title1 = "";
						lat = "0";
						lon = "0";
						dis = "0";
						flg1 = false;
						flg2 = false;
						flg3 = false;
						flg4 = false;
						flg5 = false;
					}
				} 
				
				eventType = xpp.next();
			}
			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	 @Override
	 public void onBackPressed() {
			Intent intent1 = new Intent(NavigationActivity.this, GalleryPreActivity.class);
        	startActivity(intent1);
        	finish();
	 }


	    @Override
	    protected void onDestroy() {
	           // gpsManager.stopListening();
	           // gpsManager.setGPSCallback(null);
	            
	          //  gpsManager = null;
	            
	            super.onDestroy();
	    }
}
