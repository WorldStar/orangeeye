package com.orangeeye;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.orangeeye.db.DBAdapter;
import com.orangeeye.db.OtherData;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;
import com.orangeeye.utils.MySwitch;
import com.orangeeye.utils.MySwitch.OnChangeAttemptListener;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

public class NotificationActivity extends Activity implements OnClickListener, OnCheckedChangeListener{
	Intent intent;
	OtherData other;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_screen);
        ((MyButton)findViewById(R.id.backbt)).setOnClickListener(this);
        ((MySwitch)findViewById(R.id.switch1)).setOnCheckedChangeListener(this);
        DBAdapter.init(this);
        other = DBAdapter.getOtherData("notification");
        if(other != null){
        	if(other.value.equals("1")){
        		((MySwitch)findViewById(R.id.switch1)).setChecked(true);
        	}
        }else{
        	((MySwitch)findViewById(R.id.switch1)).setChecked(true);
        }
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.backbt:
			intent = new Intent(NotificationActivity.this, SettingsActivity.class);
        	startActivity(intent);
        	finish();
			break;
		}
		
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		String value = "0";
		if (isChecked){
			value = "1";
		}else{
			value = "0";
		}
		if(other == null){
			OtherData other1 = new OtherData(0, "notification", value, "");
    		DBAdapter.addOtherData(other1);
		}else{
			OtherData other1 = new OtherData(other._id, "notification", value, "");
    		DBAdapter.updateOtherData(other1);
		}
	}
	 @Override
	 public void onBackPressed() {
		Intent intent3 = new Intent(NotificationActivity.this, SettingsActivity.class);
		startActivity(intent3);
		finish();
	 }
    
}
