package com.orangeeye;

import com.orangeeye.db.DBAdapter;
import com.orangeeye.db.OtherData;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

public class OrangeMainActivity extends Activity implements OnClickListener{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orange_main);
        ((MyButton)findViewById(R.id.yesbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.nobt)).setOnClickListener(this);

    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.nobt:
			Value_Info.incomeflg = false;
	        DBAdapter.init(this);
	        OtherData setdata1 = DBAdapter.getOtherData("ntuc_data");
	        if(setdata1 == null){
	        	OtherData other = new OtherData(0,"ntuc_data", "0", "");
	        	DBAdapter.addOtherData(other);
	        }else{
	        	OtherData other = new OtherData(setdata1._id,"ntuc_data", "0", "");
	        	DBAdapter.updateOtherData(other);
	        }
			Intent intent1 = new Intent(OrangeMainActivity.this, MMSActivity.class);
        	startActivity(intent1);
        	finish();
			break;
		case R.id.yesbt:
			//Value_Info.incomeflg = true;
	        DBAdapter.init(this);
	        OtherData setdata = DBAdapter.getOtherData("ntuc_data");
	        if(setdata == null){
	        	OtherData other = new OtherData(0,"ntuc_data", "1", "");
	        	DBAdapter.addOtherData(other);
	        }else{
	        	OtherData other = new OtherData(setdata._id,"ntuc_data", "1", "");
	        	DBAdapter.updateOtherData(other);
	        }
			Intent intent = new Intent(OrangeMainActivity.this, ContactsActivity.class);
        	startActivity(intent);
        	finish();
			break;
		}
	}

    
}
