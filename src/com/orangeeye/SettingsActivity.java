package com.orangeeye;

import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

public class SettingsActivity extends Activity implements OnClickListener{
	LinearLayout setting_layout_1 ,setting_layout_2,setting_layout_3,setting_layout_4,setting_layout_5,setting_layout_6,setting_layout_7,setting_layout_8;
	Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_screen);
        ((MyButton)findViewById(R.id.backbt)).setOnClickListener(this);
        setting_layout_1 = (LinearLayout)findViewById(R.id.setting_layout_1);
        setting_layout_2 = (LinearLayout)findViewById(R.id.setting_layout_2);
        setting_layout_3 = (LinearLayout)findViewById(R.id.setting_layout_3);
        setting_layout_4 = (LinearLayout)findViewById(R.id.setting_layout_4);
        setting_layout_5 = (LinearLayout)findViewById(R.id.setting_layout_5);
        setting_layout_6 = (LinearLayout)findViewById(R.id.setting_layout_6);
        setting_layout_7 = (LinearLayout)findViewById(R.id.setting_layout_7);
        setting_layout_8 = (LinearLayout)findViewById(R.id.setting_layout_8);
        setting_layout_1.setOnClickListener(this);
        setting_layout_2.setOnClickListener(this);
        setting_layout_3.setOnClickListener(this);
        setting_layout_4.setOnClickListener(this);
        setting_layout_5.setOnClickListener(this);
        setting_layout_6.setOnClickListener(this);
        setting_layout_7.setOnClickListener(this);
        setting_layout_8.setOnClickListener(this);
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.backbt:
			intent = new Intent(SettingsActivity.this, HomeActivity.class);
        	startActivity(intent);
        	finish();
			break;
		case R.id.setting_layout_1:
			intent = new Intent(SettingsActivity.this, EmergencyNumberActivity.class);
        	startActivity(intent);
        	finish();
			break;
		case R.id.setting_layout_2:
			
			break;
		case R.id.setting_layout_3:
			intent = new Intent(SettingsActivity.this, NotificationActivity.class);
	    	startActivity(intent);	
        	finish();
			break;
		case R.id.setting_layout_4:
			intent = new Intent(SettingsActivity.this, AboutActivity.class);
        	startActivity(intent);	
        	finish();
			break;
		case R.id.setting_layout_5:
			intent = new Intent(SettingsActivity.this, RecordingSettingActivity.class);
        	startActivity(intent);
        	finish();
			break;
		case R.id.setting_layout_6:
			intent = new Intent(SettingsActivity.this, ReminderActivity.class);
        	startActivity(intent);	
        	finish();		
			break;
		case R.id.setting_layout_7:
			intent = new Intent(SettingsActivity.this, FaqActivity.class);
        	startActivity(intent);
        	finish();	
			break;
		case R.id.setting_layout_8:
			intent = new Intent(SettingsActivity.this, ShareAppActivity.class);
        	startActivity(intent);	
        	finish();
			break;
		}
	}

	 @Override
	 public void onBackPressed() {
		Intent intent3 = new Intent(SettingsActivity.this, HomeActivity.class);
    	startActivity(intent3);
		finish();
	 }
   
    
}
