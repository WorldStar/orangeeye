package com.orangeeye;

import com.orangeeye.data.ShareData;
import com.orangeeye.db.DBAdapter;
import com.orangeeye.db.OtherData;
import com.orangeeye.lib.ReturnMessage;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EmergencyCallActivity extends Activity implements OnClickListener{
	CheckBox box1 , box2;
	EditText emer_call;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.emergency_call_screen);
        ((MyButton)findViewById(R.id.savebt)).setOnClickListener(this);
        ((ImageView)findViewById(R.id.closebt1)).setOnClickListener(this);
        ((TextView)findViewById(R.id.terms)).setOnClickListener(this);
        emer_call = (EditText)findViewById(R.id.emer_call);
        emer_call.setTypeface(Typeface.createFromAsset(getAssets(), this.getResources().getString(R.string.font_string)));
        box1 = (CheckBox)findViewById(R.id.checkBox1);
        box1.setTypeface(Typeface.createFromAsset(getAssets(), this.getResources().getString(R.string.font_string)));
        box2 = (CheckBox)findViewById(R.id.checkBox2);
        box2.setTypeface(Typeface.createFromAsset(getAssets(), this.getResources().getString(R.string.font_string)));

		DBAdapter.init(this);
		
        OtherData setdata = DBAdapter.getOtherData("emer_data");
        if(setdata != null){
        	emer_call.setText(setdata.value);
        	if(setdata.otherflg.equals("1")){
        		box1.setChecked(true);
        	}
        }
        WebView mWebView = null;
        mWebView = (WebView) findViewById(R.id.webView1);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl("file:///android_asset/html/tnc/tnc.html");
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.terms:
			((LinearLayout)findViewById(R.id.terms_layout)).setVisibility(View.VISIBLE);
			break;
		case R.id.closebt1:
			((LinearLayout)findViewById(R.id.terms_layout)).setVisibility(View.GONE);
			break;
		case R.id.savebt:
			if(!box2.isChecked()){
				ReturnMessage.showAlartDialog(EmergencyCallActivity.this,"Terms & Conditions", "Please agree to the Terms & Conditions to proceed.");
				return;
			}else{
				if(emer_call.getText().toString().equals("")){
					confirmDialog(EmergencyCallActivity.this, "Emergency message and phone numbers", "Are you sure to proceed without entering these information?");
					
				}else{
					save();
				}
			}
			break;
		}
	}

	public void confirmDialog(Activity activity, String title, String content) {
		new AlertDialog.Builder(activity)
				.setTitle(title)
				.setMessage(
						content)
				.setPositiveButton("YES",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
								save();
							}
						})
				.setNegativeButton("NO", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog4, int which) {
						// TODO Auto-generated method stub
						dialog4.cancel();
					}
				}).show();
	}
	public void save(){
		String flg = "";
		if(box1.isChecked()){
			flg = "1";
		}else{
			flg = "0";
		}
		
        OtherData setdata = DBAdapter.getOtherData("emer_data");
        if(setdata == null){
        	OtherData other = new OtherData(0,"emer_data", emer_call.getText().toString(), flg);
        	DBAdapter.addOtherData(other);
        }else{
        	OtherData other = new OtherData(setdata._id,"emer_data", emer_call.getText().toString(), flg);
        	DBAdapter.updateOtherData(other);
        }
		//ShareData.auto_120min_flg = true;
		Intent intent = new Intent(EmergencyCallActivity.this, HomeActivity.class);
    	startActivity(intent);
    	finish();
	}
    
}
