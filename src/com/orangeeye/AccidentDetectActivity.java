package com.orangeeye;

import java.io.File;

import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

public class AccidentDetectActivity extends Activity implements OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accident_detect);
        ((MyButton)findViewById(R.id.callbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.makecallbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.mmsbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.cancelbt)).setOnClickListener(this);
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.callbt:
			Intent callIntent = new Intent(Intent.ACTION_CALL);
	        callIntent.setData(Uri.parse("tel:03-1238-234"));
	        startActivity(callIntent);
			break;
		case R.id.makecallbt:
			Intent callIntent1 = new Intent(Intent.ACTION_CALL);
	        callIntent1.setData(Uri.parse("tel:03-1238-234"));
	        startActivity(callIntent1);
			break;
		case R.id.mmsbt:
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.putExtra("sms_body", "Hi how are you");
			//intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File("/sdcard/file.gif")));
			//intent.setType("image/gif"); 
			startActivity(Intent.createChooser(intent,"Send"));
			break;
		case R.id.cancelbt:
			finish();
			break;
		}
	}

    
}
