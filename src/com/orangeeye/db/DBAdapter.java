package com.orangeeye.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBAdapter {
    
    /******* if debug is set true then it will show all Logcat message ***/
    public static final boolean DEBUG = true;
     
    /********** Logcat TAG ************/
    public static final String LOG_TAG = "DBAdapter";
     
    /************ Table Fields ************/
    public static final String KEY_ID = "_id";
 
    public static final String KEY_RECORD_TIME = "record_time"; 
    public static final String KEY_RECORD_START = "record_start";
    public static final String KEY_RECORD_DESTINATION = "record_destination";
    public static final String KEY_RECORD_DISTANCE = "record_distance";
    public static final String KEY_RECORD_SPEED = "record_speed";
    public static final String KEY_RECORD_FILE = "record_file";
    public static final String KEY_RECORD_SLAT = "record_start_lat";
    public static final String KEY_RECORD_SLNG = "record_start_lng";
    public static final String KEY_RECORD_ELAT = "record_end_lat";
    public static final String KEY_RECORD_ELNG = "record_end_lng";

    public static final String KEY_OTHER_TITLE = "title"; 
    public static final String KEY_OTHER_VALUE = "value";
    public static final String KEY_OTHER_OTHERFLG = "otherflg";
     
     
    /************* Database Name ************/
    public static final String DATABASE_NAME = "DB_Record";
     
    /**** Database Version (Increase one if want to also upgrade your database) ****/
    public static final int DATABASE_VERSION = 5;// started at 1
 
    /** Table names */
    public static final String RECORD_TABLE = "tbl_record";
    public static final String OTHER_TABLE = "tbl_other";
     
    /**** Set all table with comma seperated like USER_TABLE,ABC_TABLE ******/
    private static final String[ ] ALL_TABLES = { RECORD_TABLE , OTHER_TABLE};
     
    /** Create table syntax */
    private static final String RECORD_CREATE = "create table tbl_record ( _id integer primary key autoincrement, record_time text not null, record_start text not null, record_destination text not null, record_distance text not null, record_speed text not null, record_file text not null, record_start_lat text not null , record_start_lng text not null, record_end_lat text not null, record_end_lng text not null);";
    private static final String OTHER_CREATE = "create table tbl_other ( _id integer primary key autoincrement, title text not null, value text not null, otherflg text not null);";
     
    /********* Used to open database in syncronized way *********/
    private static DataBaseHelper DBHelper = null;
 
    protected DBAdapter() {
     
    }
     
    /********** Initialize database *********/
    public static void init(Context context) {
        if (DBHelper == null) {
            if (DEBUG)
                Log.i("DBAdapter", context.toString());
            DBHelper = new DataBaseHelper(context);
        }
    }
     
  /********** Main Database creation INNER class ********/
    private static class DataBaseHelper extends SQLiteOpenHelper {
        public DataBaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
 
        @Override
        public void onCreate(SQLiteDatabase db) {
            if (DEBUG)
                Log.i(LOG_TAG, "new create");
            try {
                db.execSQL(RECORD_CREATE);
                db.execSQL(OTHER_CREATE);
                 
 
            } catch (Exception exception) {
                if (DEBUG)
                    Log.i(LOG_TAG, exception.getMessage());
            }
        }
 
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            if (DEBUG)
                Log.w(LOG_TAG, "Upgrading database from version" + oldVersion
                        + "to" + newVersion + "...");
 
            for (String table : ALL_TABLES) {
                db.execSQL("DROP TABLE IF EXISTS " + table);
            }
            //onCreate(db);
            try {
                db.execSQL(RECORD_CREATE);
                db.execSQL(OTHER_CREATE);
                 
 
            } catch (Exception exception) {
                if (DEBUG)
                    Log.i(LOG_TAG, exception.getMessage());
            }
        }
 
    } // Inner class closed
     
     
    /***** Open database for insert,update,delete in syncronized manner ****/
    private static synchronized SQLiteDatabase open() throws SQLException {
        return DBHelper.getWritableDatabase();
    }
 
 
    /************* General functions*************/
     
     
    /*********** Escape string for single quotes (Insert,Update) ********/
    private static String sqlEscapeString(String aString) {
        String aReturn = "";
         
        if (null != aString) {
            //aReturn = aString.replace(", );
            aReturn = DatabaseUtils.sqlEscapeString(aString);
            // Remove the enclosing single quotes ...
            aReturn = aReturn.substring(1, aReturn.length() - 1);
        }
         
        return aReturn;
    }
 
    /********** UnEscape string for single quotes (show data) ************/
    private static String sqlUnEscapeString(String aString) {
         
        String aReturn = "";
         
        if (null != aString) {
            //aReturn = aString.replace(, ");
        }
         
        return aReturn;
    }
     
   /********* User data functons *********/
 
  public static void addRecordData(RecordData uData) {
 
        // Open database for Read / Write       
 
         final SQLiteDatabase db = open();
         
        String record_time = uData.record_time;
        String record_start = uData.record_start;
        String record_destination = uData.record_destination;
        String record_speed = uData.record_speed;
        String record_distance = uData.record_distance;
        String record_file = uData.record_file;
        String record_s_lat = uData.record_s_lat;
        String record_s_lng = uData.record_s_lng;
        String record_e_lat = uData.record_e_lat;
        String record_e_lng = uData.record_e_lng;
        ContentValues cVal = new ContentValues();
        cVal.put(KEY_RECORD_TIME, record_time);
        cVal.put(KEY_RECORD_START, record_start);
        cVal.put(KEY_RECORD_DESTINATION, record_destination);
        cVal.put(KEY_RECORD_DISTANCE, record_distance);
        cVal.put(KEY_RECORD_SPEED, record_speed);
        cVal.put(KEY_RECORD_FILE, record_file);
        cVal.put(KEY_RECORD_SLAT, record_s_lat);
        cVal.put(KEY_RECORD_SLNG, record_s_lng);
        cVal.put(KEY_RECORD_ELAT, record_e_lat);
        cVal.put(KEY_RECORD_ELNG, record_e_lng);
        // Insert user values in database
        db.insert(RECORD_TABLE, null, cVal);     
        db.close(); // Closing database connection
    }

  	public static void addOtherData(OtherData uData) {
 
        // Open database for Read / Write       
 
         final SQLiteDatabase db = open();
         
        String title = uData.title;
        String value = uData.value;
        String otherflg = uData.otherflg;
        ContentValues cVal = new ContentValues();
        cVal.put(KEY_OTHER_TITLE, title);
        cVal.put(KEY_OTHER_VALUE, value);
        cVal.put(KEY_OTHER_OTHERFLG, otherflg);
        // Insert user values in database
        db.insert(OTHER_TABLE, null, cVal);     
        db.close(); // Closing database connection
    }
 
 
   // Updating single data
   public static int updateRecordData(RecordData data) {
 
        final SQLiteDatabase db = open();
  
        ContentValues values = new ContentValues();
        String record_time = data.record_time;
        String record_start = data.record_start;
        String record_destination = data.record_destination;
        String record_speed = data.record_speed;
        String record_distance = data.record_distance;
        String record_file = data.record_file;
        String record_s_lat = data.record_s_lat;
        String record_s_lng = data.record_s_lng;
        String record_e_lat = data.record_e_lat;
        String record_e_lng = data.record_e_lng;

        values.put(KEY_RECORD_TIME, record_time);
        values.put(KEY_RECORD_START, record_start);
        values.put(KEY_RECORD_DESTINATION, record_destination);
        values.put(KEY_RECORD_DISTANCE, record_distance);
        values.put(KEY_RECORD_SPEED, record_speed);
        values.put(KEY_RECORD_FILE, record_file);
        values.put(KEY_RECORD_SLAT, record_s_lat);
        values.put(KEY_RECORD_SLNG, record_s_lng);
        values.put(KEY_RECORD_ELAT, record_e_lat);
        values.put(KEY_RECORD_ELNG, record_e_lng);
        // updating row
        return db.update(RECORD_TABLE, values, KEY_ID + " = ?",
                new String[] { String.valueOf(data._id) });
    }

   // Updating single data
   public static int updateOtherData(OtherData data) {
 
        final SQLiteDatabase db = open();
  
        String title = data.title;
        String value = data.value;
        String otherflg = data.otherflg;
        ContentValues cVal = new ContentValues();
        cVal.put(KEY_OTHER_TITLE, title);
        cVal.put(KEY_OTHER_VALUE, value);
        cVal.put(KEY_OTHER_OTHERFLG, otherflg);

        // updating row
        return db.update(OTHER_TABLE, cVal, KEY_ID + " = ?",
                new String[] { String.valueOf(data._id) });
    }
    
    // Getting single contact
   public static RecordData getRecordData(int id) {
 
       // Open database for Read / Write
	   RecordData data = null;
	   try{
        final SQLiteDatabase db = open();
  
        Cursor cursor = db.query(RECORD_TABLE, new String[] { KEY_ID,
        		KEY_RECORD_TIME, KEY_RECORD_START, KEY_RECORD_DESTINATION, KEY_RECORD_DISTANCE, KEY_RECORD_SPEED, KEY_RECORD_FILE, KEY_RECORD_SLAT, KEY_RECORD_SLNG, KEY_RECORD_ELAT, KEY_RECORD_ELNG }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
 
 
        if (cursor != null)
            cursor.moveToFirst();
  
        data = new RecordData(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10));
	   }catch(Exception e){
		   return null;
	   }
        // return user data
        return data;
    }

   // Getting single contact
  public static OtherData getOtherData(String title) {

      // Open database for Read / Write
	  OtherData data = null;
	   try{
	       final SQLiteDatabase db = open();
	 
	       Cursor cursor = db.query(OTHER_TABLE, new String[] { KEY_ID,
	       		KEY_OTHER_TITLE, KEY_OTHER_VALUE, KEY_OTHER_OTHERFLG}, KEY_OTHER_TITLE + "=?",
	               new String[] { title }, null, null, null, null);
	
	
	       if (cursor != null){
	           cursor.moveToFirst();
	 
	           data = new OtherData(Integer.parseInt(cursor.getString(0)),
	               cursor.getString(1), cursor.getString(2), cursor.getString(3));
	       }else{
	    	   return null;
	       }
	   }catch(Exception e){
		   return null;
	   }
       // return user data
       return data;
   }
  
      // Getting All User data
    public static List<RecordData> getAllRecordData() {
 
        List<RecordData> contactList = new ArrayList<RecordData>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + RECORD_TABLE + " order by " + KEY_ID + " desc";
  
 
        // Open database for Read / Write
        final SQLiteDatabase db = open();
        Cursor cursor = db.rawQuery ( selectQuery, null );
  
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                RecordData data = new RecordData(Integer.parseInt(cursor.getString(0)), cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10));
                
 
                // Adding contact to list
                contactList.add(data);
            } while (cursor.moveToNext());
        }
  
        // return user list
        return contactList;
    }

    // Getting All User data
  public static List<OtherData> getAllOtherData() {

      List<OtherData> contactList = new ArrayList<OtherData>();
      // Select All Query
      String selectQuery = "SELECT  * FROM " + OTHER_TABLE;


      // Open database for Read / Write
      final SQLiteDatabase db = open();
      Cursor cursor = db.rawQuery ( selectQuery, null );

      // looping through all rows and adding to list
      if (cursor.moveToFirst()) {
          do {
        	  OtherData data = new OtherData(Integer.parseInt(cursor.getString(0)), cursor.getString(1),cursor.getString(2),cursor.getString(3));
              

              // Adding contact to list
              contactList.add(data);
          } while (cursor.moveToNext());
      }

      // return user list
      return contactList;
  }
 
 
  
    // Deleting single contact
    public static void deleteRecordData(RecordData data) {
        final SQLiteDatabase db = open();
        db.delete(RECORD_TABLE, KEY_ID + " = ?",
                new String[] { String.valueOf(data._id) });
        db.close();
    }
    // Deleting single contact
    public static void deleteOtherData(OtherData data) {
        final SQLiteDatabase db = open();
        db.delete(OTHER_TABLE, KEY_OTHER_TITLE + " = ?",
                new String[] { String.valueOf(data.title) });
        db.close();
    }
  
    // Getting dataCount
 
    public static int getUserDataCount() {
 
        final SQLiteDatabase db = open();
 
        String countQuery = "SELECT  * FROM " + RECORD_TABLE;
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
  
        // return count
        return cursor.getCount();
    }
 
}  // CLASS CLOSED