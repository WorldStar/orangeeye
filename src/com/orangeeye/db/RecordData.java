package com.orangeeye.db;

public class RecordData {
	//private variables
    public int _id = 0;
    public String record_time = "";
    public String record_start = "";
    public String record_destination = "";
    public String record_distance = "";
    public String record_speed = "";
    public String record_file = "";
    public String record_s_lat = "";
    public String record_s_lng = "";
    public String record_e_lat = "";
    public String record_e_lng = "";
    
 
      // Empty constructor
      public RecordData(){
 
       }


   // constructor
   public RecordData(int id, String record_time, String record_start, String record_destination, String record_distance, String record_speed, String record_file, String s_lat, String s_lng, String e_lat, String e_lng){
       this._id = id;
       this.record_time = record_time;
       this.record_start = record_start;
       this.record_destination = record_destination;
       this.record_distance = record_distance;
       this.record_speed = record_speed;
       this.record_file = record_file;
       this.record_s_lat = s_lat;
       this.record_s_lng = s_lng;
       this.record_e_lat = e_lat;
       this.record_e_lng = e_lng;
   }
 
}
