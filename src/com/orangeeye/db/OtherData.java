package com.orangeeye.db;

public class OtherData {
	//private variables
    public int _id = 0;
    public String title = "";
    public String value = "";
    public String otherflg = "";
    
 
      // Empty constructor
      public OtherData(){
 
       }


   // constructor
   public OtherData(int id, String title, String value, String otherflg){
       this._id = id;
       this.title = title;
       this.otherflg = otherflg;
       this.value = value;
   }
 
}
