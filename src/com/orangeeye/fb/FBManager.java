package com.orangeeye.fb;

import com.facebook.android.Facebook;
import com.orangeeye.R;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

public class FBManager{

	  private static final String FB_ACCESS_TOKEN = "fb_access_token";
	  private static final String FB_EXPIRES = "fb_expires";

	  private Activity context;
	  private Facebook facebookApi;

	  private Runnable successRunnable=new Runnable(){
	    @Override
	    public void run() {
	        Toast.makeText(context, "Success", Toast.LENGTH_LONG).show();
	    }
	  };    

	  public FBManager(Activity context){

	    this.context = context;

	    facebookApi = new Facebook(context.getResources().getString(R.string.app_id));

	    facebookApi.setAccessToken(restoreAccessToken());

	  } 

	  public void postOnWall(final String text, final String link){
	    new Thread(){
	        @Override
	        public void run(){
	            try {
	                Bundle parameters = new Bundle();
	                parameters.putString("message", text);
	                if(link!=null){
	                    parameters.putString("link", link);
	                }
	                String response = facebookApi.request("me/feed", parameters, "POST");
	                if(!response.equals("")){
	                    if(!response.contains("error")){
	                        context.runOnUiThread(successRunnable);                     
	                    }else{
	                        Log.e("Facebook error:", response);
	                    }
	                }
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        }
	    }.start();     
	  }


	  public void save(String access_token, long expires){
	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
	    Editor editor=prefs.edit();
	    editor.putString(FB_ACCESS_TOKEN, access_token);
	    editor.putLong(FB_EXPIRES, expires);
	    editor.commit();
	  }

	  public String restoreAccessToken(){
	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
	    return prefs.getString(FB_ACCESS_TOKEN, null);      
	  }

	  public long restoreExpires(){
	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
	    return prefs.getLong(FB_EXPIRES, 0);        
	  } 
}