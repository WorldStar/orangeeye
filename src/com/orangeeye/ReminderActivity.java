package com.orangeeye;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.orangeeye.db.DBAdapter;
import com.orangeeye.db.OtherData;
import com.orangeeye.lib.AlarmManagerBroadcastReceiver;
import com.orangeeye.lib.AlarmManagerBroadcastReceiver1;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;

import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

public class ReminderActivity extends Activity implements OnClickListener{
	Intent intent;
	private int year;
	private int month;
	private int day;
	static final int DATE_DIALOG_ID = 999;
	private DatePicker dpResult;
	OtherData setdata = null;
	OtherData setdata1 = null;
	RadioButton radio1, radio2,radio3,radio4;
	String otherflg = "0", roadflg = "0";
	String expiredate = "";
	String roaddate = "";
	int flg = 0;
	Spinner roadspinner, reminderspinner;
	private AlarmManagerBroadcastReceiver alarm;
	public boolean set1 = false;
	public boolean set2 = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reminder_screen);
        ((MyButton)findViewById(R.id.backbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.save2bt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.close2bt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.changedt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.changebt1)).setOnClickListener(this);
        roadspinner = (Spinner)findViewById(R.id.spinner1);
        roadspinner.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int pos, long arg3) {
				// TODO Auto-generated method stub
				if(set1){
					roadflg = ""+pos;
		    		if(setdata1 == null){
			    		OtherData other1 = new OtherData(0, "reminder_data1", expiredate, roadflg);
			    		DBAdapter.addOtherData(other1);
		    		}else{
			    		OtherData other1 = new OtherData(setdata1._id, "reminder_data1", expiredate, roadflg);
			    		DBAdapter.updateOtherData(other1);
		    		}
		    		if(roadflg != null && !roadflg.equals("")){
		            	roadAlarm(roadflg);
		            }
				}
				set1 = true;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
        	
        });
        reminderspinner = (Spinner)findViewById(R.id.spinner2);
        reminderspinner.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int pos, long arg3) {
				// TODO Auto-generated method stub
				if(set2){
					otherflg = ""+pos;
		    		if(setdata == null){
			    		OtherData other1 = new OtherData(0, "reminder_data", expiredate, otherflg);
			    		DBAdapter.addOtherData(other1);
		    		}else{
			    		OtherData other1 = new OtherData(setdata._id, "reminder_data", expiredate, otherflg);
			    		DBAdapter.updateOtherData(other1);
		    		}
		    		if(otherflg != null && !otherflg.equals("")){
		            	reminderAlarm(otherflg);
		            }
				}
				set2 = true;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
        	
        });
        /*radio1 = ((RadioButton)findViewById(R.id.radio1));
        radio2 = ((RadioButton)findViewById(R.id.radio2));
        radio3 = ((RadioButton)findViewById(R.id.radio3));
        radio4 = ((RadioButton)findViewById(R.id.radio4));
        radio1.setOnClickListener(this);
        radio2.setOnClickListener(this);
        radio3.setOnClickListener(this);
        radio4.setOnClickListener(this);*/
        dpResult = (DatePicker) findViewById(R.id.mdate);
        DBAdapter.init(this);
        setdata = DBAdapter.getOtherData("reminder_data");
        setdata1 = DBAdapter.getOtherData("reminder_data1");
        
        if(setdata == null){
	        final Calendar c = Calendar.getInstance();
			year = c.get(Calendar.YEAR);
			month = c.get(Calendar.MONTH);
			day = c.get(Calendar.DAY_OF_MONTH);
			dpResult.init(year, month, day, null);
			((MyTextView)findViewById(R.id.datetx)).setText("");
        }else{
        	String date = setdata.value;
			expiredate = date;
        	String reminder =  setdata.otherflg;
    		otherflg = reminder;
        	if(!reminder.equals("")){
        		int remind = Integer.parseInt(reminder);
        		setReminder(remind);
        	}
        	if(!date.equals("")){
        		SimpleDateFormat fmt = new SimpleDateFormat("dd MMMM yyyy");
        		try {
					Date d = fmt.parse(date);
	        		SimpleDateFormat fmt1 = new SimpleDateFormat("dd MMMM yyyy");
	        		String ss = fmt1.format(d);
					((MyTextView)findViewById(R.id.datetx)).setText(ss.substring(0, ss.length()-4));
					Calendar c = Calendar.getInstance();
					c.setTime(d);
	    			dpResult.init(c.get(Calendar.YEAR),c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), null);
	    			//dpResult.init(d.getYear(), d.getMonth(), d.getDay(), null);
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
	        		final Calendar c = Calendar.getInstance();
	    			year = c.get(Calendar.YEAR);
	    			month = c.get(Calendar.MONTH);
	    			day = c.get(Calendar.DAY_OF_MONTH);
	    			dpResult.init(year, month, day, null);
					((MyTextView)findViewById(R.id.datetx)).setText("");
				}
        	}else{
        		final Calendar c = Calendar.getInstance();
    			year = c.get(Calendar.YEAR);
    			month = c.get(Calendar.MONTH);
    			day = c.get(Calendar.DAY_OF_MONTH);
    			dpResult.init(year, month, day, null);
				((MyTextView)findViewById(R.id.datetx)).setText("");
        	}
        }
        if(setdata1 == null){
			((MyTextView)findViewById(R.id.datetx1)).setText("");
		}else{
			String date = setdata1.value;
			roaddate = date;
			String road =  setdata1.otherflg;
    		roadflg = road;
        	if(!road.equals("")){
        		int remind = Integer.parseInt(road);
        		setRoad(remind);
        	}
			if(!date.equals("")){
        		SimpleDateFormat fmt = new SimpleDateFormat("dd MMMM yyyy");
        		try {
					Date d = fmt.parse(date);
	        		SimpleDateFormat fmt1 = new SimpleDateFormat("dd MMMM yyyy");
	        		String ss = fmt1.format(d);
					((MyTextView)findViewById(R.id.datetx1)).setText(ss.substring(0, ss.length()-4));
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					((MyTextView)findViewById(R.id.datetx1)).setText("");
				}
        	}else{
				((MyTextView)findViewById(R.id.datetx1)).setText("");
        	}
		}
        try {        	
            Field f[] = dpResult.getClass().getDeclaredFields();
            for (Field field : f) {
                if (field.getName().equals("mYearDecrementButton")) {
                    field.setAccessible(true);
                    Object yearPicker = new Object();
                    yearPicker = field.get(dpResult);
                    ((View) yearPicker).setVisibility(View.GONE);
                }else if (field.getName().equals("mYearIncrementButton")) {
                    field.setAccessible(true);
                    Object yearPicker = new Object();
                    yearPicker = field.get(dpResult);
                    ((View) yearPicker).setVisibility(View.GONE);
                }else if (field.getName().equals("mYearSpinnerInput")) {
                    field.setAccessible(true);
                    Object yearPicker = new Object();
                    yearPicker = field.get(dpResult);
                    ((View) yearPicker).setVisibility(View.GONE);
                }else if (field.getName().equals("mYearSpinner")) {
                    field.setAccessible(true);
                    Object yearPicker = new Object();
                    yearPicker = field.get(dpResult);
                    ((View) yearPicker).setVisibility(View.GONE);
                }
            }
        } 
        catch (Exception e) {
           
        } 
        //cancelOneTimer1();
        //oneAlarmTimer1(System.currentTimeMillis() + 5000);
        //oneAlarmTimer(System.currentTimeMillis() + 5000);
        //cancelOneTimer();
    }
    public void reminderAlarm(String flg){
        alarm = new AlarmManagerBroadcastReceiver();
    	try{
    		int val = Integer.parseInt(flg);
            cancelOneTimer();
    		switch(val){
    		case 0:
    			//cancelOneTimer();
    			break;
    		case 1:
    			calAlarmReminder(1);
    			break;
    		case 2:
    			calAlarmReminder(2);
    			break;
    		case 3:
    			calAlarmReminder(3);
    			break;
    		case 4:
    			calAlarmReminder(4);
    			break;
    		}
    	}catch(Exception e){
    		
    	}
    }

    public void reminderAlarm3(String flg){
        alarm = new AlarmManagerBroadcastReceiver();
    	try{
    		int val = Integer.parseInt(flg);
            cancelOneTimer();
    		switch(val){
    		case 0:
    			//cancelOneTimer();
    			break;
    		case 1:
    			calAlarmReminder2(1);
    			break;
    		case 2:
    			calAlarmReminder2(2);
    			break;
    		case 3:
    			calAlarmReminder2(3);
    			break;
    		case 4:
    			calAlarmReminder2(4);
    			break;
    		}
    	}catch(Exception e){
    		
    	}
    }
    public void roadAlarm(String flg){
        alarm = new AlarmManagerBroadcastReceiver();
    	try{
    		int val = Integer.parseInt(flg);
    		int val1 = Integer.parseInt(otherflg);
            cancelOneTimer1();
    		switch(val){
    		case 0:
    			calAlarmRoad(0, val1);
    			break;
    		case 1:
    			calAlarmRoad(1, val1);
    			break;
    		}
    	}catch(Exception e){
    		
    	}
    }
    public void calAlarmRoad(int val, int val1){
    	SimpleDateFormat fmt = new SimpleDateFormat("dd MMMM yyyy");
		SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(this);
        Editor editor = sharedPreferences.edit();
		try {
			Date d = fmt.parse(roaddate);
			long roadtime = 0;
			Calendar c = Calendar.getInstance();
			Date dd = d;
			if(val1 != 0){
				switch(val){
	    		case 0:
	    			switch(val1){
	        		case 1:
	        			dd.setMonth(d.getMonth() - 2);
	        			dd.setHours(12);     			
	        			dd.setMinutes(0);    
	        			dd.setSeconds(0);
	        			roadtime = dd.getTime();
	        	        editor.putString("set_month","2 months");
	        	        editor.commit();

	        			break;
	        		case 2:
	        			dd.setMonth(d.getMonth() - 1);   
	        			dd.setHours(12);     			
	        			dd.setMinutes(0);    
	        			dd.setSeconds(0);  			
	        			roadtime = dd.getTime();
	        	        editor.putString("set_month","1 month");
	        	        editor.commit();
	        			break;
	        		case 3:
	        			dd.setMonth(d.getMonth());  
	        			dd.setDate(d.getDate()-14);	
	        			dd.setHours(12);     			
	        			dd.setMinutes(0);    
	        			dd.setSeconds(0);
	        			roadtime = dd.getTime();
	        	        editor.putString("set_month","2 weeks");
	        	        editor.commit();
	        			break;
	        		case 4:
	        			dd.setMonth(d.getMonth());  
	        			dd.setDate(d.getDate()-7);	
	        			dd.setHours(12);     			
	        			dd.setMinutes(0);    
	        			dd.setSeconds(0);
	        			roadtime = dd.getTime();
	        	        editor.putString("set_month","1 week");
	        	        editor.commit();
	        			break;
	        		}
	    			break;
	    		case 1:
	    			switch(val1){
	        		case 1:
	        			dd.setMonth(d.getMonth() - 2);    	
	        			dd.setHours(12);     			
	        			dd.setMinutes(0);    
	        			dd.setSeconds(0);		
	        			roadtime = dd.getTime();
	        	        editor.putString("set_month","2 months");
	        	        editor.commit();
	        			break;
	        		case 2:
	        			dd.setMonth(d.getMonth() - 1);     			
	        			roadtime = dd.getTime();	
	        			dd.setHours(1);
	        	        editor.putString("set_month","1 month");
	        	        editor.commit();
	        			break;
	        		case 3:
	        			dd.setMonth(d.getMonth());  
	        			dd.setDate(d.getDate()-14);	
	        			dd.setHours(12);     			
	        			dd.setMinutes(0);    
	        			dd.setSeconds(0);
	        			roadtime = dd.getTime();
	        	        editor.putString("set_month","2 weeks");
	        	        editor.commit();
	        			break;
	        		case 4:
	        			dd.setMonth(d.getMonth());  
	        			dd.setDate(d.getDate()-7);	
	        			dd.setHours(12);     			
	        			dd.setMinutes(0);    
	        			dd.setSeconds(0);
	        			roadtime = dd.getTime();
	        	        editor.putString("set_month","1 week");
	        	        editor.commit();
	        			break;
	        		}
	    			break;
	    		}
				switch(val){
				case 0:
					Date dd1 = new Date();
					dd1.setTime(roadtime);
					dd1.setMonth(dd1.getMonth() + 6);
        			dd1.setHours(12);     			
        			dd1.setMinutes(0);    
        			dd1.setSeconds(0);
					oneAlarmTimer1(roadtime, dd1.getTime());
					break;
				case 1:
					Date dd2 = new Date();
					dd2.setTime(roadtime);
					dd2.setMonth(dd2.getMonth() + 12);
        			dd2.setHours(12);     			
        			dd2.setMinutes(0);    
        			dd2.setSeconds(0);
					oneAlarmTimer1(roadtime, dd2.getTime());
					break;
				}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    public void calAlarmReminder(int val){
		SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(this);
        Editor editor = sharedPreferences.edit();
    	SimpleDateFormat fmt = new SimpleDateFormat("dd MMMM yyyy");
		try {
			Date d = fmt.parse(expiredate);
			long expiretime = 0;
			Date dd = d;
			switch(val){
    		case 1:
    			dd.setMonth(d.getMonth() - 2);  
    			dd.setHours(12);     			
    			dd.setMinutes(10);    
    			dd.setSeconds(0);  			
    			expiretime = dd.getTime();
    	        editor.putString("set_month","2 months");
    	        editor.commit();
    			break;
    		case 2:
    			dd.setMonth(d.getMonth() - 1);    
    			dd.setHours(12);     			
    			dd.setMinutes(10);    
    			dd.setSeconds(0);  			 			
    			expiretime = dd.getTime();
    	        editor.putString("set_month","1 month");
    	        editor.commit();
    			break;
    		case 3:    		
    			dd.setDate(d.getDate()-14);	 
    			dd.setHours(12);     			
    			dd.setMinutes(10);    
    			dd.setSeconds(0);  			
    			expiretime = dd.getTime();
    	        editor.putString("set_month","2 weeks");
    	        editor.commit();
    			break;
    		case 4:
    			dd.setDate(d.getDate()-7);	 
    			dd.setHours(12);     			
    			dd.setMinutes(10);    
    			dd.setSeconds(0);  			
    			expiretime = dd.getTime();
    	        editor.putString("set_month","1 week");
    	        editor.commit();
    			break;
    		}
			if(expiretime > 0){

				Date dd2 = new Date();
				dd2.setTime(expiretime);
				dd2.setMonth(dd2.getMonth() + 12); 
    			dd2.setHours(12);     			
    			dd2.setMinutes(10);    
    			dd2.setSeconds(0);  			
				oneAlarmTimer(expiretime, dd2.getTime());
			}
			if(roadflg != null && !roadflg.equals("")){
            	roadAlarm(roadflg);
            }
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    public void calAlarmReminder2(int val){
		SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(this);
        Editor editor = sharedPreferences.edit();
    	SimpleDateFormat fmt = new SimpleDateFormat("dd MMMM yyyy");
		try {
			Date d = fmt.parse(expiredate);
			long expiretime = 0;
			Date dd = d;
			switch(val){
    		case 1:
    			dd.setMonth(d.getMonth() - 2);    
    			dd.setHours(12);     			
    			dd.setMinutes(10);    
    			dd.setSeconds(0);  			 			
    			expiretime = dd.getTime();
    	        editor.putString("set_month","2 months");
    	        editor.commit();
    			break;
    		case 2:
    			dd.setMonth(d.getMonth() - 1);   
    			dd.setHours(12);     			
    			dd.setMinutes(10);    
    			dd.setSeconds(0);  			  			
    			expiretime = dd.getTime();
    	        editor.putString("set_month","1 month");
    	        editor.commit();
    			break;
    		case 3:    		
    			dd.setDate(d.getDate()-14);	 
    			dd.setHours(12);     			
    			dd.setMinutes(10);    
    			dd.setSeconds(0);  			
    			expiretime = dd.getTime();
    	        editor.putString("set_month","2 weeks");
    	        editor.commit();
    			break;
    		case 4:
    			dd.setDate(d.getDate()-7);	 
    			dd.setHours(12);     			
    			dd.setMinutes(10);    
    			dd.setSeconds(0);  			
    			expiretime = dd.getTime();
    	        editor.putString("set_month","1 week");
    	        editor.commit();
    			break;
    		}
			if(expiretime > 0){

				Date dd2 = new Date();
				dd2.setTime(expiretime);
				dd2.setMonth(dd2.getMonth() + 12); 
    			dd2.setHours(12);     			
    			dd2.setMinutes(10);    
    			dd2.setSeconds(0);  			
				oneAlarmTimer(expiretime, dd2.getTime());
			}
			//if(roadflg != null && !roadflg.equals("")){
            	//roadAlarm(roadflg);
            //}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    public void oneAlarmTimer(long time, long time1){
    	Context context = this.getApplicationContext();
    	if(alarm != null){
    		alarm.setOnetimeTimer(context, time, time1);
    	}else{
    		//Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
    	}
    }
    public void oneAlarmTimer1(long time, long val){
    	Context context = this.getApplicationContext();
    	if(alarm != null){
    		alarm.setOnetimeTimer1(context, time, val);
    	}else{
    		//Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
    	}
    }
    public void cancelOneTimer(){
    	Context context = this.getApplicationContext();
    	if(alarm != null){
    		alarm.CancelAlarm(context);
    	}else{
    		//Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
    	}
    }
    public void cancelOneTimer1(){
    	Context context = this.getApplicationContext();
    	if(alarm != null){
    		alarm.CancelAlarm1(context);
    	}else{
    		//Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
    	}
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.backbt:
			intent = new Intent(ReminderActivity.this, SettingsActivity.class);
        	startActivity(intent);
        	finish();
			break;
		case R.id.changedt:
			flg = 1;
			((LinearLayout)findViewById(R.id.date_set_layout)).setVisibility(View.VISIBLE);
			if(setdata == null){
		        final Calendar c = Calendar.getInstance();
				year = c.get(Calendar.YEAR);
				month = c.get(Calendar.MONTH);
				day = c.get(Calendar.DAY_OF_MONTH);
				dpResult.init(year, month, day, null);
				((MyTextView)findViewById(R.id.datetx)).setText("");
	        }else{
	        	String date = expiredate;
	        	if(!date.equals("")){
	        		SimpleDateFormat fmt = new SimpleDateFormat("dd MMMM yyyy");
	        		try {
						Date d = fmt.parse(date);
						((MyTextView)findViewById(R.id.datetx)).setText(expiredate.substring(0, expiredate.length()-4));
						Calendar c = Calendar.getInstance();
						c.setTime(d);
		    			dpResult.init(c.get(Calendar.YEAR),c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), null);
						
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
		        		final Calendar c = Calendar.getInstance();
		    			year = c.get(Calendar.YEAR);
		    			month = c.get(Calendar.MONTH);
		    			day = c.get(Calendar.DAY_OF_MONTH);
		    			dpResult.init(year, month, day, null);
						((MyTextView)findViewById(R.id.datetx)).setText("");
					}
	        	}else{
	        		final Calendar c = Calendar.getInstance();
	    			year = c.get(Calendar.YEAR);
	    			month = c.get(Calendar.MONTH);
	    			day = c.get(Calendar.DAY_OF_MONTH);
	    			dpResult.init(year, month, day, null);
					((MyTextView)findViewById(R.id.datetx)).setText("");
	        	}
	        }
			break;
		case R.id.changebt1:
			flg = 2;
			((LinearLayout)findViewById(R.id.date_set_layout)).setVisibility(View.VISIBLE);
			if(setdata1 == null){
				final Calendar c = Calendar.getInstance();
    			year = c.get(Calendar.YEAR);
    			month = c.get(Calendar.MONTH);
    			day = c.get(Calendar.DAY_OF_MONTH);
    			dpResult.init(year, month, day, null);
				((MyTextView)findViewById(R.id.datetx1)).setText("");
			}else{
				String date = roaddate;
				if(!date.equals("")){
	        		SimpleDateFormat fmt = new SimpleDateFormat("dd MMMM yyyy");
	        		try {
						Date d = fmt.parse(date);
						((MyTextView)findViewById(R.id.datetx1)).setText(roaddate.substring(0, roaddate.length()-4));
						Calendar c = Calendar.getInstance();
						c.setTime(d);
		    			dpResult.init(c.get(Calendar.YEAR),c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), null);
		    			//dpResult.init(d.getYear(), d.getMonth(), d.getDay(), null);
						
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
		        		final Calendar c = Calendar.getInstance();
		    			year = c.get(Calendar.YEAR);
		    			month = c.get(Calendar.MONTH);
		    			day = c.get(Calendar.DAY_OF_MONTH);
		    			dpResult.init(year, month, day, null);
						((MyTextView)findViewById(R.id.datetx1)).setText("");
					}
	        	}else{
	        		final Calendar c = Calendar.getInstance();
	    			year = c.get(Calendar.YEAR);
	    			month = c.get(Calendar.MONTH);
	    			day = c.get(Calendar.DAY_OF_MONTH);
	    			dpResult.init(year, month, day, null);
					((MyTextView)findViewById(R.id.datetx1)).setText("");
	        	}
			}
			break;
		case R.id.save2bt:
			if(flg == 1){
				((LinearLayout)findViewById(R.id.date_set_layout)).setVisibility(View.GONE);
				int y = dpResult.getYear();
				int m = dpResult.getMonth();
				int d = dpResult.getDayOfMonth();
				Calendar c = Calendar.getInstance();
				c.set(y, m, d);
				Date dd = c.getTime();
	    		SimpleDateFormat fmt = new SimpleDateFormat("dd MMMM yyyy");
	    		String s = fmt.format(dd);
	    		expiredate = s;
	    		((MyTextView)findViewById(R.id.datetx)).setText(expiredate.substring(0, expiredate.length()-4));
	    		if(setdata == null){
		    		OtherData other = new OtherData(0, "reminder_data", expiredate, otherflg);
		    		DBAdapter.addOtherData(other);
	    		}else{
		    		OtherData other = new OtherData(setdata._id, "reminder_data", expiredate, otherflg);
		    		DBAdapter.updateOtherData(other);
	    		}
	    		if(otherflg != null && !otherflg.equals("")){
	            	reminderAlarm3(otherflg);
	            }
			}else if(flg == 2){
				((LinearLayout)findViewById(R.id.date_set_layout)).setVisibility(View.GONE);
				int y = dpResult.getYear();
				int m = dpResult.getMonth();
				int d = dpResult.getDayOfMonth();
				Calendar c = Calendar.getInstance();
				c.set(y, m, d);
				Date dd = c.getTime();
	    		SimpleDateFormat fmt = new SimpleDateFormat("dd MMMM yyyy");
	    		String s = fmt.format(dd);
	    		roaddate = s;
	    		((MyTextView)findViewById(R.id.datetx1)).setText(roaddate.substring(0, roaddate.length()-4));
	    		if(setdata1 == null){
		    		OtherData other = new OtherData(0, "reminder_data1", roaddate, "");
		    		DBAdapter.addOtherData(other);
	    		}else{
		    		OtherData other = new OtherData(setdata1._id, "reminder_data1", roaddate, roadflg);
		    		DBAdapter.updateOtherData(other);
	    		}
	    		if(roadflg != null && !roadflg.equals("")){
	            	roadAlarm(roadflg);
	            }
			}
			break;
		case R.id.close2bt:
			((LinearLayout)findViewById(R.id.date_set_layout)).setVisibility(View.GONE);
			break;
		/*case R.id.radio1:
    		otherflg = "1";
    		setReminder(1);
    		if(setdata == null){
	    		OtherData other1 = new OtherData(0, "reminder_data", expiredate, otherflg);
	    		DBAdapter.addOtherData(other1);
    		}else{
	    		OtherData other1 = new OtherData(setdata._id, "reminder_data", expiredate, otherflg);
	    		DBAdapter.updateOtherData(other1);
    		}
			break;
		case R.id.radio2:
    		otherflg = "2";
    		setReminder(2);
    		if(setdata == null){
	    		OtherData other1 = new OtherData(0, "reminder_data", expiredate, otherflg);
	    		DBAdapter.addOtherData(other1);
    		}else{
	    		OtherData other2 = new OtherData(setdata._id, "reminder_data", expiredate, otherflg);
	    		DBAdapter.updateOtherData(other2);
    		}
			break;
		case R.id.radio3:
    		otherflg = "3";
    		setReminder(3);
    		if(setdata == null){
	    		OtherData other1 = new OtherData(0, "reminder_data", expiredate, otherflg);
	    		DBAdapter.addOtherData(other1);
    		}else{
	    		OtherData other3 = new OtherData(setdata._id, "reminder_data", expiredate, otherflg);
	    		DBAdapter.updateOtherData(other3);
    		}
			break;
		case R.id.radio4:
    		otherflg = "4";
    		setReminder(4);
    		if(setdata == null){
	    		OtherData other1 = new OtherData(0, "reminder_data", expiredate, otherflg);
	    		DBAdapter.addOtherData(other1);
    		}else{
	    		OtherData other4 = new OtherData(setdata._id, "reminder_data", expiredate, otherflg);
	    		DBAdapter.updateOtherData(other4);
    		}
			break;*/
		}
		
	}
	public void setReminder(int no){
		reminderspinner.setSelection(no);
		
	}
	public void setRoad(int no){
		roadspinner.setSelection(no);
		
	}
	 @Override
	 public void onBackPressed() {
		Intent intent3 = new Intent(ReminderActivity.this, SettingsActivity.class);
		startActivity(intent3);
		finish();
	 }
    
}
