package com.orangeeye;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.model.LatLng;
import com.orangeeye.NavigationActivity.ParkAdapter;
import com.orangeeye.data.DealData;
import com.orangeeye.data.SearchAddr;
import com.orangeeye.data.busStopCodeSet;
import com.orangeeye.lib.CheckInternetAccess;
import com.orangeeye.lib.GetSearchLocation;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;

public class DealsActivity  extends Activity implements OnClickListener{
	Intent intent;
	WebView detailview;
	RelativeLayout detail_layout;
	ArrayList<DealData> deallist = new ArrayList<DealData>();
	public int pos = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deals_screen);
        ((MyButton)findViewById(R.id.backbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.leftbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.rightbt)).setOnClickListener(this);
        ((LinearLayout)findViewById(R.id.image1_layout)).setOnClickListener(this);
        ((LinearLayout)findViewById(R.id.image2_layout)).setOnClickListener(this);
        ((ImageView)findViewById(R.id.imageView3)).setOnClickListener(this);
        detailview = (WebView)findViewById(R.id.webView1);
        detail_layout = (RelativeLayout)findViewById(R.id.detail_layout);
        detail_layout.setVisibility(View.GONE);
        new GetImageTask().execute();
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.backbt:
			intent = new Intent(DealsActivity.this, HomeActivity.class);
        	startActivity(intent);
        	finish();
			break;
		case R.id.leftbt:			
			if(pos-2 < 0)break;
			if(pos-2 > deallist.size()-1)break;
			pos = pos - 2;
			displaydata();
			break;
		case R.id.rightbt:		
			if(pos+2 < 0)break;
			if(pos+2 > deallist.size()-1)break;
			pos = pos + 2;
			displaydata();
			break;
		case R.id.image1_layout:
			if(pos < 0)break;
			if(pos > deallist.size()-1)break;
			detailview.getSettings().setJavaScriptEnabled(true);
			detailview.loadUrl(deallist.get(pos).detailurl);
	        detail_layout.setVisibility(View.VISIBLE);
			break;
		case R.id.image2_layout:
			if(pos+1 < 0)break;
			if(pos+1 > deallist.size()-1)break;
			detailview.getSettings().setJavaScriptEnabled(true);
			detailview.loadUrl(deallist.get(pos+1).detailurl);
	        detail_layout.setVisibility(View.VISIBLE);
			break;
		case R.id.imageView3:
	        detail_layout.setVisibility(View.GONE);
			break;
		}
	}
	public void displaydata(){
		((LinearLayout)findViewById(R.id.image1_layout)).setVisibility(View.VISIBLE);
		((LinearLayout)findViewById(R.id.image2_layout)).setVisibility(View.VISIBLE);
		if(pos < 0){
			((LinearLayout)findViewById(R.id.image1_layout)).setVisibility(View.GONE);
			((LinearLayout)findViewById(R.id.image2_layout)).setVisibility(View.GONE);
			return;
		}
		if(pos > deallist.size()-1){
			((LinearLayout)findViewById(R.id.image1_layout)).setVisibility(View.GONE);
			((LinearLayout)findViewById(R.id.image2_layout)).setVisibility(View.GONE);
			return;
		}
		new GetBackGroundTask1().execute();

		if(pos+1 < 0){
			((LinearLayout)findViewById(R.id.image2_layout)).setVisibility(View.GONE);
			return;
		}
		if(pos+1 > deallist.size()-1){
			((LinearLayout)findViewById(R.id.image2_layout)).setVisibility(View.GONE);
			return;
		}
		new GetBackGroundTask2().execute();
	}

	// Retrieve voucher web service async task
		private class GetBackGroundTask1 extends
		AsyncTask<Void, Void, Bitmap> {
			ProgressDialog MyDialog;
	       public GetBackGroundTask1() {
	       }
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				 MyDialog = ProgressDialog.show(DealsActivity.this, "",
		                    "Loading... ", false);
		            MyDialog.setCancelable(false);
			}
			@Override
			protected void onPostExecute(Bitmap _result) {
				super.onPostExecute(_result);
				MyDialog.dismiss();
				if(_result != null){
					//((ImageView)findViewById(R.id.imageView1)).setImageBitmap(_result);
					BitmapDrawable drawable = new BitmapDrawable(_result);
					int height = ((LinearLayout)findViewById(R.id.image1_layout)).getHeight();
					int w = _result.getWidth();
					int h = _result.getHeight();
					int width = height * w / h;
					((LinearLayout)findViewById(R.id.image1_layout)).setBackgroundDrawable(drawable);
					LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height);
					((LinearLayout)findViewById(R.id.image1_layout)).setLayoutParams(params);
					((MyTextView)findViewById(R.id.title1)).setText(deallist.get(pos).title);
				}
			}
			@Override
			protected Bitmap doInBackground(Void... params) {	
				CheckInternetAccess check = new CheckInternetAccess(DealsActivity.this);
				Bitmap bitmap = null;
		        if (check.checkNetwork()) {
		        	bitmap = DownloadBitmap(deallist.get(pos).imgurl);
		        }
		        return bitmap;
			}
		}

		// Retrieve voucher web service async task
	private class GetBackGroundTask2 extends
	AsyncTask<Void, Void, Bitmap> {
		ProgressDialog MyDialog;
       public GetBackGroundTask2() {
       }
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			 MyDialog = ProgressDialog.show(DealsActivity.this, "",
	                    "Loading... ", false);
	            MyDialog.setCancelable(false);
		}
		@Override
		protected void onPostExecute(Bitmap _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			if(_result != null){
				//((ImageView)findViewById(R.id.imageView2)).setImageBitmap(_result);
				BitmapDrawable drawable = new BitmapDrawable(_result);
				int height = ((LinearLayout)findViewById(R.id.image2_layout)).getHeight();
				int w = _result.getWidth();
				int h = _result.getHeight();
				int width = height * w / h;
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height);
				((LinearLayout)findViewById(R.id.image2_layout)).setLayoutParams(params);
				((LinearLayout)findViewById(R.id.image2_layout)).setBackgroundDrawable(drawable);
				
				((MyTextView)findViewById(R.id.title2)).setText(deallist.get(pos+1).title);
			}
		}
		@Override
		protected Bitmap doInBackground(Void... params) {	
			CheckInternetAccess check = new CheckInternetAccess(DealsActivity.this);
			Bitmap bitmap = null;
	        if (check.checkNetwork()) {
	        	bitmap = DownloadBitmap(deallist.get(pos+1).imgurl);
	        }
	        return bitmap;
		}
	}

	public Bitmap DownloadBitmap(String url) {
		Bitmap bitmapFromUrl = null;
		URL newurl;

		try {
			Log.d("Downloading", url);
			newurl = new URL(url);

			HttpGet httpRequest = null;
			try {
				httpRequest = new HttpGet(newurl.toURI());
				HttpClient httpclient = new DefaultHttpClient();
				HttpResponse response = (HttpResponse) httpclient
						.execute(httpRequest);
				HttpEntity entity = response.getEntity();
				BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(
						entity);
				InputStream instream = bufHttpEntity.getContent();
				
				bitmapFromUrl = BitmapFactory.decodeStream(instream);
				instream.close();

			} catch (URISyntaxException e) {
				Log.d("Download Error URL", url);
				//bitmapFromUrl = BitmapFactory.decodeResource(
						//DealsActivity.this.getResources(), R.drawable.item_bg);
				e.printStackTrace();
			} catch (Exception e) {
				Log.d("Download Error Exception", url);
				//bitmapFromUrl = BitmapFactory.decodeResource(
						//DealsActivity.this.getResources(), R.drawable.item_bg);
				e.printStackTrace();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return bitmapFromUrl;
	}
	public static Bitmap getBitmapFromURL(String src)  {
	    try {
	        URL url = new URL(src);
	        //HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setDoInput(true);
	        connection.setConnectTimeout(15000);
	        connection.setReadTimeout(15000);
	        connection.connect();
	        InputStream input = connection.getInputStream();
	        
	        BitmapFactory.Options options = new BitmapFactory.Options();
	        
	        Bitmap myBitmap = BitmapFactory.decodeStream(input,null,options);
	        
	        
	        return myBitmap;
	    }catch (IOException e) {
	       
	        return null;
	    }catch(OutOfMemoryError error){
	        return null;
	    }
	    
	   
	}
	// Retrieve voucher web service async task
		private class GetImageTask extends
		AsyncTask<Void, Void, List<DealData>> {
			ProgressDialog MyDialog;
	       public GetImageTask() {
	       }
			@Override
			protected void onPostExecute(List<DealData> _result) {
				super.onPostExecute(_result);
				MyDialog.dismiss();
				if(_result.size() > 0){
					if(_result.size() > 1){
						pos = 0;
						displaydata();
					}
					//search_list_layout.setVisibility(View.GONE);
				}else{
					//ReturnMessage.showAlartDialog(NavigationActivity.this, "No parking available");
				}
			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				 MyDialog = ProgressDialog.show(DealsActivity.this, "",
		                    "Loading... ", false);
		            MyDialog.setCancelable(false);
			}

			@Override
			protected List<DealData> doInBackground(Void... params) {	
				
				ArrayList<DealData> result = new ArrayList<DealData>();
				CheckInternetAccess check = new CheckInternetAccess(DealsActivity.this);
		        if (check.checkNetwork()) {
					GetSearchLocation getaddress = new GetSearchLocation();
					//getaddress.getCountryCode(point1);
					try{
						JSONObject data = getaddress.getLocationInfo3();
					
						JSONArray arraylist = (JSONArray)data.get("promotions");
						for(int i = 0; i < arraylist.length(); i++){					
							DealData add = new DealData();
							add.imgurl = arraylist.getJSONObject(i).getString("image");
							add.title = arraylist.getJSONObject(i).getString("title");
							add.detailurl = arraylist.getJSONObject(i).getString("details");
							
							result.add(add);
						}
					}catch(Exception e){
						
					}
					deallist.addAll(result);
		        }
				return deallist;
			}
		}

	 @Override
	 public void onBackPressed() {
		Intent intent3 = new Intent(DealsActivity.this, HomeActivity.class);
		startActivity(intent3);
		finish();
	 }
    
}
