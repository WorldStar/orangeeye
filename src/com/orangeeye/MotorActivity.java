package com.orangeeye;

import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

public class MotorActivity extends Activity implements OnClickListener{
	WebView webview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.motor_screen);
        ((MyButton)findViewById(R.id.backbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.callbt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.closebt)).setOnClickListener(this);
        ((MyButton)findViewById(R.id.setbt)).setOnClickListener(this);
        ((LinearLayout)findViewById(R.id.important_layout)).setOnClickListener(this);
        ((MyTextView)findViewById(R.id.link1tx)).setMovementMethod(LinkMovementMethod.getInstance());
        Linkify.addLinks(((MyTextView)findViewById(R.id.link1tx)), Linkify.ALL);
        
        ((MyTextView)findViewById(R.id.link2tx)).setMovementMethod(LinkMovementMethod.getInstance());
        Linkify.addLinks(((MyTextView)findViewById(R.id.link2tx)), Linkify.ALL);
        
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.backbt:
			Intent intent = new Intent(MotorActivity.this, HomeActivity.class);
        	startActivity(intent);
        	finish();
			break;
		case R.id.setbt:
			Intent intent3 = new Intent(MotorActivity.this, ReminderActivity.class);
        	startActivity(intent3);
			break;
		case R.id.important_layout:
			((LinearLayout)findViewById(R.id.notes_layout)).setVisibility(View.VISIBLE);
			break;
		case R.id.closebt:
			((LinearLayout)findViewById(R.id.notes_layout)).setVisibility(View.GONE);
			break;
		case R.id.callbt:
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        callIntent.setData(Uri.parse("tel:67899595"));
	        startActivity(callIntent);
			break;
		}
	}

	 public class URLSpanNoUnderline extends URLSpan {
		    public URLSpanNoUnderline(String url) {
		        super(url);
		    }
		    @Override public void updateDrawState(TextPaint ds) {
	        super.updateDrawState(ds);
	        ds.setUnderlineText(false);
	        }
	 }
	 @Override
	 public void onBackPressed() {
		Intent intent3 = new Intent(MotorActivity.this, HomeActivity.class);
     	startActivity(intent3);
		finish();
	 }
}
