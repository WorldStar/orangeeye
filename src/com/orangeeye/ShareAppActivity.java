	package com.orangeeye;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.Request;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.Facebook.DialogListener;
import com.orangeeye.ShareOnFacebook.LoginDialogListener;
import com.orangeeye.fb.FBManager;
import com.orangeeye.lib.CheckInternetAccess;
import com.orangeeye.lib.ReturnMessage;
import com.orangeeye.textview.MyButton;
import com.orangeeye.textview.MyTextView;
import com.orangeeye.utils.BaseDialogListener;
import com.orangeeye.utils.BaseRequestListener;
import com.orangeeye.utils.SessionEvents;
import com.orangeeye.utils.SessionEvents.AuthListener;
import com.orangeeye.utils.SessionEvents.LogoutListener;
import com.orangeeye.utils.SessionStore;
import com.orangeeye.utils.UpdateStatusResultDialog;
import com.orangeeye.utils.Utility;
import com.orangeeye.utils.UpdateStatusResultDialog;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.Toast;

public class ShareAppActivity extends Activity implements OnClickListener{
	Intent intent;
	String s = "";
    private Handler mHandler;
    private SessionListener mSessionListener = new SessionListener();
    final static int AUTHORIZE_ACTIVITY_RESULT_CODE = 0;
    Facebook facebook;
    Facebook facebookClient;    
    SharedPreferences mPrefs;
    String[] permissions = { "offline_access", "publish_stream", "user_photos", "publish_checkins",
    "photo_upload" };
    private static final String[] PERMISSIONS = new String[] {"publish_stream"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.share_screen);
        ((MyButton)findViewById(R.id.backbt)).setOnClickListener(this);
        ((LinearLayout)findViewById(R.id.setting_layout_1)).setOnClickListener(this);
        ((LinearLayout)findViewById(R.id.setting_layout_2)).setOnClickListener(this);
        ((LinearLayout)findViewById(R.id.setting_layout_3)).setOnClickListener(this);
        ((LinearLayout)findViewById(R.id.setting_layout_4)).setOnClickListener(this);
        ((LinearLayout)findViewById(R.id.setting_layout_5)).setOnClickListener(this);
        s = "Check out Orange Eye by NTUC Income http://www.income.com.sg � it�s an in-car camera and more. With its accident detection and emergency call function, video footage sharing and policy renewal alerts, you may like it as much as I do!";
        //Utility.mFacebook = new Facebook(getResources().getString(R.string.app_id));
        //Utility.mAsyncRunner = new AsyncFacebookRunner(Utility.mFacebook);
       // SessionStore.restore(Utility.mFacebook, this);
        //SessionEvents.addAuthListener(new FbAPIsAuthListener());
        //SessionEvents.addLogoutListener(new FbAPIsLogoutListener());
        //mHandler = new Handler();
        //if (Utility.mFacebook.isSessionValid()) {
            //requestUserData();
        //}
        //Facebook mFacebook = new Facebook(getResources().getString(R.string.app_id));
        //AsyncFacebookRunner mAsyncRunner = new AsyncFacebookRunner(mFacebook);
        //SessionEvents.addAuthListener(new FbAPIsAuthListener());
        //SessionEvents.addLogoutListener(new FbAPIsLogoutListener());
		CheckInternetAccess check = new CheckInternetAccess(this);
        if (!check.checkNetwork()) {
        	ReturnMessage.showAlartDialog(this, "No internet connection.");
        	return;
        }
    }

    public class FbAPIsAuthListener implements AuthListener {

        @Override
        public void onAuthSucceed() {
            requestUserData();
        }

        @Override
        public void onAuthFail(String error) {
            //mText.setText("Login Failed: " + error);
        }
    }

    public class FbAPIsLogoutListener implements LogoutListener {
        @Override
        public void onLogoutBegin() {
            //mText.setText("Logging out...");
        }

        @Override
        public void onLogoutFinish() {
            //mText.setText("You have logged out! ");
            //mUserPic.setImageBitmap(null);
        }
    }
    public void requestUserData() {
        //mText.setText("Fetching user name, profile pic...");
        Bundle params = new Bundle();
        params.putString("fields", "name, picture");
        Utility.mAsyncRunner.request("me", params, new UserRequestListener());
    }

    public class UserRequestListener extends BaseRequestListener {

        @Override
        public void onComplete(final String response, final Object state) {
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(response);

                final String picURL = jsonObject.getJSONObject("picture")
                        .getJSONObject("data").getString("url");
                final String name = jsonObject.getString("name");
                Utility.userUID = jsonObject.getString("id");

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                       // mText.setText("Welcome " + name + "!");
                       // mUserPic.setImageBitmap(Utility.getBitmap(picURL));
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

		@Override
		public void onFacebookError(FacebookError e, Object state) {
			// TODO Auto-generated method stub
			
		}

    }

    private class LogoutRequestListener extends BaseRequestListener {
        @Override
        public void onComplete(String response, final Object state) {
            /*
             * callback should be run in the original thread, not the background
             * thread
             */
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    SessionEvents.onLogoutFinish();
                }
            });
        }

		@Override
		public void onFacebookError(FacebookError e, Object state) {
			// TODO Auto-generated method stub
			
		}
    }

    private final class LoginDialogListener implements DialogListener {
        @Override
        public void onComplete(Bundle values) {
            SessionEvents.onLoginSuccess();
            Bundle params = new Bundle();
            params.putString("caption", "");
            params.putString("description", s);
            params.putString("picture", "");
            params.putString("name", "");

            Utility.mFacebook.dialog(ShareAppActivity.this, "feed", params, new UpdateStatusListener());
            String access_token = Utility.mFacebook.getAccessToken();
            //System.out.println(access_token);
            
            //new UiAsyncTask().execute();
        }

        @Override
        public void onFacebookError(FacebookError error) {
            SessionEvents.onLoginError(error.getMessage());
        }

        @Override
        public void onError(DialogError error) {
            SessionEvents.onLoginError(error.getMessage());
        }

        @Override
        public void onCancel() {
            SessionEvents.onLoginError("Action Canceled");
        }
    }
    public class UiAsyncTask extends AsyncTask<Void, Void, Void> {

        public void onPreExecute() {
            // On first execute
        }

        public Void doInBackground(Void... unused) {
            // Background Work
             Log.d("Tests", "Testing graph API wall post");
             try {
                    String response = Utility.mFacebook.request("me");
                    Bundle parameters = new Bundle();
                    parameters.putString("message", s);
                    parameters.putString("description", "");
                    response = Utility.mFacebook.request("me/feed", parameters, "POST");
                    Log.d("Tests", "got response: " + response);
                    if (response == null || response.equals("") || response.equals("false")) {
                       
                    }
                    
             } catch(Exception e) {
                 e.printStackTrace();
             }
            return null;
        }

        public void onPostExecute(Void unused) {
             // Result
        	Toast toast = Toast.makeText(getApplicationContext(), "Successfully.",
                    Toast.LENGTH_SHORT);
            toast.show();
        }
    }
    public class UpdateStatusListener extends BaseDialogListener {
        @Override
        public void onComplete(Bundle values) {
            final String postId = values.getString("post_id");
            if (postId != null) {
                //new UpdateStatusResultDialog(ShareAppActivity.this, "Update Status executed", values).show();
            	Toast toast = Toast.makeText(getApplicationContext(), "Successfully.",
                        Toast.LENGTH_SHORT);
                toast.show();
            } else {
                Toast toast = Toast.makeText(getApplicationContext(), "No wall post made",
                        Toast.LENGTH_SHORT);
                toast.show();
            }
        }

        @Override
        public void onFacebookError(FacebookError error) {
            Toast.makeText(getApplicationContext(), "Facebook Error: " + error.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCancel() {
            Toast toast = Toast.makeText(getApplicationContext(), "Cancelled",
                    Toast.LENGTH_SHORT);
            toast.show();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        if(Utility.mFacebook != null) {
            if (!Utility.mFacebook.isSessionValid()) {
                //mText.setText("You are logged out! ");
                //mUserPic.setImageBitmap(null);
            } else {
                Utility.mFacebook.extendAccessTokenIfNeeded(this, null);
            }
        }
    }

    private class SessionListener implements AuthListener, LogoutListener {

        @Override
        public void onAuthSucceed() {
            //setImageResource(R.drawable.logout_button);
            SessionStore.save(Utility.mFacebook, ShareAppActivity.this);
        }

        @Override
        public void onAuthFail(String error) {
        }

        @Override
        public void onLogoutBegin() {
        }

        @Override
        public void onLogoutFinish() {
            //SessionStore.clear(getContext());
            //setImageResource(R.drawable.login_button);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        facebookClient.authorizeCallback(requestCode, resultCode, data);
    }

    @SuppressWarnings("deprecation")
	public void loginToFacebook() {
        mPrefs = getPreferences(MODE_PRIVATE);
        String access_token = mPrefs.getString("access_token", null);
        long expires = mPrefs.getLong("access_expires", 0);

        if (access_token != null) {
            facebookClient.setAccessToken(access_token);
        }

        if (expires != 0) {
            facebookClient.setAccessExpires(expires);
        }

        if (!facebookClient.isSessionValid()) {
            facebookClient.authorize(this, new String[] { "publish_stream" }, new DialogListener() {

                @Override
                public void onCancel() {
                    // Function to handle cancel event
                }

                @Override
                public void onComplete(Bundle values) {
                    // Function to handle complete event
                    // Edit Preferences and update facebook acess_token
                    SharedPreferences.Editor editor = mPrefs.edit();
                    editor.putString("access_token", facebookClient.getAccessToken());
                    editor.putLong("access_expires", facebookClient.getAccessExpires());
                    editor.commit();

                    postToWall();
                }

                @Override
                public void onError(DialogError error) {
                    // Function to handle error

                }

                @Override
                public void onFacebookError(FacebookError fberror) {
                    // Function to handle Facebook errors

                }

            });
        }
    }

    @SuppressWarnings("deprecation")
	private void postToWall() {
        Bundle parameters = new Bundle();
        parameters.putString("name", "Battery Monitor");
        parameters.putString("link", "https://play.google.com/store/apps/details?id=com.ck.batterymonitor");
        parameters.putString("picture", "link to the picture");
        parameters.putString("display", "page");
        // parameters.putString("app_id", "228476323938322");

        facebookClient.dialog(ShareAppActivity.this, "feed", parameters, new DialogListener() {

            @Override
            public void onFacebookError(FacebookError e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onError(DialogError e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onComplete(Bundle values) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onCancel() {
                // TODO Auto-generated method stub
            }
        });
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.backbt:
			intent = new Intent(ShareAppActivity.this, SettingsActivity.class);
        	startActivity(intent);
        	finish();
			break;
		case R.id.setting_layout_1:
			
			ArrayList<String> file = new ArrayList<String>();
			String s3 = "Drive with Orange Eye by NTUC Income, a free in-car camera mobile app with accident detection and emergency call function!  You may like the policy renewal alerts and video sharing features too. (http://bit.ly/1fw98HU)";
			
			try {
				sendEmailMulipleFiles(this,"", "Orange Eye", s3, file);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				 ReturnMessage.showAlartDialog(this, "Share Failed", "The selected application is not installed in this phone.");
			}	
			break;
		case R.id.setting_layout_2:
			boolean flg = false;
			//FBManager fb = new FBManager(ShareAppActivity.this);
			//fb.postOnWall("This is test", "");
			s = "Check out Orange Eye by NTUC Income http://www.income.com.sg/orangeeye/download.asp � it�s an in-car camera and more. With its accident detection and emergency call function, video footage sharing and policy renewal alerts, you may like it as much as I do!";
			
			
			
			Intent fi = new Intent(Intent.ACTION_SEND);
			fi.putExtra(Intent.EXTRA_TEXT, "http://www.income.com.sg/orangeeye/download.asp");
			fi.putExtra(Intent.EXTRA_SUBJECT, s);
			fi.putExtra(Intent.EXTRA_TITLE, s);
			fi.putExtra(Intent.EXTRA_SUBJECT, s);
			fi.setType("text/plain");

			PackageManager packManager = getPackageManager();
			//List<ResolveInfo> reinfo = packManager.queryIntentActivities(fi,  PackageManager.MATCH_DEFAULT_ONLY);
			List<ResolveInfo> activityList = packManager.queryIntentActivities(fi, 0);
			boolean resoved = false;
			for(ResolveInfo resolveInfo: activityList){
			    if(resolveInfo.activityInfo.name.contains("facebook")){
			    	fi.setClassName(
			            resolveInfo.activityInfo.packageName, 
			            resolveInfo.activityInfo.name );
			    	resoved = true;
			    	
			        break;
			    }
			}
			if(resoved){
			    startActivity(fi);
			}else{
				//ReturnMessage.showAlartDialog(this, "Share Failed", "The selected application is not installed in this phone.");
				ReturnMessage.showAlartDialog(ShareAppActivity.this, "Application not installed.");
			}
			
			//SessionEvents.addAuthListener(mSessionListener);
	        //SessionEvents.addLogoutListener(mSessionListener);
			//if (Utility.mFacebook.isSessionValid()) {
                //SessionEvents.onLogoutBegin();
                //AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(Utility.mFacebook);
               // asyncRunner.logout(this, new LogoutRequestListener());
            //} else {
            	//Utility.mFacebook.authorize(this, permissions, AUTHORIZE_ACTIVITY_RESULT_CODE, new LoginDialogListener());
            //}
			/*Intent tweetIntent = new Intent(Intent.ACTION_SEND);
			tweetIntent.putExtra(Intent.EXTRA_TEXT, s);
			tweetIntent.setType("text/plain");

			PackageManager packManager = getPackageManager();
			List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent,  PackageManager.MATCH_DEFAULT_ONLY);

			boolean resolved = false;
			for(ResolveInfo resolveInfo: resolvedInfoList){
			    if(resolveInfo.activityInfo.packageName.startsWith("com.facebook.android")){
			        tweetIntent.setClassName(
			            resolveInfo.activityInfo.packageName, 
			            resolveInfo.activityInfo.name );
			        resolved = true;
			        break;
			    }
			}
			if(resolved){
			    startActivity(tweetIntent);
			}else{
				Intent share = new Intent(Intent.ACTION_SEND);
				share.setType("text/plain");               
                
                share.putExtra(Intent.EXTRA_TEXT, s);

                startActivity(share);
			}*/
			//postToFacebook();
			break;
		case R.id.setting_layout_3:	
			Intent tweetIntent1 = new Intent(Intent.ACTION_SEND);
			String s1 = "Drive with Orange Eye by NTUC Income, a free in-car camera mobile app with accident detection and emergency call function!  You may like the policy renewal alerts and video sharing features too. (http://bit.ly/1fw98HU)";
			tweetIntent1.putExtra(Intent.EXTRA_TEXT, s1);
			tweetIntent1.setType("text/plain");

			PackageManager packManager1 = getPackageManager();
			//List<ResolveInfo> resolvedInfoList1 = packManager1.queryIntentActivities(tweetIntent1,  PackageManager.MATCH_DEFAULT_ONLY);
			//PackageManager packManager1 = v.getContext().getPackageManager();
            List<ResolveInfo> resolvedInfoList1 = packManager1.queryIntentActivities(tweetIntent1, 0);
			boolean resolved1 = false;
			for (final ResolveInfo resolveInfo : resolvedInfoList1) {
			     if ((resolveInfo.activityInfo.name).contains("whatsapp")) {
			        tweetIntent1.setClassName(
			            resolveInfo.activityInfo.packageName, 
			            resolveInfo.activityInfo.name );
			        resolved1 = true;
			        break;
			    }
			}
			if(resolved1){
			    startActivity(tweetIntent1);
			}else{
				//ReturnMessage.showAlartDialog(this, "Share Failed", "The selected application is not installed in this phone.");
				ReturnMessage.showAlartDialog(ShareAppActivity.this, "Application not installed.");
			}
			break;
		case R.id.setting_layout_4:	
			Intent smsIntent = new Intent(Intent.ACTION_VIEW);
			String s2 = "Drive with Orange Eye by NTUC Income, a free in-car camera mobile app with accident detection and emergency call function!  You may like the policy renewal alerts and video sharing features too. (http://bit.ly/1fw98HU)";
			
	        smsIntent.putExtra("sms_body", s2); 
	        smsIntent.putExtra("address", "");
	        smsIntent.setType("vnd.android-dir/mms-sms");

	        startActivity(smsIntent);
			break;
		case R.id.setting_layout_5:	
			String s4 = "Drive with the new Orange Eye by NTUC Income, a free in-car camera mobile app with accident detection and more. http://bit.ly/1fw98HU";
			Intent tweetIntent2 = new Intent(Intent.ACTION_SEND);
			tweetIntent2.putExtra(Intent.EXTRA_TEXT, s4);
			tweetIntent2.setType("text/plain");

			PackageManager packManager2 = getPackageManager();
			List<ResolveInfo> resolvedInfoList2 = packManager2.queryIntentActivities(tweetIntent2,  PackageManager.MATCH_DEFAULT_ONLY);

			boolean resolved2 = false;
			for(ResolveInfo resolveInfo: resolvedInfoList2){
			    if(resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")){
			        tweetIntent2.setClassName(
			            resolveInfo.activityInfo.packageName, 
			            resolveInfo.activityInfo.name );
			        resolved2 = true;
			        break;
			    }
			}
			if(resolved2){
			    startActivity(tweetIntent2);
			}else{
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/intent/tweet?text="+s4));
				  startActivity(browserIntent); 
			}
			break;
		case R.id.setting_layout_6:	
			Intent tweetIntent3 = new Intent(Intent.ACTION_SEND);
			tweetIntent3.putExtra(Intent.EXTRA_TEXT, s);
			tweetIntent3.setType("text/plain");

			PackageManager packManager3 = getPackageManager();
			List<ResolveInfo> resolvedInfoList3 = packManager3.queryIntentActivities(tweetIntent3,  PackageManager.MATCH_DEFAULT_ONLY);

			boolean resolved3 = false;
			for(ResolveInfo resolveInfo: resolvedInfoList3){
			    if(resolveInfo.activityInfo.packageName.contains("line")){
			        tweetIntent3.setClassName(
			            resolveInfo.activityInfo.packageName, 
			            resolveInfo.activityInfo.name );
			        resolved3 = true;
			        break;
			    }
			}
			if(resolved3){
			    startActivity(tweetIntent3);
			}else{
				//ReturnMessage.showAlartDialog(this, "Share Failed", "The selected application is not installed in this phone.");
				Intent share = new Intent(Intent.ACTION_SEND);
				share.setType("text/plain");  
                share.putExtra(Intent.EXTRA_TEXT, s);
                startActivity(share);
			}

			break;
		}
	}

	 @Override
	 public void onBackPressed() {
		Intent intent3 = new Intent(ShareAppActivity.this, SettingsActivity.class);
		startActivity(intent3);
		finish();
	 }
		public static void sendEmailMulipleFiles(Context context, String toAddress, String subject, String body, ArrayList<String> attachmentPath) throws Exception {
		    try {
		        Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
		        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { toAddress });
		        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		        intent.putExtra(Intent.EXTRA_TEXT, body);
		        intent.setType("message/rfc822");
		        PackageManager pm = context.getPackageManager();
		        List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
		        ResolveInfo best = null;
		        for (final ResolveInfo info : matches) {
		            if (info.activityInfo.packageName.contains(".gm.") || info.activityInfo.name.toLowerCase().contains("gmail"))
		                best = info;
		        }
		        ArrayList<Uri> uri = new ArrayList<Uri>();
		        for (int i = 0; i < attachmentPath.size(); i++) {
		            File file = new File(Environment.getExternalStorageDirectory().getPath()+"/OrangeEyeRecord/" +attachmentPath.get(i));
		            uri.add(Uri.fromFile(file));
		        }

		        intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uri);


		        if (best != null)
		            intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);

		        context.startActivity(Intent.createChooser(intent, "Choose an email application..."));
		    } catch (Exception ex) {
		        ex.printStackTrace();	       
		        throw ex;
		    }
		}
		
		public void postToFacebook() {
	        //mProgress.setMessage("Posting ...");
	        //mProgress.show();
			//facebook = new Facebook("878260348856258");
	        //AsyncFacebookRunner mAsyncRunner = new AsyncFacebookRunner(facebook);
			//AsyncFacebookRunner mAsyncRunner;
		    //final Facebook    facebook =new Facebook("878260348856258");
	        //facebook.authorize(this, PERMISSIONS, Facebook.FORCE_DIALOG_AUTH, new LoginDialogListener1());
			String urlToShare = s;
			String query = "Test";
			try {
				query = URLEncoder.encode(s, "utf-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			// intent.putExtra(Intent.EXTRA_SUBJECT, "Foo bar"); // NB: has no effect!
			intent.putExtra(Intent.EXTRA_TEXT, query);

			// See if official Facebook app is found
			boolean facebookAppFound = false;
			List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
			for (ResolveInfo info : matches) {
			    if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
			        intent.setPackage(info.activityInfo.packageName);
			        facebookAppFound = true;
			        break;
			    }
			}

			// As fallback, launch sharer.php in a browser
			if (!facebookAppFound) {
				
			    String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + query;
			    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
			}

			startActivity(intent);
			

	    }
		class LoginDialogListener1 implements DialogListener {
		    public void onComplete(Bundle values) {
		    	//saveCredentials(facebook);
		    	String messageToPost = "Test sample";
		    	if (messageToPost != null){
		    		postToWall(messageToPost, facebook);
		    	}
		    }
		    public void onFacebookError(FacebookError error) {
		    	//showToast("Authentication with Facebook failed!");
		        finish();
		    }
		    public void onError(DialogError error) {
		    	//showToast("Authentication with Facebook failed!");
		        finish();
		    }
		    public void onCancel() {
		    	//showToast("Authentication with Facebook cancelled!");
		        finish();
		    }
		}
		public void postToWall(String message, Facebook facebook){
			Bundle parameters = new Bundle();
	                parameters.putString("message", message);
	                parameters.putString("description", "topic share");
	                try {
	        	        facebook.request("me");
				String response = facebook.request("me/feed", parameters, "POST");
				Log.d("Tests", "got response: " + response);
				if (response == null || response.equals("") ||
				        response.equals("false")) {
					//showToast("Blank response.");
				}
				else {
					//showToast("Message posted to your facebook wall!");
				}
				finish();
			} catch (Exception e) {
				//showToast("Failed to post to wall!");
				e.printStackTrace();
				finish();
			}
		}
    
}
